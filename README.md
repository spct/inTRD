# inSCL/inTRD v6.52

Plugins for Total Commander for working with ZX Spectrum TR-DOS disk images `*.trd` and `*.scl`.

- Support viewing, extracting, creating, modifying images;
- smart TR-DOS file length detection;
- hobeta files support;
- transferring START and LENGTH properties via file creation date;
- automatic joining of split TR-DOS files;
- checking SCL file integrity;
- show deleted files;
- can defragment TRD images;
- partial DirSys support;
- bonus functionality: content plugin (WDX);
- configurable;
- detailed manual in English and Russian.

Download release packages:

- [GitLab](releases/)
- [Totalcmd.net](<https://totalcmd.net/authors/6888865.html>)
- [Wincmd.ru](<https://wincmd.ru/authors/6888865.html>)
