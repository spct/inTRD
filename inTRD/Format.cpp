﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

#include "common.h"
#include "Format.h"
#include "ArcHandle.h"
#include "helpers.h" // ParseHexByte

FILETIME FileCreateDateData::MakeFiletime()
{
	/*
	 * "FILETIME struct contains a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601 (UTC)." (c) MSDN
	 * Отсюда следует, что функции Get/SetFileTime принимают/возвращают UTC-дату;
	 * точность хранения creation date составляет 0.01 сек.
	 * 
	 * Старая кодировка использовала даты с 1980-01-01 по 1981-05-12.
	 * Чтобы с ней не пересекаться, новая кодировка использует диапазон начинающийся с 1981-06-01.
	 * Если файл с датой 1981-06-01 переедет в другой пояс, его дата может стать 1981-05-31,
	 * что мне не нравится :) поэтому начальной датой возьмём на день позже: 1981-06-02.
	 * Кроме того, добавим возможность контроля, что дата передана с требуемой точностью.
	 * Для этого используем только даты с нечётным числом сантисекунд.
	 *
	 * Получаем диапазон:
	 *  0x00000000 = 1981-06-02 00:00:00.010 = 120047616000100000
	 *  0xFFFFFFFF = 1984-02-21 04:55:45.910 = 120906609459100000
	 */

	/* В inTAP ещё использует спец. значение (1984-02-21 04:55:48.000) для маркера headerless. 
	 * Почему именно такая дата? Нам не требуется защита от случайного образования такой даты
	 * (во-первых, она и так мала; во-вторых, это только часть маркера (вторая часть - имя файла)),
	 * важнее передать маркер. Поэтому это должно быть целое число секунд, а еще лучше - чётное;
	 * но 46 сек. брать не желательно - оно может образоваться в результате округления 45.910.
	 * Поэтому 48 сек.
	 */

	if (!IsValid()) { // should never happen
		_throw("FCDD01");
	}

	union { FILETIME t; uint64 q; };
	
	if (IsHeaderless) { 
		q = 120906609480000000;
		return t; 
	}

	DWORD StartLength = Start + (Length << 16);
	uint64 x = StartLength * 2ui64 + 1;
	q = x * 100000u + 120047616000000000u;
	return t;
};

FileCreateDateData::FileCreateDateData(FILETIME createDate)
{
	union { FILETIME t; uint64 q; };
	t = createDate;

	clear_this();
	if (q == 120906609480000000)
	{
		IsHeaderless = true;
	}
	else
	{
		q -= 120047616000000000;
		if (q % 100000u == 0)
		{
			q /= 100000u;
			if ((q & 1) != 0 && (q >>= 1) <= UINT_MAX)
			{
				Start = (ushort)q;
				Length = (ushort)(q >> 16);
				Has_StartLength = true;
			}
		}
	}
};


Result NameZX2PC_char(byte c, String<char>& dest, bool escapeSpace)
{
	if (memchr_(InvalidFileNameChars, c, sizeof(InvalidFileNameChars))
		|| c >= 127
		|| c == '#'
		|| (escapeSpace && c == ' ')
		)
		return dest.Append('#', HexChars_Up[c / 16u], HexChars_Up[c & 15]);
	else
		return dest.Append(c);
};

Result NameZX2PC(const NAME_HEADER& header, String<char>& output, bool hobeta, CONFIG& cfg)
{
	Result res = SUCCESS;

	// Name

	const byte* name = header.Name;
	const byte* end = &LAST(header.Name);
	while(end >= name && *end == ' ') end--; // remove spaces padding. It's ok to leave name empty as we have extension yet.
	for (; name <= end; name++) {
        res &= NameZX2PC_char(*name, output, false);
	}
	if (!res) {
		return res;
	}

	// Extension

	res &= output.Append('.');
	name = &header.Extension;

	if (hobeta) {
		res &= output.Append('$');
	}

	// пробел в расширении должен быть заэскейплен, т.к. имя файла не может заканчиваться пробелом или точкой.
	// (такие файлы есть в INSTY_10.TRD)
	res &= NameZX2PC_char(*name++, output, true);

	// Не показываем длинное расширение для basic-файлов. В этом нет
	// никакого смысла, т.к. параметр start содержит длину basic-программы.
	// Кроме того, это затрудняет последующее определение типа (basic/не-basic)
	// файла при паковке во всех моих плагинах.
	if (header.Extension != 'B')
	{
		if (cfg.ExtendedExtension && InRange(name[0], 33, 126) && InRange(name[1], 32, 126))
		{
			res &= NameZX2PC_char(name[0], output, false);
			if (name[1] != ' ') {
				res &= NameZX2PC_char(name[1], output, false);
			}
		}
	}

	return res;
};

// Build file name in dest.
// Assumes that dest points to tHeaderDataEx.FileName, which is at least [260]
Result GetName(Entry& entry, char* dest, CONFIG& cfg, bool withDups)
{
	Result res = SUCCESS;
	String<char> s;
	s.AddSpace(50);    // зарезервируем (если получится) немного, чтобы уменьшить число realloc'ов

	if (entry.IsJoinedFile) {
		for(char* c = cfg.AutojoinedFilePrefix; *c; c++) 
			res &= NameZX2PC_char(*c, s, false);
	}
	if (!res) {
		return res;
	}

	if (entry.MBD_Mode)
	{
		// Number prefix not used in this mode.

		MBD::FILE_NAMING_MODE fm =
		{
			MBD::FILE_NAMING_MODE::STRUCT_VER, 
			false,								// bool Decimal. Use hex because it's shorter
			MBD::FILENAME_GAP::GAP_FIXED,		// FILENAME_GAP Gap
			"  \x01 \x02 \x03 \x04 \x05 \x06"	// ParamsPattern
		};
		
		res = MBD::NameMBD2PC_File(s, &entry.MBDFileInfo, fm);
	}
	else
	{
		// Number prefix.
		// Max len: 3 + 3*8

		uint n = entry.FileN;
		if (cfg.FileNameNumbering() != NUMBERING::NONE && n <= 255)
		{
			if (cfg.FileNameNumbering() == NUMBERING::HEX) {
				res = s.AppendUint0_Hex(n, 2, HexChars_Up);
			} else {
				res = s.AppendUint0(n, 3);
			}

			for(char* c = cfg.FileNameNumberingSeparator; *c; c++) {
				res &= NameZX2PC_char(*c, s, false);
			}

			if (!res) {
				return res;
			}
		}

		// Name and extension

		res = NameZX2PC(entry.NameHeader, s, entry.HobetaHeaderProvided, cfg);
	}

	// DuplicateN suffix

	if (withDups && entry.DuplicateN != 0)
	{
		if (res) res = s.Append('[');
		if (res) res = s.AppendUint(entry.DuplicateN + 1);
		if (res) res = s.Append(']');
	}

	if (!res) {
		return res;
	}

	if (s.Length + 1 > 260) {
		return Result("Filename too long", true); // should never happen
	}
	strcpy_(dest, s.GetPtr());

	return SUCCESS;
};

// Если s < sEnd, то обрабатывает 1 символ, смещает указатель s, возвращает true.
bool NamePC2ZX_ParseChar(char* &s, char* sEnd, byte* out_char)
{
	if (s >= sEnd) {
		return false;
	}
	byte c;
	if (s + 2 < sEnd && s[0] == '#' && ParseHexByte(s[1], s[2], &c)) {
		s += 3;
	} else { 
		c = InRange(s[0], 0, 126) ? s[0] : '?'; 
		s++; 
	}
	if (out_char != NULL) {
		*out_char = c;
	}
	return true;
};

// Parse numbering prefix, return its length
size_t NamePC2ZX_GetNumPrefixLen(const char* name, const CONFIG& cfg)
{
	for(uint l = 2; l <= 3; l++)
	{
		// l==2 - try hex; l==3 - try dec
		const char* p = name;
		for(uint i = 0; i < l; i++)	{
			bool isDigit = InRange(*p, '0', '9') || (l == 2 && InRange(*p, 'A', 'F'));
			if (!isDigit) {
				goto k0;
			}
			p++;
		}
		for(const char* s = cfg.FileNameNumberingSeparator; *s != 0; s++, p++) {
			if (*s != *p)
				goto k0;
		}

		return (p - name);
	k0: ;
	}
	
	return 0;
};

// Отрезает numbering prefix и DuplicateN-суффикс.
// Заполняет Name, Extension;
// если есть длинное расширение и разрешена его поддержка, то заполняет и Start.
// name может содержать и '\', т.е. относительный путь.
// name не может быть пустым!
void NamePC2ZX(char* name, NAME_HEADER* output, const CONFIG& cfg, bool canBeHobeta, bool* out_hasHobetaExtension)
{
	// Пропустим всё до последнего слэша

	char* p;
	for(p = name; *p; p++) {
		if (IsSlash(*p))
			name = p + 1;
	}
	char* nameEnd = p;		// сейчас указывает на zero-terminator

	// Теперь name указывает на начало имени файла
	
	// Skip numbering prefix
	size_t prefLen = NamePC2ZX_GetNumPrefixLen(name, cfg);
	name += prefLen;
		
	// Strip duplicates number
	char* i = nameEnd;
	if (i > name && *(--i) == ']')
	{
		size_t len = 0;
		while(i > name && byte(*(--i) - '0') < 10) {
			len++;
		}
		if (len > 0 && *i == '[') {
			nameEnd = i;
		}
	}

	// инициализация

	fill_array(output->Name, ' ');
	output->Extension = 'C'; // default extension

	// теперь обрабатываем расширение
	
	*out_hasHobetaExtension = false;
	p = nameEnd;
	while (p > name && p[-1] != '.') {
		p--;
	}
	if (p > name) 
	{
		// есть расширение

		char* newNameEnd = p - 1;

		*out_hasHobetaExtension = (canBeHobeta && *p == '$');
		if (*out_hasHobetaExtension) {
			// '$' тоже считаем в extLen: если caller решит, что это не hobeta, значит это должно считаться частью расширения
			p++;
		}
		
		if (NamePC2ZX_ParseChar(p, nameEnd, &output->Extension))	 // первый символ расширения
		{
			// для файлов типа 'B' нет записывать длинное расширение вместо длины файла
			if (output->Extension != 'B' && cfg.ExtendedExtension)
			{
				byte extext[2];
				if (NamePC2ZX_ParseChar(p, nameEnd, &extext[0])) // второй символ расширения
				{
					if (InRange(extext[0], 33, 126))
					{
						// two-symbol extension (3rd char is space)
						output->Extension2 = extext[0] + 0x2000;
					}
					if (NamePC2ZX_ParseChar(p, nameEnd, &extext[1])) // третий символ расширения
					{
						if (InRange(extext[0], 33, 126) && InRange(extext[1], 33, 126))
						{
							// Got 3-symbol extension
							output->Extension2 = *(WORD*)extext;
						}
					}
				}
			}
		}

		nameEnd = newNameEnd;
	}

	// наконец займёмся именем

	byte* dest = output->Name;
	for(size_t i = 0; NamePC2ZX_ParseChar(name, nameEnd, dest); )
	{
		dest++;
		i++;
		if (i >= ARRAY_LEN(output->Name)) {	// take first 8 symbols only
			dest = NULL;
		}
	}
};


// Add entries for big (compound) files
Result AutojoinFiles(ArcHandle& arc)
{
	Result res;
	for(uint i = 0; i < arc.Entries.Count; )
	{
		Entry* e0 = arc.Entries[i];
		const NAME_HEADER* h0 = &e0->NameHeader;

		uint totalSize = 0, totalSectors = 0;
		uint cnt = 0;
		do
		{
			Entry* e = arc.Entries[i];
			const NAME_HEADER* h = &e->NameHeader;
			if (!e->IsRealFile) {
				break;
			}

			// Compare file names ignoring padding
			byte buf0[8], buf1[8];
			memcpy_(buf0, h0->Name, 8);
			memcpy_(buf1, h ->Name, 8);
			for(uint j = 8; j != 0; j--) if (buf0[j-1] < 33) buf0[j-1] = 0; else break;
			for(uint j = 8; j != 0; j--) if (buf1[j-1] < 33) buf1[j-1] = 0; else break;
			if (!memeq(buf0, buf1, 8)) {
				break;
			}
			
			// Не допускаем, чтобы файлы, участвующие в autojoin'е, шли на диске с разрывом
			if (e->FileDataPos != e0->FileDataPos + totalSize) {
				break;
			}

			bool size65280 = (e->Header->Length == 0xFF00 && e->Header->Sectors == 0xFF);
			bool matchExt = (h->Extension2 == h0->Extension2 && (cnt == 0 || h->Extension == (byte)('0' + cnt - 1)));

			if (!matchExt) {
				break;
			}
			i++;
			cnt++;
			totalSize += e->FileDataSize;
			totalSectors += e->SizeInSectors;
			if (!size65280)
			{
				break;
			}
		} while (i < arc.Entries.Count);

		if (cnt == 0)
		{
			i++;
		}
		else if (cnt > 1)
		{
			/*
			 * Была мысль сделать так, чтобы составные файлы autojoined файлов
			 * не показывались. Но потом сообразил, что тогда autojoined файлы
			 * невозможно будет скопировать из архива в архив.
			 */
			/*
			 * Обращаем внимание, что даже в режиме inMBD compatibility 
			 * склеенные файлы не получают MBD-заголовка, нет смысла.
			 */

			Entry* e;
			if (!(res = arc.CreateEntry(&e))) {
				return res;
			}
	
			e->IsRealFile = false;
			e->IsJoinedFile = true;
			e->FileN = UINT_MAX;    // чтобы числовой префикс в имени не добавлялся
			e->JoinedFileStart = e0->FileN;
			e->JoinedFileCount = cnt;
			e->FileDataPos = e0->FileDataPos;
			e->FileDataSize = totalSize;
			e->SizeInSectors = totalSectors;
			e->NameHeader = e0->NameHeader;
			e->Header = NULL;
			e->CreateDate = FileCreateDateData();	// don't set creation date
		}
	}

	return SUCCESS;
};

bool SetFileModificationAndCreationDate(HANDLE hFile, FILETIME modificationTimeUtc, FILETIME creationTimeUtc)
{
	#define IS_ZERO_FILETIME(t) (t.dwLowDateTime == 0 && t.dwHighDateTime == 0)
	
	return (0 != SetFileTime(hFile, 
		!IS_ZERO_FILETIME(creationTimeUtc) ? &creationTimeUtc : NULL, 
		NULL,
		!IS_ZERO_FILETIME(modificationTimeUtc) ? &modificationTimeUtc : NULL
	));
};
