﻿
#pragma once

#include "common.h"

bool DirSys_Exists(byte* image, uint size);

// Scan dirsys structure to find its actual size.
// Returns the position of final zero byte.
// Does not check dirsys header (call DirSys_Exists before).
// If dirsys structure looks corrupted, returns 0.
uint DirSys_End(byte* image, uint size);

ushort DirSys_CRC(byte* data, uint count);
