﻿
#include "ArcHandle.h"
#include "Format_TRD.h"

Result ArcHandle::Open(const TCHAR* path, bool writeable, DWORD createDisposition)
{
	Result res = File.Open(path, writeable, createDisposition, false, true);
	if (res && !File.Created)
	{
		// Opening existing archive
		
		// Image must have sys sector; image can't be larger than 256 tracks
		TRD_SystemSector* syssec = (TRD_SystemSector*)(File.Adr + 0x800);
		if (!InRange(File.Size, 0x900, 256 * 4096)) 
			res = Result(E_UNKNOWN_FORMAT);
		else if (syssec->Mark_10 != 0x10)
			res = Result(E_UNKNOWN_FORMAT);
	}

	if (res)
	{
		// Save creation and mod date, we may need these
		if ((0 == GetFileTime(File.Handle(), &ArchiveCreateDateUtc, NULL, &ArchiveDateUtc))
			|| !(ArchiveDateTC = Utc2DosLocal(ArchiveDateUtc)))
		{
			res = Result(E_EOPEN);
		}
	}

	if (!res) {
		File.Close();
	}
	return res;
};
