﻿
#include "common.h"
#include <tchar.h>
#include "settings.h"
#include "resource.h"
#include "MyStructs\Result.h"
#include "MyStructs\Dialog.h"
#include "MyStructs\WcxHelpers.h"
#include "MyStructs\IniFileHelpers.h"

#define PluginName "inTRD"
#define cfg_name "inTRD.cfg"

TCHAR cfgpath[MAX_PATH + 1];	// [global var] stores config file path

// При изменении не забыть поменять версию в PluginInfo.cpp
// TODO: url сильно сложный, надо либо сделать его clickable, либо дать возможность скопировать его
const char about[] = "\ninTRD 6.52 (20-Oct-2021)\n\nWritten by Eugene Larchenko\n(spct@mail.ru)\nhttps://totalcmd.net/authors/6888865.html";

static void SaveConfig(CONFIG cfg, bool showMsgOnError, HWND errMsgParent);

// Default config values
static volatile CONFIG PrimaryCfg =
{
	CONFIG::STRUCT_VER,

	false,				// bool InMBDCompatible;
	NUMBERING::DEC,		// FileNameNumbering
	"!   \0\0\0\0",		// FileNameNumberingSeparator. Supposed to never change; whoever wants changing it, pokes into config file manually

	true,				// bool SmartLenDetect;
	false,				// bool ExtendedExtension;
	true,				// bool DetectHobeta;
	false,				// bool ExtractHobeta;
	false,				// bool ShowDeletedFiles;
	false,				// bool ShowSizeInSectors;
	false,				// bool DefragmentOnDelete;

	true,				// bool AutoJoinLargeFiles;
	"  \0\0\0\0\0\0"	// char AutojoinedFilePrefix[9];
};

// Some 99.9999% reliable mutex
static volatile bool config_mutex = false;

CONFIG GetPrimaryConfig()
{
	// знаю, знаю, это не совсем правильный "мутех", но чрезмерная надёжность здесь не требуется
	while(config_mutex) Sleep(0);
	config_mutex = true;

	CONFIG c;
	memcpy_(&c, (void*)&PrimaryCfg, sizeof(CONFIG));

	config_mutex = false;
	return c;
};

static void SetPrimaryConfig(CONFIG cfg, bool save, bool showMsgOnSaveError, HWND errMsgParent)
{
	while(config_mutex) Sleep(0);
	config_mutex = true;

	memcpy_((void*)&PrimaryCfg, &cfg, sizeof(CONFIG));

	config_mutex = false;

	if (save) {
		SaveConfig(cfg, showMsgOnSaveError, errMsgParent);
	}
};

static void SaveConfig(CONFIG cfg, bool showMsgOnError, HWND errMsgParent)
{
	Result res = SUCCESS;
	cfg.StructVer = CONFIG::STRUCT_VER;

	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("InMBDCompatible"), cfg.InMBDCompatible);
	res &= ConfigFile_SaveUint(cfgpath, _T(PluginName), _T("FileNameNumbering"), cfg.FileNameNumbering_);
	res &= ConfigFile_SaveStruct(cfgpath, _T(PluginName), _T("FileNameNumberingSeparator"), &cfg.FileNameNumberingSeparator, sizeof(cfg.FileNameNumberingSeparator));

	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("SmartLenDetect"), cfg.SmartLenDetect_);
	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("ExtendedExtension"), cfg.ExtendedExtension);
	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("DetectHobeta"), cfg.DetectHobeta);
	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("ExtractHobeta"), cfg.ExtractHobeta_);
	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("ShowDeletedFiles"), cfg.ShowDeletedFiles);
	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("ShowSizeInSectors"), cfg.ShowSizeInSectors);
	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("DefragmentOnDelete"), cfg.DefragmentOnDelete);

	res &= ConfigFile_SaveBool(cfgpath, _T(PluginName), _T("AutoJoinLargeFiles"), cfg.AutoJoinLargeFiles_);
	res &= ConfigFile_SaveStruct(cfgpath, _T(PluginName), _T("AutojoinedFilePrefix"), &cfg.AutojoinedFilePrefix, sizeof(cfg.AutojoinedFilePrefix));

	if (showMsgOnError) {
		res.ShowMessageIfNeed(errMsgParent);
	}
};

static CONFIG ReadConfig() 
{
	// Helper, loading string saved as struct
	#define ReadConfig_String(name, var)											\
	{																				\
		byte buf[sizeof(var)];														\
		if (GetPrivateProfileStruct(_T(PluginName), _T(name), buf, sizeof(buf), cfgpath)) {\
			COPY(var, buf);														    \
		}																		    \
		LAST(var) = 0; /* extra precaution */										\
	}																				\

	CONFIG default = GetPrimaryConfig();
	CONFIG cfg = default;

	cfg.InMBDCompatible = (0 != GetPrivateProfileInt(_T(PluginName), _T("InMBDCompatible"), default.InMBDCompatible, cfgpath));

	int t = GetPrivateProfileInt(_T(PluginName), _T("FileNameNumbering"), default.FileNameNumbering_, cfgpath);
	if (t < NUMBERING::NONE) t = NONE;
	if (t > NUMBERING::DEC) t = DEC;
	cfg.FileNameNumbering_ = (NUMBERING)t;

	ReadConfig_String("FileNameNumberingSeparator", cfg.FileNameNumberingSeparator);

	cfg.SmartLenDetect_ = (0 != GetPrivateProfileInt(_T(PluginName), _T("SmartLenDetect"), default.SmartLenDetect_, cfgpath));
	cfg.ExtendedExtension = (0 != GetPrivateProfileInt(_T(PluginName), _T("ExtendedExtension"), default.ExtendedExtension, cfgpath));
	cfg.DetectHobeta = (0 != GetPrivateProfileInt(_T(PluginName), _T("DetectHobeta"), default.DetectHobeta, cfgpath));
	cfg.ExtractHobeta_ = (0 != GetPrivateProfileInt(_T(PluginName), _T("ExtractHobeta"), default.ExtractHobeta_, cfgpath));
	cfg.ShowDeletedFiles = (0 != GetPrivateProfileInt(_T(PluginName), _T("ShowDeletedFiles"), default.ShowDeletedFiles, cfgpath));
	cfg.ShowSizeInSectors = (0 != GetPrivateProfileInt(_T(PluginName), _T("ShowSizeInSectors"), default.ShowSizeInSectors, cfgpath));
	cfg.DefragmentOnDelete = (0 != GetPrivateProfileInt(_T(PluginName), _T("DefragmentOnDelete"), default.DefragmentOnDelete, cfgpath));
	
	cfg.AutoJoinLargeFiles_ = (0 != GetPrivateProfileInt(_T(PluginName), _T("AutoJoinLargeFiles"), default.AutoJoinLargeFiles_, cfgpath));
	ReadConfig_String("AutojoinedFilePrefix", cfg.AutojoinedFilePrefix);

	return cfg;
};

class ConfigDialog : public Dialog<IDD_CONFIG>
{
public:
	CONFIG Cfg;
	
private:
	HFONT oldMessageFont;

	bool InitDialog(WPARAM hFocusItem, LPARAM dwInitParam)
	{
		oldMessageFont = SetLargeFont(IDC_MESSAGE);

		SetDlgItemTextA(hDlg, IDC_ABOUT, about);
		SetDlgItemText(hDlg, IDC_CFGPATH, cfgpath);

		SetCheck(IDC_INMBD_COMPATIBLE, Cfg.InMBDCompatible);
		CheckRadioButton(IDC_NUMBERING_NONE, IDC_NUMBERING_DEC, Cfg.FileNameNumbering_ - NUMBERING::NONE + IDC_NUMBERING_NONE);
		SendDlgItemMessage(hDlg, IDC_NUMBERING_SEPARATOR, EM_SETLIMITTEXT, ARRAY_LEN(Cfg.FileNameNumberingSeparator) - 1, 0);
		SetDlgItemTextA(hDlg, IDC_NUMBERING_SEPARATOR, Cfg.FileNameNumberingSeparator);

		SetCheck(IDC_SMART_LEN_DETECT, Cfg.SmartLenDetect_);
		SetCheck(IDC_EXT_EXT, Cfg.ExtendedExtension);
		SetCheck(IDC_DETECT_HOBETA, Cfg.DetectHobeta);
		SetCheck(IDC_EXTRACT_HOBETA, Cfg.ExtractHobeta_);
		SetCheck(IDC_SHOW_DELETED, Cfg.ShowDeletedFiles);
		SetCheck(IDC_SHOWSIZEINSECTORS, Cfg.ShowSizeInSectors);
		SetCheck(IDC_DEFRAG_ON_DELETE, Cfg.DefragmentOnDelete);

		SetCheck(IDC_AUTOJOIN, Cfg.AutoJoinLargeFiles_);
		SendDlgItemMessage(hDlg, IDC_AUTOJOINED_PREFIX, EM_SETLIMITTEXT, ARRAY_LEN(Cfg.AutojoinedFilePrefix) - 1, 0);
		SetDlgItemTextA(hDlg, IDC_AUTOJOINED_PREFIX, Cfg.AutojoinedFilePrefix);

		DlgInitialized = true;
		UpdateControls();
		return true;
	};

	bool DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		if (uMsg == WM_COMMAND)
		{
			if (DlgInitialized)
			{
				switch(LOWORD(wParam))
				{
				case IDC_NUMBERING_NONE:
				case IDC_NUMBERING_HEX:
				case IDC_NUMBERING_DEC:
					CheckRadioButton(IDC_NUMBERING_NONE, IDC_NUMBERING_DEC, LOWORD(wParam));

				case IDC_INMBD_COMPATIBLE:
				case IDC_NUMBERING_SEPARATOR:

				case IDC_SMART_LEN_DETECT:
				case IDC_EXT_EXT:
				case IDC_DETECT_HOBETA:
				case IDC_EXTRACT_HOBETA:
				case IDC_SHOW_DELETED:
				case IDC_SHOWSIZEINSECTORS:

				case IDC_AUTOJOIN:
				case IDC_AUTOJOINED_PREFIX:
					UpdateControls();
					break;

				case IDOK:
					if (!IsValid(NULL)) {
						break; // do not close dialog
					}
					Cfg = GetConfig();
				case IDCANCEL:
					SendDlgItemMessage(hDlg, IDC_MESSAGE, WM_SETFONT, (WPARAM)oldMessageFont, FALSE); // restore font
					DeleteObject(oldMessageFont);
					EndDialog(hDlg, LOWORD(wParam)); // close dialog
					break;
				}
				return true;
			}
		}
		return false;
	}

	bool IsValid(CONFIG* cfg)
	{
		//CONFIG temp;
		//if (!cfg)
		//{
		//	temp = GetConfig();
		//	cfg = &temp;
		//}
		//return cfg->FileNamingMode.ParamsPatternValid();
		return true;
	}

	CONFIG GetConfig()
	{
		CONFIG cfg = Cfg;

		cfg.InMBDCompatible = IsChecked(IDC_INMBD_COMPATIBLE);
		cfg.FileNameNumbering_ = NUMBERING(GetRadioValue(IDC_NUMBERING_NONE, IDC_NUMBERING_DEC) - IDC_NUMBERING_NONE + NUMBERING::NONE);
		GetDlgItemTextA(hDlg, IDC_NUMBERING_SEPARATOR, cfg.FileNameNumberingSeparator, ARRAY_LEN(cfg.FileNameNumberingSeparator));

		cfg.SmartLenDetect_ = IsChecked(IDC_SMART_LEN_DETECT);
		cfg.ExtendedExtension = IsChecked(IDC_EXT_EXT);
		cfg.DetectHobeta = IsChecked(IDC_DETECT_HOBETA);
		cfg.ExtractHobeta_ = IsChecked(IDC_EXTRACT_HOBETA);
		cfg.ShowDeletedFiles = IsChecked(IDC_SHOW_DELETED);
		cfg.ShowSizeInSectors = IsChecked(IDC_SHOWSIZEINSECTORS);
		cfg.DefragmentOnDelete = IsChecked(IDC_DEFRAG_ON_DELETE);
		cfg.AutoJoinLargeFiles_ = IsChecked(IDC_AUTOJOIN);
		GetDlgItemTextA(hDlg, IDC_AUTOJOINED_PREFIX, cfg.AutojoinedFilePrefix, ARRAY_LEN(cfg.AutojoinedFilePrefix));

		return cfg;
	}
	
	void UpdateControls()
	{
		CONFIG newcfg = GetConfig();

		bool needRestart = false
			|| newcfg.InMBDCompatible != Cfg.InMBDCompatible
			|| newcfg.FileNameNumbering() != Cfg.FileNameNumbering()
			|| !streq(newcfg.FileNameNumberingSeparator, Cfg.FileNameNumberingSeparator)

			|| newcfg.SmartLenDetect() != Cfg.SmartLenDetect()
			|| newcfg.ExtendedExtension != Cfg.ExtendedExtension
			//|| newcfg.DetectHobeta != Cfg.DetectHobeta	// no need to restart
			|| newcfg.ExtractHobeta() != Cfg.ExtractHobeta()
			|| newcfg.ShowDeletedFiles != Cfg.ShowDeletedFiles
			|| newcfg.ShowSizeInSectors != Cfg.ShowSizeInSectors
			//|| newcfg.DefragmentOnDelete != Cfg.DefragmentOnDelete	// no need to restart

			|| newcfg.AutoJoinLargeFiles() != Cfg.AutoJoinLargeFiles()
			|| !streq(newcfg.AutojoinedFilePrefix, Cfg.AutojoinedFilePrefix)
			;

		EnableDlgItem(IDC_EXTRACT_HOBETA, !newcfg.InMBDCompatible);
		EnableDlgItem(IDC_SMART_LEN_DETECT, !newcfg.ExtractHobeta());
		EnableDlgItem(IDC_AUTOJOIN, !newcfg.ExtractHobeta());
		
		EnableDlgItem(IDC_NUMBERING_NONE, !newcfg.InMBDCompatible);
		EnableDlgItem(IDC_NUMBERING_HEX, !newcfg.InMBDCompatible);
		EnableDlgItem(IDC_NUMBERING_DEC, !newcfg.InMBDCompatible);

		char* msg = "";
		if (needRestart) {
			msg = "\nPlease restart all instances of Total Commander to fully apply new " PluginName " settings!";
		} else {
			// Comment out when releasing!
			//if (_allocated_ != 0) msg = "(debug info: nonzero number of bytes allocated)";
		}
		SetDlgItemTextA(hDlg, IDC_MESSAGE, msg);

		EnableDlgItem(IDOK, IsValid(&newcfg));
	}

	HFONT SetLargeFont(int nIDDlgItem)
	{
		HFONT hCurFont = (HFONT)SendDlgItemMessage(hDlg, nIDDlgItem, WM_GETFONT, 0, 0);
		HFONT hFont = hCurFont;
		if (hFont == NULL) {
			hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
		}
		LOGFONT lf;
		GetObject(hFont, sizeof(lf), &lf);
		lf.lfHeight += lf.lfHeight / 4;  // make it 25% larger
		lf.lfWeight = FW_BOLD;           // and bold
		HFONT hBigFont = CreateFontIndirect(&lf);
		
		SendDlgItemMessage(hDlg, nIDDlgItem, WM_SETFONT, (WPARAM)hBigFont, FALSE);
		return hCurFont; // return old font, need to restore it when disposing dialog
	}
};


void __stdcall ConfigurePacker (HWND Parent, HINSTANCE DllInstance)
{
	ConfigDialog cd;
	cd.Cfg = GetPrimaryConfig();

	if (cd.Show(Parent, DllInstance) == IDOK)
	{
		SetPrimaryConfig(cd.Cfg, true, true, Parent);
	}
}

BOOL InitConfig(HANDLE hModule)
{
	// Compute config file path

	// Мы не поддерживаем пути длиннее MAX_PATH, чтобы не делать большой буфер и не заморачиваться с alloc.
	ssize_t len = (ssize_t)GetModuleFileName((HMODULE)hModule, cfgpath, ARRAY_LEN(cfgpath));
	//if (len >= ARRAY_LEN(cfgpath)) { error: truncated }

	if (len + strlen_(_T(cfg_name)) >= ARRAY_LEN(cfgpath))
	{
		//MessageBoxA(NULL, "Path of plugin DLL is too long", PluginName, MB_OK | MB_ICONERROR | MB_APPLMODAL);
		Result::ShowErrorMessage(NULL, "Path of plugin DLL is too long.");
		return false;
	}

	ssize_t i;
	for (i = len-1; i >= 0 && !IsSlash(cfgpath[i]); i--);
	// either i==-1 or i points to last slash
	strcpy_(cfgpath+i+1, _T(cfg_name));

	SetPrimaryConfig(ReadConfig(), false, false, NULL);
	return true;
};
