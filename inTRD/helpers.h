﻿
#pragma once

#include "common.h"

extern const sbyte Char2Hex[23];

// Parse hexadecimal byte, eg "A5" -> 165, or return false on error.
// Does not change 'result' if returning false (as required by NamePC2MBD_Simple routine)
template<typename TCHR, typename TRESULT>
bool ParseHexByte(TCHR c1, TCHR c2, TRESULT* result)
{
	if (unsigned(c1 -= '0') >= sizeof(Char2Hex) || Char2Hex[unsigned(c1)] < 0) return false;
	if (unsigned(c2 -= '0') >= sizeof(Char2Hex) || Char2Hex[unsigned(c2)] < 0) return false;
	*result = (Char2Hex[unsigned(c1)] << 4) + Char2Hex[unsigned(c2)];
	return true;
}

