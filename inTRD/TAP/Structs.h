﻿
#pragma once

#include "..\common.h"

namespace TAP
{
	// 17-byte tape header block
	#pragma pack(push, 1)
	struct Header
	{
		byte Type;
		byte Name[10];	// complemented with spaces
		WORD Length;
		WORD Param1;
		WORD Param2;
	};
	#pragma pack(pop)
};
