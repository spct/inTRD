﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

#include "common.h"
#include "wcxhead.h"
#include "ArcHandle.h"
#include "MyStructs\Result.h"
#include "MyStructs\Random.h"
#include "MyStructs\EncodingHelpers.h"
#include "settings.h"
#include "Format.h"
#include "Format_TRD.h"

HINSTANCE dllInstance;


// Поддержка дупликатов имён обязательна, т.к. они встречаются нередко,
// особенно в режиме inMBD-compatibility. Например на диске IG#7_2.TRD
// будет два файла "200D.3   FF00 787A 8000 0000 FF 00"
// и еще куча других.

// Подсчитывает кол-во нодов (среди нодов от 0 до nodeN-1) для которых
// GetName возвращает то же, что и для nodes[nodeN].
Result CalcDuplicates(List<Entry*>& nodes, uint nodeN, uint* result, CONFIG& cfg)
{
	uint count = 0;
	Entry* n1 = nodes[nodeN];
	char name1[260];
	Result res = GetName(*n1, name1, cfg, false);
	if (!res) {
		return res;
	}
	
	char name2[260];
	for(uint i = 0; i < nodeN; i++)
	{
		Entry* n2 = nodes[i];
		res = GetName(*n2, name2, cfg, false);
		if (!res) {
			return res;
		}

		if (streq_ci(name1, name2)) {
			count++;
		}
	}
	*result = count;
	return SUCCESS;
}

Result CalcDuplicates(List<Entry*>& nodes, CONFIG& cfg)
{
	for(uint i=0; i < nodes.Count; i++) {
		Result res = CalcDuplicates(nodes, i, &nodes[i]->DuplicateN, cfg);
		if (!res) {
			return res;
		}
	}
	return SUCCESS;
}


EXPORTED HANDLE STDCALL OpenArchiveW (tOpenArchiveDataW *ArchiveData)
{
	Random_Feed(ArchiveData, sizeof(*ArchiveData));

	Result res = RESULT_NO_MEM;
	ArcHandle* arc = new ArcHandle();
	if (!arc) {
		goto oa_error;
	}
	arc->Cfg = GetPrimaryConfig();
	arc->OpenMode = ArchiveData->OpenMode;

	if (strlen_(ArchiveData->ArcName) + 1 > ARRAY_LEN(arc->ArcName))
	{
		res = Result("Archive path too long", true);
		goto oa_error;
	}
	strcpy_(arc->ArcName, ArchiveData->ArcName);

	res = arc->Open(ArchiveData->ArcName, false, OPEN_EXISTING);
	if (!res) {
		goto oa_error;
	}

	res = TRD_FillEntries(*arc);
	if (!res) {
		goto oa_error;
	}

	if (arc->Cfg.AutoJoinLargeFiles())
	{
		// Add fictitious entries for big (compound) files
		if (!(res = AutojoinFiles(*arc)))
			goto oa_error;
	}

	// Precalc [n] suffixes for files with same names
	res = CalcDuplicates(arc->Entries, arc->Cfg);
	if (!res) {
		goto oa_error;
	}

	arc->CurrentEntry = 0;

	ArchiveData->OpenResult = 0;
	return arc;
	
oa_error:

	delete arc;	// Close вызовется деструктором

	res.ShowMessageIfNeed(NULL);
	ArchiveData->OpenResult = res.WcxErrorCode();
	return NULL;
};

EXPORTED int STDCALL CloseArchive (HANDLE hArcData)
{
	ArcHandle* arc = (ArcHandle*)hArcData;
	Result res = arc->Close();
	delete arc;
	//res.ShowMessageIfNeed(NULL);	// no sense in displaying error message here
	return res.WcxErrorCode();
};

EXPORTED int STDCALL ReadHeaderExW (HANDLE hArcData, tHeaderDataExW *HeaderData)
{
	ArcHandle* arc = (ArcHandle*)hArcData;
	if (arc->CurrentEntry >= arc->Entries.Count) {
		return E_END_ARCHIVE;
	}
	Entry* e = arc->Entries[arc->CurrentEntry];

	clear_struct(*HeaderData);
	strcpy_(HeaderData->ArcName, arc->ArcName);
	HeaderData->FileTime = arc->ArchiveDateTC;
	HeaderData->PackSize = arc->Cfg.ShowSizeInSectors ? e->SizeInSectors : e->SizeInSectors * 256;
	HeaderData->UnpSize = arc->Cfg.ShowSizeInSectors ? e->SizeInSectors : e->Size();

	char name[260];
	Result res = GetName(*e, name, arc->Cfg, true);
	if (res)
	{
		res = Convert_A_to_W(name, HeaderData->FileName, ARRAY_LEN(HeaderData->FileName))
			? SUCCESS
			: Result("Error reading archive headers", true);
	}
	
	if (!res)
	{
		res.ShowMessageIfNeed(NULL);
		return res.WcxErrorCode();
	}

	return 0;
};

EXPORTED int STDCALL ProcessFileW (HANDLE hArcData, int Operation, WCHAR *DestPath, WCHAR *DestName)
{
	Result res = SUCCESS;

	ArcHandle& arc = *(ArcHandle*)hArcData;
	Entry& e = *arc.Entries[arc.CurrentEntry];

	if (arc.OpenMode == PK_OM_EXTRACT) // open for extracting/testing
	{
		// Подсчёт и проверку КС надо делать для всех файлов подряд, 
		// не только для тестируемых/распаковываемых.
		// Для TRD не актуально.
	}
	else // PK_OM_LIST
	{
		if (Operation != PK_SKIP) { // should not happen
			res = Result("Invalid operation (code PF6)", true); goto k9;
		}
	}

	if (Operation != PK_SKIP)
	{
		if (DestPath != NULL) // TC never uses this param
		{
			Result::ShowErrorMessage(NULL, "DestPath is not null.\nPlease report this error to plugin author.");
			return E_NOT_SUPPORTED;
		}
		
		// Test file.
		// TODO: пофиксить операцию тестирования архива так, чтобы файлы, найденные расширенным алгоритмом
		// поиска удалённых файлов, не тестировались (см. коммент в TRD_FillEntries)
		bool readable = TRD_CheckFileReadable(&arc, &e);
		if (!readable) {
			res = Result(E_BAD_DATA); goto k9;
		}

		if (Operation == PK_EXTRACT)
		{
			FILETIME createDate = {0,0};
			if (e.CreateDate.IsValid()) {
				createDate = e.CreateDate.MakeFiletime();
			}

			FileHandle out;
			res = out.Open_DeleteOnClose(DestName, true, CREATE_NEW, true);
			if (e.HobetaHeaderProvided)
			{
				if (res) res = out.Write(&e.HobetaHeader, 17) ? SUCCESS : Result(E_EWRITE);
			}
			if (res) res = out.Write(arc.Adr() + e.FileDataPos, e.FileDataSize) ? SUCCESS : Result(E_EWRITE);
			if (res) res = SetFileModificationAndCreationDate(out.Handle(), arc.ArchiveDateUtc, createDate) ? SUCCESS : Result(E_EWRITE);
			if (!out.Close(res.IsSuccess())) res &= Result(E_EWRITE);
			if (!res) {
				goto k9;
			}
		}
	}

	res = SUCCESS;

k9:
	arc.CurrentEntry++; // proceed to next file
	res.ShowMessageIfNeed(NULL);
	return res.WcxErrorCode();
}


// forward declaration
int deleteFiles (const WCHAR *PackedFile, const WCHAR *DeleteList, bool forceDefragment);

// Check if user tries to make dir named "move" inside TRD image
bool IsDefragCommand(const WCHAR *addList, int flags)
{
	// Check that "Pack path names" option is specified
	if (!(flags & PK_PACK_SAVE_PATHS)) {
		return false;
	}

	// Check that AddList contains exactly one item
	const WCHAR* end = addList;
	if (*end == 0) {
		return false; // empty AddList
	}
	while(*end++) {} // move to next item
	if (*end != 0) {
		// more than one item in AddList
		return false;
	}

	// Check that dir name is "move"
	end--; // 0
	end--; if (end < addList || !IsSlash(*end)) return false;
	end--; if (end < addList || *end != 'e') return false;
	end--; if (end < addList || *end != 'v') return false;
	end--; if (end < addList || *end != 'o') return false;
	end--; if (end < addList || *end != 'm') return false;
	end--; if (end >= addList && !IsSlash(*end)) return false;
	return true;
}

EXPORTED int STDCALL PackFilesW (const WCHAR *PackedFile, const WCHAR *SubPath_notused, WCHAR *SrcPath, WCHAR *AddList, int Flags)
{
	if (IsDefragCommand(AddList, Flags))
	{
		// Special case: if user makes dir named "move" inside TRD image, do defragment image (tr-dos MOVE command).
		// DeleteFiles function with empty DeleteList will do the job.
		// NOTE: в TC 7.55- не будет работать, т.к. в AddList не будет слэша на конце,
		// а убрать требование наличия там слэша нельзя, ведь юзер может захотеть
		// запаковать ФАЙЛ с таким именем.
		int res2 = deleteFiles(PackedFile, L"\0", true);
		if (res2 == 0) {
			// delete "move" dir if requested by TC
			if (Flags & PK_PACK_MOVE_FILES) {
				DeletePackedFiles<true>(AddList, SrcPath);
			}
		}
		return res2;
	}

	Result res = SUCCESS;
	ArcHandle arc;
	arc.Cfg = GetPrimaryConfig();
	String<WCHAR> tempPath;
	FileHandle tempArc;
	bool deleteTempArc = false;

	if (!(res = arc.Open(PackedFile, true, OPEN_ALWAYS))) {
		goto pf_error;
	}
	bool Adding = !arc.File.Created; // true = modify existing archive; false = create new archive

	if (Adding)
	{
		// Adding to existing archive.
		// Have to test it first, because if it's not ok, we corrupt it even more.
		// Simulating "Test archive" TC command here.

		res = TRD_FillEntries(arc);

		// Autojoin здесь не делаем, тестировать фиктивные файлы ни к чему.
		
		if (res)
		{
			res = CalcDuplicates(arc.Entries, arc.Cfg);
		}
		
		if (res)
		{
			arc.OpenMode = PK_OM_EXTRACT;
			tHeaderDataExW headerData;
			int t;
			while (0 == (t = ReadHeaderExW(&arc, &headerData))) {
				if (0 != (t = ProcessFileW(&arc, PK_TEST, NULL, NULL)))
					break;
			}
			if (t != E_END_ARCHIVE) {
				res = FAIL;
			}
		}
		
		if (arc.ProtectionDetected) {
			res = FAIL;
		}

		if (!res)
		{
			res = Result(E_BAD_ARCHIVE);
			res.SetMessage("TRD image is corrupted or protected and cannot be modified.", true);
			goto pf_error;
		}
	}

	if (Adding)
	{
		// To be on a safe side, we write modified version of image to a temp file.
		// After operation succeeds, replace original image with temp file.
		res = CreateTempFile<WCHAR>(PackedFile, tempPath, tempArc);
		if (!res) {
			goto pf_error;
		}
		deleteTempArc = true;
	
		// Set creation date. We want image file creation date unchanged.
		SetFileTime(tempArc.Handle(), &arc.ArchiveCreateDateUtc, NULL, NULL);
	}

	FileHandle& newArc = Adding ? tempArc : arc.File;
	res = TRD_PackFiles(Adding, arc, newArc, SrcPath, AddList, arc.Cfg);
	if (!res) {
		goto pf_error;
	}

	if (Adding) {
		if (!(res = tempArc.Close()))
			goto pf_error;
	}

	if (!(res = arc.Close())) {
		goto pf_error;
	}

	if (Adding)
	{
		// Replace original image with temp file
		if (!MoveFileEx(tempPath.GetPtr(), PackedFile, MOVEFILE_REPLACE_EXISTING)) {
			res = Result(E_EWRITE);
			goto pf_error;
		}
		deleteTempArc = false;
	}

	res = SUCCESS;


pf_error:

	tempArc.Close(); // it's ok to Close() twice
	if (deleteTempArc) {
		DeleteFile(tempPath.GetPtr());
	}

	bool fileCreated = arc.File.Created;
	bool close_ok = arc.Close();
	if (!res && fileCreated) {
		// Creating new image has failed. Delete incomplete image.
		DeleteFile(PackedFile);
	}
	if (res && !close_ok) {
		res = Result(E_ECLOSE);
	}

	if (Adding) {
		// Здесь по идее надо обновлять дату архива с помощью UpdateFileWriteTime2,
		// но по какой-то причине TC сам догадывается обновить панель, даже если ни даты, 
		// ни размер архива не изменились. При удалении файлов из архива так почему-то
		// не происходит.
	}

	if (res && (Flags & PK_PACK_MOVE_FILES))
	{
		// User has picked "Move to archive" option
		DeletePackedFiles<true>(AddList, SrcPath);
	}

	res.ShowMessageIfNeed(NULL);
	return res.WcxErrorCode();
};

EXPORTED int STDCALL DeleteFilesW (const WCHAR *PackedFile, const WCHAR *DeleteList)
{
	return deleteFiles(PackedFile, DeleteList, false);
}

int deleteFiles(const WCHAR *PackedFile, const WCHAR *DeleteList, bool forceDefragment)
{
	Result res = SUCCESS;
	ArcHandle arc;
	arc.Cfg = GetPrimaryConfig();
	String<WCHAR> tempPath;
	FileHandle tempArc;
	bool deleteTempArc = false;

	bool defragmentImage = arc.Cfg.DefragmentOnDelete || forceDefragment;

	// We don't use temp archive in deleting files from TRD.
	// Because this operation writes first 4096 bytes only.
	// However, if DefragmentOnDelete is on, gotta back it with temp file
	// because we are modifying the whole image.
	bool useTempFile = defragmentImage;

	if (!(res = arc.Open(PackedFile, !useTempFile, OPEN_EXISTING))) {
		goto df_error;
	}

	// Populate arc->Entries

	res = TRD_FillEntries(arc);
	if (!res) {
		goto df_error;
	}

	if (arc.Cfg.AutoJoinLargeFiles()) {
		if (!(res = AutojoinFiles(arc)))
			goto df_error;
	}

	res = CalcDuplicates(arc.Entries, arc.Cfg);
	if (!res) {
		goto df_error;
	}

	// Have to test it first, because if it's not ok, we corrupt it even more.
	// Simulating "Test archive" TC command here.
	{
		arc.OpenMode = PK_OM_EXTRACT;
		tHeaderDataExW headerData;
		int t;
		while (0 == (t = ReadHeaderExW(&arc, &headerData))) {
			if (0 != (t = ProcessFileW(&arc, PK_TEST, NULL, NULL)))
				break;
		}
		if (t != E_END_ARCHIVE) {
			res = FAIL;
		}
		
		if (arc.ProtectionDetected) {
			res = FAIL;
		}

		if (!res)
		{
			res = Result(E_BAD_ARCHIVE);
			res.SetMessage("TRD image is corrupted or protected and cannot be modified.", true);
			goto df_error;
		}
	}

	if (defragmentImage)
	{
		// To be on a safe side, we write modified version of image to a temp file.
		// After operation succeeds, replace original image with temp file.
		res = CreateTempFile(PackedFile, tempPath, tempArc);
		if (!res) {
			goto df_error;
		}
		deleteTempArc = true;

		// Gotta keep archive creation date.
		// Gotta update file modification date to force update TC panel.
		// (Последнее обязательно, я проверил, без этого TC не обновляет панель)
		FILETIME newFileModTimeUtc = MakeDifferentFileTime(arc.ArchiveDateUtc);
		SetFileTime(tempArc.Handle(), &arc.ArchiveCreateDateUtc, NULL, NULL/*&newFileModTimeUtc*/);

		res = TRD_DeleteFiles_WithDefragment(arc, DeleteList, tempArc, arc.Cfg);
	}
	else
	{
		res = TRD_DeleteFiles(arc, DeleteList, arc.Cfg);
	}

	if (useTempFile) {
		if (!(res = tempArc.Close()))
			goto df_error;
	}

	if (!useTempFile)
	{
		// Since we use memory mapping, have to explicitly update archive modification date.
		// И нужно использовать именно UpdateFileWriteTime2, чтобы гарантировать, что TC обновит панель,
		// т.к. размер архива не меняется.
		res = UpdateFileWriteTime2(arc.File.Handle(), arc.ArchiveDateUtc) ? SUCCESS : Result(E_EWRITE);
		if (!res) {
			goto df_error;
		}
	}

	if (!(res = arc.Close())) {
		goto df_error;
	}

	if (useTempFile)
	{
		// Replace original image with temp file.
		// File creation and write dates are taken from temp file.
		if (!MoveFileEx(tempPath.GetPtr(), PackedFile, MOVEFILE_REPLACE_EXISTING)) {
			res = Result(E_EWRITE); goto df_error;
		}
		deleteTempArc = false;
	}

df_error:

	tempArc.Close(); // it's ok to Close() twice
	if (deleteTempArc) {
		// Operation failed. Delete incomplete temp image.
		DeleteFile(tempPath.GetPtr());
	}

	res &= arc.Close();

	res.ShowMessageIfNeed(NULL);
	return res.WcxErrorCode();
}

EXPORTED int STDCALL GetPackerCaps()
{
	return 0
		| PK_CAPS_NEW
		| PK_CAPS_MODIFY
		| PK_CAPS_MULTIPLE
		| PK_CAPS_DELETE
		| PK_CAPS_OPTIONS
		//| PK_CAPS_MEMPACK
		//| PK_CAPS_BY_CONTENT
		//| PK_CAPS_SEARCHTEXT
		//| PK_CAPS_HIDE
		//| PK_CAPS_ENCRYPT
		;
}

EXPORTED void STDCALL SetChangeVolProcW (HANDLE hArcData, ::tChangeVolProcW pChangeVolProc)
{}

EXPORTED void STDCALL SetProcessDataProcW (HANDLE hArcData, ::tProcessDataProcW pProcessDataProc)
{}

BOOL WINAPI DllMain (HANDLE hModule, DWORD reason, LPVOID reserv) 
{
	if (sizeof(TRD_SystemSector) != 256) { // compiler settings check
		_throw(0);
	}

	if (reason == DLL_PROCESS_ATTACH) 
	{
		dllInstance = (HINSTANCE)hModule;
		InitConfig(hModule);
	}
	else if (reason == DLL_PROCESS_DETACH) 
	{
	}

	return TRUE;
}

