﻿
// Pseudo Random Number Generator.
// Генератор псевдослучайных чисел.
// Достаточно простой, криптографической стойкостью не озабоченный.

#pragma once

#include "..\common.h"

// initialize generator seed
extern void Random_Feed();

// initialize generator seed using provided bytes array
extern void Random_Feed(void* data, size_t size);

// get random 32 bits
extern uint Random_Next();

// get random uint from [0; N) range
extern uint Random_Next(uint N);

// get random bytes
extern void Random_Next(void* output, size_t count);

// get random 64 bits
extern uint64 Random_Next64();

// get random uint64 from [0; N) range
extern uint64 Random_Next64(uint64 N);

// get random size_t from [0; N) range
extern size_t Random_NextT(size_t N);
