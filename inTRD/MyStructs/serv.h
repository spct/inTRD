﻿
/** Miscellaneous general purpose helpers and types **/

#pragma once

#include "..\common.h"
//#include <stdio.h>

#define EXPORTED __declspec(dllexport) 
#define STDCALL __stdcall
// Надо проверять, что EXPORTED всегда сопровождается STDCALL

typedef unsigned int uint;
typedef __int64 int64;
typedef unsigned __int64 uint64;
typedef unsigned short ushort;
typedef signed char sbyte;

#ifdef _WIN64
	typedef signed __int64 ssize_t;
#else
	typedef signed int ssize_t;
#endif

__forceinline uint HI_DWORD(uint64 a) { return (uint)(a >> 32); }
__forceinline int HI_DWORD(__int64 a) { return (int)(a >> 32); }
__forceinline uint& HI_DWORD_REF(uint64& a) { return ((uint*)&a)[1]; }
__forceinline int& HI_DWORD_REF(__int64& a) { return ((int*)&a)[1]; }

// Обращаем внимание, что выражение (a) вычисляется дважды!
#define InRange(a, min, max) (((a) >= (min)) && ((a) <= (max)))

//#define ARRAY_LEN(a) (sizeof(a) / sizeof((a)[0]))
#define ARRAY_LEN(a) _ARRAYSIZE(a)
#define ARRAY_LEN_FIELD(type, field)  ARRAY_LEN( ((type*)0)->field )

// copy-assign array or struct, checking their size
#define COPY(a,b) \
{ \
	__pragma(warning(push)) \
	__pragma(warning(disable:4804)) \
	memcpy_((a), (b), sizeof(b) / (sizeof(b) == sizeof(a))); \
	__pragma(warning(pop)) \
}

// copy-assign given part of array, checking item size
#define COPY_ARR(a,b,count) \
{ \
	__pragma(warning(push)) \
	__pragma(warning(disable:4804)) \
	memcpy_((a), (b), count * sizeof((b)[0]) / (sizeof((b)[0]) == sizeof((a)[0]))); \
	__pragma(warning(pop)) \
}

#define LAST(arr) ((arr)[ARRAY_LEN(arr) - 1])

#define SIZE_OF(type, field) RTL_FIELD_SIZE(type, field)

extern const char HexChars_Up[17];
extern const char HexChars_Low[17];

#define ASSIGNMENT_OPERATOR(T) T& operator= (T& value) { memcpy_(this, &value, sizeof(*this)); return *this; }

#define memeq(a, b, sz) (memcmp_((a), (b), (sz)) == 0)

// chars prohibited in file name; includes zero.
extern const char InvalidFileNameChars[41];
// chars prohibited in file path; includes zero.
extern const char InvalidPathChars[39];

template<typename TCHR>
bool IsSlash(TCHR c) { return c == '/' || c == '\\'; }

template<class TCHR>
inline bool streq(const TCHR* str1, const TCHR* str2)
{
	return (strcmp_(str1, str2) == 0);
}

template<class TCHR>
inline TCHR* End(TCHR* str) { return &str[strlen_(str)]; }

template<typename TCHR>
TCHR ToLower(TCHR c) { return !InRange(c, 'A', 'Z') ? c : c - 'A' + 'a'; }

// case-insensitive string comparison for equality
template<typename TCHR>
bool streq_ci(const TCHR* s1, const TCHR* s2)
{
	TCHR c1, c2;
	do {
		c1 = ToLower(*s1++);
		c2 = ToLower(*s2++);
		if (c1 != c2) {
			return false;
		}
	} while(c1);
	return true;
};

template<typename TCHR>
bool ends_with(const TCHR* s, const TCHR* with)
{
	size_t sl = strlen_(s);
	size_t wl = strlen_(with);
	if (wl > sl)
		return false;
	else
		return memeq(&s[sl - wl], with, wl * sizeof(s[0]));
};

template<typename TCHR>
bool ends_with_ci(const TCHR* s, const TCHR* with)
{
	size_t sl = strlen_(s);
	size_t wl = strlen_(with);
	if (wl > sl)
		return false;
	else
		return streq_ci(&s[sl - wl], with);
};

extern bool is_clear(char* buf, size_t count);

#define this_is_clear() (is_clear((char*)this, sizeof(*this)))

template<class TCHR>
void utoa64(uint64 a, TCHR* buf)
{
	byte st[25];
	size_t sp = 0;
	do {
		st[sp++] = a % 10u;
		a /= 10u;
	} while(a != 0);
		
	do {
		*buf++ = st[--sp] + '0';
	} while(sp != 0);

	*buf = 0;
}

// convert integer to string, splitting decimal groups with spaces (eg. 12345 -> "12 345")
template<class TCHR>
void utoa64_formatted(uint64 a, TCHR* buf)
{
	byte st[30];
	size_t sp = 0;
	uint gs = 0;
	do {
		if (gs == 3) {
			st[sp++] = ' ';
			gs = 0;
		}
		st[sp++] = (byte)(a % 10u) + '0';
		a /= 10u;
		gs++;
	} while(a != 0);
		
	do {
		*buf++ = st[--sp];
	} while(sp != 0);

	*buf = 0;
}

template<class TCHR>
void itoa64(int64 a, TCHR* buf)
{
	if (a < 0) {
		*buf++ = '-'; a = -a;
	}
	utoa64((uint64)a, buf);
}

#ifdef _WIN64
	#include "serv64.h"
#else

	static void utoa(unsigned int a, char* buf)
	{
		__asm
		{
			mov eax,a
			xor ecx,ecx
			lea edi,[ecx+10]
	L0:
			inc ecx
			xor edx,edx
			div edi
			push edx
			test eax,eax
			jnz L0

			mov edi,buf
	L1:
			pop eax
			add al,48
			mov [edi],al
			inc edi
			loop L1
		
			mov byte ptr [edi],cl
		}
	}

	static void utoa(unsigned int a, WCHAR* buf)
	{
		__asm
		{
			mov eax,a
			xor ecx,ecx
			lea edi,[ecx+10]
	L0:
			inc ecx
			xor edx,edx
			div edi
			push edx
			test eax,eax
			jnz L0

			mov edi,buf
	L1:
			pop eax
			add al,48
			mov [edi],ax
			inc edi
			inc edi
			loop L1
		
			mov [edi],cx
		}
	}

	static void utoa0(unsigned int a, char* buf, size_t digitCount)
	{
		__asm
		{
			mov eax, a
			mov ecx, digitCount
			mov ebx, buf
			add ebx, ecx
			mov byte ptr [ebx], 0       // terminating zero
			mov edi,10
	L0:
			xor edx,edx
			div edi
			add dl, 48
			dec ebx
			mov [ebx], dl
			loop L0
		}
	}

	static void utoa0(unsigned int a, WCHAR* buf, size_t digitCount)
	{
		__asm
		{
			mov eax, a
			mov ecx, digitCount
			mov ebx, buf
			add ebx, ecx
			add ebx, ecx
			mov word ptr [ebx], 0       // terminating zero
			mov edi,10
	L0:
			xor edx,edx
			div edi
			add dl, 48
			dec ebx
			dec ebx
			mov [ebx], dx
			loop L0
		}
	}

	static void utoa0_hex(unsigned int a, char* buf, size_t digitCount, const char* chars)
	{
		__asm
		{
			mov ecx, digitCount
			mov edx, buf
			add edx, ecx
			mov byte ptr [edx], 0       // terminating zero
	L0:
			mov eax, a
			and eax,15
			add eax,chars
			mov al,[eax]
			dec edx
			mov [edx], al
			shr a,4
			loop L0
		}
	}

	static void itoa(int a, char* buf)
	{
		__asm
		{
			mov eax,a
			xor ecx,ecx
			lea edi,[ecx+10]
			cdq					//
			xor eax,edx			// abs
			sub eax,edx			//
	L0:
			inc ecx
			xor edx,edx
			idiv edi
			push edx
			test eax,eax
			jnz L0

			mov edi,buf
			add eax,a		// eax=a
			jns L1
			mov byte ptr [edi],'-'
			inc edi
	L1:
			pop eax
			add al,48
			mov [edi],al
			inc edi
			loop L1
		
			mov [edi],cl
		}
	}

	static void itoa(int a, WCHAR* buf)
	{
		__asm
		{
			mov eax,a
			xor ecx,ecx
			lea edi,[ecx+10]
			cdq					//
			xor eax,edx			// abs
			sub eax,edx			//
	L0:
			inc ecx
			xor edx,edx
			idiv edi
			push edx
			test eax,eax
			jnz L0

			mov edi,buf
			add eax,a		// eax=a
			jns L1
			mov word ptr [edi],'-'
			inc edi
			inc edi
	L1:
			pop eax
			add al,48
			mov [edi],ax
			inc edi
			inc edi
			loop L1
		
			mov [edi],cx
		}
	}

	template<typename TCHR>
	static void utoa_t(size_t a, TCHR* buf)
	{
		utoa(a, buf);
	}

#endif

// Show messagebox, throw unhandled exception, exit process.
extern __declspec(noreturn) void _throw(char* msg);

static __declspec(noreturn) void error(char *txt) 
{
	MessageBoxA(0, txt, 0, MB_APPLMODAL);
	ExitProcess(1);
}

static __declspec(noreturn) void error() 
{
	ExitProcess(GetLastError());
}


