﻿
/*
 * Helpers for working with *.ini files
 */

#pragma once

#include "..\common.h"
#include "Result.h"

extern Result ConfigFile_SaveStruct(TCHAR* filename, TCHAR* appName, TCHAR* keyName, void* pStruct, size_t structSize);
extern Result ConfigFile_SaveBool(TCHAR* filename, TCHAR* appName, TCHAR* keyName, bool val);
extern Result ConfigFile_SaveUint(TCHAR* filename, TCHAR* appName, TCHAR* keyName, uint val);

extern uint ConfigFile_LoadUint(TCHAR* filename, TCHAR* appName, TCHAR* keyName, uint def_val);
extern bool ConfigFile_LoadBool(TCHAR* filename, TCHAR* appName, TCHAR* keyName, bool def_val);

template<class TSTRUCT>
TSTRUCT ConfigFile_LoadStruct(TCHAR* filename, TCHAR* appName, TCHAR* keyName, const TSTRUCT& def_val)
{
	TSTRUCT buf;
	bool res = (0 != GetPrivateProfileStruct(appName, keyName, &buf, sizeof(TSTRUCT), filename));
	if (!res) {
		return def_val;
	}
	return buf;
};

