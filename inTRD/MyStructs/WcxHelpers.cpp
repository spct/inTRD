﻿
#include "WcxHelpers.h"
#include "..\MyStructs\String.h"


// path must not end with slash!
bool IsDirectory(const TCHAR* path)
{
	WIN32_FIND_DATA fd;
	HANDLE h = FindFirstFile(path, &fd);
	if (h == INVALID_HANDLE_VALUE) {
		return false; // not found
	}
	FindClose(h);
	return (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
}

// path must not end with slash!
bool DirectoryExists(const TCHAR* path)
{
	return IsDirectory(path);
}


FILETIME NowUtc()
{
	FILETIME t;
	GetSystemTimeAsFileTime(&t);
	return t;
}

bool NowLocal(FILETIME* out_time)
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	return (0 != SystemTimeToFileTime(&st, out_time));
}

// Returns 0 on error
DWORD NowDosLocal()
{
	FILETIME utc = NowUtc();
	return Utc2DosLocal(utc);
};


bool SetFileModificationDate(HANDLE hFile, FILETIME time, bool timeIsUtc)
{
	if (!timeIsUtc)	{
		FILETIME temp = time;
		if (!LocalFileTimeToFileTime(&temp, &time)) {	// convert local->UTC
			return false;
		}
	}
	return (0 != SetFileTime(hFile, NULL, NULL, &time));
}

bool GetFileModificationDateUtc(HANDLE hFile, FILETIME* out_time)
{
	return (0 != GetFileTime(hFile, NULL, NULL, out_time));
}

// Returns 0 on error
DWORD Utc2DosLocal(FILETIME utc)
{
	FILETIME local;
	DWORD dostime;
	if (!FileTimeToLocalFileTime(&utc, &local)) return 0;
	if (!FileTimeToDosDateTime(&local, ((WORD*)&dostime)+1, (WORD*)&dostime)) return 0;
	return dostime;
};

// Returns 0 on error
DWORD GetFileModificationDateDosLocal(HANDLE hFile)
{
	FILETIME utc;
	if (!GetFileModificationDateUtc(hFile, &utc)) {
		return 0;
	}
	return Utc2DosLocal(utc);
};

bool SetFileModificationDateDosLocal(HANDLE hFile, DWORD filetimeLocal)
{
	FILETIME local;
	return DosDateTimeToFileTime(HIWORD(filetimeLocal), LOWORD(filetimeLocal), &local)
		&& SetFileModificationDate(hFile, local, false);
};


// Нужно ли явно обновлять дату архива? Если да, то какую из следующих двух функций
// нужно использовать? Чтобы ответить на эти вопросы, нужно знать следующее:
// - Total Commander считает архив изменившимся, если у него изменился размер или время модификации.
// - У FAT32 точность хранения даты записи: 2 сек. (например, если архив TRD изменить дважды
//   в течение секунды, то после второго изменения панель TC не обновится!)
// - При изменении файла посредством механизма memory mapping его дата не изменяется!

FILETIME MakeDifferentFileTime(FILETIME time)
{
	uint64 oldTime = *(uint64*)&time;
	uint fs_precision = (oldTime % 10000000u == 0)  // 1 sec or worse precision?
		? 20000000	// assuming it's FAT32 whose precision is 2 sec
		: 1;		// assuming it's NTFS whose precision is 0.1 microsec

	// Нужно установить файлу время, удовлетворяющее следующим условиям:
	// - оно должно укладываться в возможности файловой системы;
	// - оно должно отличаться от oldTime;
	// - оно должно отличаться от now как можно меньше;
	// - (не беда если оно будет меньше чем oldTime, в конце концов мы хотим 
	//   присвоить текущее время; тем более что oldTime в принципе может быть 
	//   вообще из будущего);

	FILETIME t;
	GetSystemTimeAsFileTime(&t);
	uint64 now = *(uint64*)&t;

	uint m = uint(now % fs_precision);
	uint64 t1 = now - m;
	uint64 t2 = t1 + fs_precision;

	uint64 newTime = (t1 == oldTime || t2 != oldTime && t2 - now <= now - t1) ? t2 : t1;
	return *(FILETIME*)&newTime;
};

bool UpdateFileWriteTime(HANDLE hFile)
{
	FILETIME t;
	GetSystemTimeAsFileTime(&t);
	return (0 != SetFileTime(hFile, NULL, NULL, &t));
};

// Эта версия в отличие от предыдущей гарантирует, что дата файла изменится.
// Эту функцию следует использовать, если есть шанс, что изменяемый файл находится 
// на файловой системе FAT(32).
// origModTime: дата файла на момент начала работы с ним.
bool UpdateFileWriteTime2(HANDLE hFile, FILETIME origModTime)
{
	FILETIME newTime = MakeDifferentFileTime(origModTime);
	return (0 != SetFileTime(hFile, NULL, NULL, &newTime));
};



ProgressReporter::ProgressReporter(tProcessDataProcW* processDataProc_varPtr)
{
	this->ProcessDataProc = *processDataProc_varPtr;
	*processDataProc_varPtr = NULL;
};

bool ProgressReporter::Report(int64 done, WCHAR* filename)
{
    if (ProcessDataProc == NULL) {
		return false;
	}

    done = max(done, 0);

    for (; done > INT_MAX; done -= INT_MAX) ProcessDataProc(NULL, INT_MAX);
    bool cancellationPending = (ProcessDataProc(filename, (int)done) == 0);
	return cancellationPending;
};

bool ProgressReporter::SetProgress(int firstIndicatorPercents, int secondIndicatorPercents, WCHAR* filename)
{
    if (ProcessDataProc == NULL) { 
		return false;
	}

    // See comment in ProcessDataProcW definition.

    firstIndicatorPercents = min(max(firstIndicatorPercents, 1), 100);    // do not allow zero. Zero 'size' arg will not be treated as percentage.
    secondIndicatorPercents = min(max(secondIndicatorPercents, 0), 100);

    // first, we query cancellation status (this will also change progressbar),
    // then set new progressbar value. (это сделано из-за бага в TC, см. мои комменты в WrapperCore.cs)
    // Yes, it may blink, but it's hard to notice.

    bool cancellationPending = (ProcessDataProc(NULL, 0) == 0);
    ProcessDataProc(NULL, -firstIndicatorPercents);                   // first percent bar
    ProcessDataProc(filename, -1000 - secondIndicatorPercents);       // second percent bar

	return cancellationPending;
};
