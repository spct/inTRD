﻿
#include "EncodingHelpers.h"

bool Convert_W_to_A(
	const WCHAR* input, // zero-terminated string to convert
	char* out_buf,		// output buffer
	size_t out_size		// size (in bytes) of output buffer including zero terminator
	)
{
	if ((int)out_size != out_size) { // prevent integer overflow
		return false;
	}

	// First get required buffer size
	CHAR def = '?';
	int req_size = WideCharToMultiByte(CP_ACP, WC_NO_BEST_FIT_CHARS, input, -1, out_buf, 0, &def, NULL);
	if (req_size < 0 || req_size > (int)out_size) {
		return false; // output buffer is not large enough
	}

	int res = WideCharToMultiByte(CP_ACP, WC_NO_BEST_FIT_CHARS, input, -1, out_buf, (int)out_size, &def, NULL);
	// поскольку мы обрабатываем как минимум zero terminator, результат обязан быть положительным:
	if (res == 0) {
		return false; // some error
	}

	return true;
};

bool Convert_A_to_W(
	const char* input,	// zero-terminated string to convert
	WCHAR* out_buf,		// output buffer
	size_t out_size		// size (in characters) of output buffer including zero terminator
	)
{
	if ((int)out_size != out_size) { // prevent integer overflow
		return false;
	}

	// First get required buffer size
	int req_size = MultiByteToWideChar(CP_ACP, 0, input, -1, out_buf, 0);
	if (req_size < 0 || req_size > (int)out_size) {
		return false; // output buffer is not large enough
	}

	int res = MultiByteToWideChar(CP_ACP, 0, input, -1, out_buf, (int)out_size);
	// поскольку мы обрабатываем как минимум zero terminator, результат обязан быть положительным
	if (res == 0) {
		return false; // some error
	}

	return true;
};

bool Convert_W_to_Utf8(
	const WCHAR* input, // zero-terminated string to convert
	char* out_buf,		// output buffer
	size_t out_size		// size (in bytes) of output buffer including zero terminator
	)
{
	if ((int)out_size != out_size) { // prevent integer overflow
		return false;
	}

	// First get required buffer size
	int req_size = WideCharToMultiByte(CP_UTF8, 0, input, -1, out_buf, 0, NULL, NULL);
	if (req_size < 0 || req_size > (int)out_size) {
		return false; // output buffer is not large enough
	}

	int res = WideCharToMultiByte(CP_UTF8, 0, input, -1, out_buf, (int)out_size, NULL, NULL);
	// поскольку мы обрабатываем как минимум zero terminator, результат обязан быть положительным
	if (res == 0) {
		return false; // some error
	}

	return true;
};

bool Convert_Utf8_to_W(
	const char* input,	// zero-terminated string to convert
	WCHAR* out_buf,		// output buffer
	size_t out_size		// size (in characters) of output buffer including zero terminator
	)
{
	if ((int)out_size != out_size) { // prevent integer overflow
		return false;
	}

	// First get required buffer size
	int req_size = MultiByteToWideChar(CP_UTF8, 0, input, -1, out_buf, 0);
	if (req_size < 0 || req_size > (int)out_size) {
		return false; // output buffer is not large enough
	}

	int res = MultiByteToWideChar(CP_UTF8, 0, input, -1, out_buf, (int)out_size);
	// поскольку мы обрабатываем как минимум zero terminator, результат обязан быть положительным
	if (res == 0) {
		return false; // some error
	}

	return true;
};

