﻿
#include "FileHandle.h"

#ifdef FILEHANDLE_USE_CONSTRUCTOR

	FileHandle::FileHandle()
	{
		clear_this();
	};

	// Обращаем внимание, что деструктор вызывает Close,
	// причём если активирован режим DeleteOnClose, файл будет удалён
	// (имхо логично: если мы не успели записать всё что хотели, лучше его удалить)!
	FileHandle::~FileHandle()
	{
		if (CloseInDestructor) {
			Close_Core(false);
		}
		delete[] Path;
		clear_this();
	};

#endif

	// Sets File Handle, Size and Created fields.
	// If already opened, returns error.
	Result FileHandle::Open(const TCHAR* path, bool writeable, DWORD createDisposition, bool seq_scan, bool startMapping)
	{
		return Open(path, writeable, createDisposition, seq_scan, startMapping, /*MAXUINT64*/ULLONG_MAX, FAIL);
	};

	// Sets File Handle, Size and Created fields.
	// If already opened, returns error.
	Result FileHandle::Open(
		const TCHAR* path, 
		bool writeable, 
		DWORD createDisposition,
		bool seq_scan,
		bool startMapping,
		uint64 maxFileSize, Result onFilesizeExceed
		)
	{
		return Open_Core(path, writeable, createDisposition, seq_scan, startMapping, maxFileSize, onFilesizeExceed, false);
	};

	// Open file and configure FileHandle to delete file on closing.
	// Sets File Handle, Size and Created fields.
	// If already opened, returns error.
	// The file will be deleted using path from 'path' argument.
	// Make sure path represents absolute, not relative path;
	// also remember, that file may be deleted even if it was not actually created
	// (i.e. an existing one was opened ('Created' field is false)).
	Result FileHandle::Open_DeleteOnClose(const TCHAR* path, bool writeable, DWORD createDisposition, bool seq_scan)
	{
		if (createDisposition != CREATE_NEW && createDisposition != CREATE_ALWAYS)
		{
			// Страховка. Я пока не определился, что делать, если файл не был создан,
			// а был открыт существующий.
			return Result("Internal error (FileHandle opened with wrong disposition).\n"
						  "Please report to the author!", true);
		}
		return Open_Core(path, writeable, createDisposition, seq_scan, false, /*MAXUINT64*/ULLONG_MAX, FAIL, true);
	};


	// Core procedure opening file.
	// Sets Handle, Size and Created fields.
	// If already opened, returns error.
	Result FileHandle::Open_Core(
		const TCHAR* path, 
		bool writeable, 
		DWORD createDisposition,
		bool seq_scan,
		bool startMapping,
		uint64 maxFileSize, Result onFilesizeExceed,
		bool deleteOnClose
		)
	{

#ifdef FILEHANDLE_USE_CONSTRUCTOR
		if (Handle() != INVALID_HANDLE_VALUE || Mapping != NULL) {
			return Result("Error 2834 (FileHandle already opened)", true);
		}
#endif

		clear_this();

		#ifdef FILEHANDLE_WCX_SUPPORT
			Result errorOpening = Result(
				(createDisposition == CREATE_NEW || createDisposition == CREATE_ALWAYS) ? E_ECREATE : E_EOPEN
			);
			#define FILEHANDLE_OPEN_ERROR errorOpening
		#else
			#define FILEHANDLE_OPEN_ERROR Result(ErrorCode, true)
		#endif

		// Path backup, we may need it to delete file
		size_t pathLen = strlen_(path);
		if (!(Path = new TCHAR[pathLen + 1])) {
			return RESULT_NO_MEM;
		}
		strcpy_(Path, path);

		Created = false;
		HANDLE hf = CreateFile(path, 
			GENERIC_READ | (writeable ? GENERIC_WRITE : 0), 
			writeable ? 0 : FILE_SHARE_READ,
			NULL,
			createDisposition,
			seq_scan ? FILE_FLAG_SEQUENTIAL_SCAN : 0, 
			NULL
		);
		if (hf == INVALID_HANDLE_VALUE) { 
			GetOSErrorCode(); 
			return FILEHANDLE_OPEN_ERROR; 
		}
		inv_handle = ~(size_t)hf;

		Created = ((createDisposition == CREATE_ALWAYS || createDisposition == OPEN_ALWAYS) &&
			       GetLastError() != ERROR_ALREADY_EXISTS); 

		this->deleteOnClose = deleteOnClose;

		// Процесс открытия ещё толком не завершён, поэтому в режиме DeleteOnClose
		// в случае ошибки здесь мы удаляем файл только если он был создан.

		if (!GetSize(Size)) { 
			Close_Core(!Created); 
			return FILEHANDLE_OPEN_ERROR; 
		}
		if (Size > maxFileSize) { 
			Close_Core(!Created); 
			return onFilesizeExceed; 
		}

		Mapping = NULL;
		if (startMapping)
		{
			if (!OpenMapping(writeable)) { 
				Close_Core(!Created); 
				return FILEHANDLE_OPEN_ERROR; 
			}
		}

		#ifdef FILEHANDLE_USE_CONSTRUCTOR
			CloseInDestructor = true;
		#endif
		
		return SUCCESS;
	};

	// Core method performing closing/deleting file.
	// Closes (if opened) File and Mapping.
	// If handle was opened with DeleteOnClose and deleteOnClose_keep=false
	// then file is deleted, but only if it was actually created/overwritten
	// (in this mode we support CREATE_NEW and CREATE_ALWAYS dispositions only).
	Result FileHandle::Close_Core(bool deleteOnClose_keep)
	{
		bool ok = CloseMapping();
			
		if (Handle() != INVALID_HANDLE_VALUE)
		{
			if (!CloseHandle(Handle()))
			{
				GetOSErrorCode();
				ok = false;
			}
			inv_handle = 0;

			if (deleteOnClose && !deleteOnClose_keep)
			{
				if (!DeleteFile(Path))
				{
					GetOSErrorCode();
					ok = false;
				}
			}
		}

		deleteOnClose = false;
		Created = false;
		Size = 0;
	
		delete[] Path; Path = NULL;

		#ifdef FILEHANDLE_WCX_SUPPORT
			return ok ? SUCCESS : Result(E_ECLOSE);
		#else
			return ok ? SUCCESS : Result(ErrorCode, true);
		#endif
	};

	bool FileHandle::OpenMapping(bool writeable)
	{
		// Заметим, что если маппинг уже открыт, но не-writeable, а теперь попросили writeable,
		// то невелика печаль - просто caller свалится позже, при попытке записи в маппинг.
		if (IsMapped()) {
			return true;
		}

		// CreateFileMapping doesn't work for zero size files!
		// Прежде чем открывать mapping, надо точно знать, не равен ли нулю его размер.
		// Но попробуем лишний раз не вызывать GetFileSize.
		MappingSize = Size;
		bool sizeKnown = false;
	L0:
		if (MappingSize == 0)
		{
			if (!sizeKnown)
			{
				if (!GetSize(MappingSize)) {
					return false;
				}
				sizeKnown = true;
				goto L0;
			}
			Mapping = HANDLE(1); // fake non-null handle
			Adr = (byte*)1;
		}
		else
		{
			Mapping = CreateFileMapping(Handle(), 0, writeable ? PAGE_READWRITE : PAGE_READONLY, 0,0,0);
			if (!Mapping)
			{
				if (!sizeKnown)
				{
					if (!GetSize(MappingSize)) {
						return false;
					}
					sizeKnown = true;
					goto L0;
				}
				GetOSErrorCode();
			}
			Adr = (byte*)MapViewOfFile(Mapping, writeable ? FILE_MAP_WRITE : FILE_MAP_READ, 0,0,0);
			if (!Mapping || !Adr) { 
				GetOSErrorCode(); 
				CloseMapping(); 
				return false; 
			}
		}

		return true;
	};

	bool FileHandle::IsMapped()
	{
		return (Mapping != NULL);
	};

	// Closes (if opened) memory mapping, but doesn't close file handle.
	bool FileHandle::CloseMapping()
	{
		bool ok = true;
		if (Mapping != NULL)
		{
			if (MappingSize != 0 && Adr != NULL && !UnmapViewOfFile(Adr)) { 
				GetOSErrorCode(); 
				ok = false; 
			}
			Adr = NULL;
			if (MappingSize != 0 && !CloseHandle(Mapping)) { 
				GetOSErrorCode(); 
				ok = false; 
			}
			Mapping = NULL;
			MappingSize = 0;
		}
		return ok;
	};

	Result FileHandle::Close()
	{
		return Close_Core(true);
	}

	// Closes (if opened) File and Mapping.
	// Deletes file if deleteOnClose mode is on and deleteOnClose_keep=false.
	Result FileHandle::Close(bool deleteOnClose_keep)
	{
		return Close_Core(deleteOnClose_keep);
	}

	// Queries file size from OS but doesn't update Size variable!
	// Returns zero and false on error.
	bool FileHandle::GetSize(uint64& out_size)
	{
		bool ok = (0 != GetFileSizeEx(Handle(), (LARGE_INTEGER*)&out_size));
		if (!ok) { 
			GetOSErrorCode(); 
			out_size = 0; 
		}
		return ok;
	};

	bool FileHandle::Read(LPVOID lpBuffer, uint64 nNumberOfBytesToRead)
	{
		byte* ptr = (byte*)lpBuffer;
		while(nNumberOfBytesToRead != 0)
		{
			DWORD t = (DWORD)min(nNumberOfBytesToRead, 1U << 30);
			
			DWORD actuallyRead;
			bool ok = (0 != ReadFile(Handle(), ptr, t, &actuallyRead, NULL));
			ok &= (actuallyRead == t);
			
			if (!ok) { 
				GetOSErrorCode(); 
				return false; 
			}
			nNumberOfBytesToRead -= t;
			ptr += t;
		}
		return true;
	};

	bool FileHandle::Write(LPVOID lpBuffer, uint64 nNumberOfBytesToWrite)
	{
		byte* ptr = (byte*)lpBuffer;
		while(nNumberOfBytesToWrite != 0)
		{
			DWORD t = (DWORD)min(nNumberOfBytesToWrite, 1U << 30);

			DWORD actuallyWritten;
			bool ok = (0 != WriteFile(Handle(), ptr, t, &actuallyWritten, NULL));
			ok &= (actuallyWritten == t);

			if (!ok) { 
				GetOSErrorCode(); 
				return false; 
			}
			nNumberOfBytesToWrite -= t;
			ptr += t;
		}
		return true;
	};

	// Reads nNumberOfBytesToRead bytes, filling specified buffer cyclically
	bool FileHandle::TestRead(LPVOID lpBuffer, size_t bufSize, uint64 nNumberOfBytesToRead)
	{
		bufSize = min(bufSize, 1U << 30);
		while(nNumberOfBytesToRead != 0)
		{
			DWORD t = (DWORD)min(nNumberOfBytesToRead, bufSize);

			DWORD actuallyRead;
			bool ok = (0 != ReadFile(Handle(), lpBuffer, t, &actuallyRead, NULL));
			ok &= (actuallyRead == t);

			if (!ok) { 
				GetOSErrorCode(); 
				return false; 
			}
			nNumberOfBytesToRead -= t;
		}
		return true;
	};

	bool FileHandle::GetPosition(uint64& out_pos)
	{
		LARGE_INTEGER zero; zero.QuadPart = 0;
		LARGE_INTEGER newptr;
		bool ok = (0 != SetFilePointerEx(Handle(), zero, &newptr, FILE_CURRENT));
		if (!ok) { 
			GetOSErrorCode(); 
			return false; 
		}
		if (newptr.QuadPart < 0) { 
			UpdateErrorCode(ERROR_SEEK); 
			return false; 
		}
		out_pos = newptr.QuadPart;
		return true;
	};

	bool FileHandle::SetPosition(uint64 pos)
	{
		if ((LONGLONG)pos < 0) { 
			UpdateErrorCode(ERROR_NEGATIVE_SEEK); 
			return false; 
		}
		LARGE_INTEGER newptr; newptr.QuadPart = (LONGLONG)pos;
		bool ok = (0 != SetFilePointerEx(Handle(), newptr, NULL, FILE_BEGIN));
		if (!ok) {
			GetOSErrorCode();
		}
		return ok;
	};

	bool FileHandle::SetPosition(int64 pos, DWORD dwMoveMethod)
	{
		LARGE_INTEGER newptr; newptr.QuadPart = pos;
		bool ok = (0 != SetFilePointerEx(Handle(), newptr, NULL, dwMoveMethod));
		if (!ok) {
			GetOSErrorCode();
		}
		return ok;
	};

	// Имеем в виду, что
	// "The requested operation cannot be performed on a file with a user-mapped section open."
	bool FileHandle::SetEndOfFile()
	{
		bool ok = (0 != ::SetEndOfFile(Handle()));
		if (!ok) {
			GetOSErrorCode();
		}
		return ok;
	};
