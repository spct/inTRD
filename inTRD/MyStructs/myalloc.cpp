﻿
//#include <Windows.h>
#include "myalloc.h"

ssize_t _allocated_;	// TODO: падать, если принимает отрицательное значение
myalloc_mem_functions _myalloc_mf;

/* Override default new/delete operators */
// При отлове memory leaks можно закомментить, чтобы уменьшить объём вывода myalloc_debug_output.
void* operator new(size_t _Count)/*throw(bad_alloc)*/ {	return myalloc<byte>( _Count); }
void operator delete(void* _Ptr)/*throw( )*/ {	myfree(_Ptr); }


/*Debug output*/
static void myalloc_debug_output(const char* s, size_t d, ssize_t allocated)
{
#ifdef MYALLOC_DEBUG_OUTPUT
	char buf[100];
	strcpy_(buf, s);
	utoa64(d, End(buf));
	strcat_(buf, ", allocated=");
	itoa64(allocated, End(buf));
	strcat_(buf, "\r\n");
	OutputDebugStringA(buf);
#endif
};

	#ifndef _WIN64
		const uint myalloc_chunk::GUARD1 = 0x99663311;
	#else
		const uint myalloc_chunk::GUARD1 = 0x9966;
	#endif
	const uint myalloc_chunk::GUARD2 = 0xFF336699;

	// Этот конструктор существует с единственной целью: инициализация
	// статической переменной myalloc_zerosize_chunk.
	myalloc_chunk::myalloc_chunk(int dummy)
	{
		Init(0);
	};

	void myalloc_chunk::Init(size_t size)
	{
		SetSize(size);
		SetGuards();
	};

	void myalloc_chunk::CheckGuards()
	{
		if (Guard1() != GUARD1) _throw("Memory corrupted");
		if (Guard2() != GUARD2) _throw("Memory corrupted");
	};

	inline void myalloc_chunk::CheckAllocated()
	{
		if (Guard1() == 0) _throw("Memory not allocated");
	};

	inline void myalloc_chunk::SetGuards()
	{
		Guard1() = GUARD1;
		Guard2() = GUARD2;
	};

	inline void myalloc_chunk::ClearGuards()
	{
		Guard1() = 0;
		Guard2() = 0;
	};

	void myalloc_chunk::SetSize(size_t size)
	{
		#ifndef _WIN64
			this->size = size;
		#else
			if (size >= 0x0001000000000000) _throw(0);    // TODO: вместо этого надо возвращать NULL. И не обязательно дожидаться, пока появятся такие объёмы данных: кто-то может захотеть выделить миллиард структур по 300Кб (вероятно по ошибке, но всё же...).
			memcpy_(&this->size, &size, 6);
		#endif
	};

static myalloc_chunk myalloc_zerosize_chunk(0);

myalloc_chunk* allocate_myalloc_chunk(size_t size)
{
	if (size == 0)
	{
		myalloc_debug_output("amem +", size, _allocated_ + size);
		return &myalloc_zerosize_chunk;
	}

	size_t sz = size + sizeof(myalloc_chunk);
	if (sz < size) { // overflow
		return NULL;
	}

	// Всегда вызываем версию с clear и не паримся.
	myalloc_chunk* ch = (myalloc_chunk*)_myalloc_mf.alloc_clear(sz);
	if (ch)
	{
		ch->Init(size);
		_allocated_ += size;
		myalloc_debug_output("amem +", size, _allocated_);
	}
	return ch;
}

void deallocate_myalloc_chunk(myalloc_chunk* ch)
{
	ch->CheckAllocated();
	ch->CheckGuards();
	myalloc_debug_output("fmem -", ch->Size(), _allocated_ - ch->Size());
	if (ch != &myalloc_zerosize_chunk)
	{
		_allocated_ -= ch->Size();
		size_t sz = ch->Size() + sizeof(myalloc_chunk);
		memset_(ch, 0, sz); // clear memory before freeing
		_myalloc_mf.free(ch);
	}
}

myalloc_chunk* myalloc_chunk_from_dataPtr(void* data)
{
	if ((size_t)data <= 8) {  // защита от переполнения вычитания, да и просто на всякий случай. На самом деле эта функция не должна вызываться с data=NULL
		return NULL;
	}
	return (myalloc_chunk*)((byte*)data - 8);
}

myalloc_chunk* reallocate_myalloc_chunk(myalloc_chunk* ch, size_t newSize)
{
	ch->CheckAllocated();
	ch->CheckGuards();

	if (newSize == ch->Size())
	{
		return ch;
	}

	if (ch == &myalloc_zerosize_chunk)
	{
		return allocate_myalloc_chunk(newSize);
	}

	if (newSize == 0)
	{
		deallocate_myalloc_chunk(ch);
		return &myalloc_zerosize_chunk;
	}

	size_t newSz = newSize + sizeof(myalloc_chunk);
	if (newSz < newSize) { // overflow
		return NULL;
	}

	// Тут мы имеем облом с очисткой памяти при уменьшении блока.
	// Мы не можем очистить память до realloc, т.к. не уверены, что realloc будет успешен;
	// а после realloc этого делать нельзя. Максимум можем занулить, а потом при необходимости
	// восстановить guard'ы.
	size_t oldSize = ch->Size();
	ch->ClearGuards();
	void* newC = ch;
	bool ok = _myalloc_mf.realloc(newC, newSz, true);  // всегда вызываем версию с clear
	ch = (myalloc_chunk*)newC;
	if (ok)
	{
		ch->SetSize(newSize);
		_allocated_ += newSize - oldSize;
		if (newSize >= oldSize) {
			myalloc_debug_output("rmem +", newSize - oldSize, _allocated_); 
		} else {
			myalloc_debug_output("rmem -", oldSize - newSize, _allocated_);
		}
	}
	ch->SetGuards();
	return ok ? ch : NULL;
}


	// Если mem=NULL, то ничего не делает.
	void myfree(void* mem)
	{
		if (!mem) {
			return;
		}
		myalloc_chunk* ch = myalloc_chunk_from_dataPtr(mem);
		deallocate_myalloc_chunk(ch);
		mem = NULL;
	}

#ifdef MYALLOC_USE_WINAPI_HEAP

	/* implementation via HeapAlloc with process heap*/

	// Returns NULL on error!
	HANDLE myalloc_mem_functions::GetHeap()
	{
		static HANDLE heap = NULL;
		if (heap == NULL) {
			heap = GetProcessHeap();
		}
		return heap;
	}

	LPVOID myalloc_mem_functions::_HeapAlloc(DWORD dwFlags, SIZE_T dwBytes)
	{
		HANDLE heap = GetHeap();
		if (heap == NULL) {
			return NULL;
		}
		return HeapAlloc(heap, dwFlags, dwBytes);
	}

	LPVOID myalloc_mem_functions::_HeapReAlloc(DWORD dwFlags, LPVOID lpMem, SIZE_T dwBytes)
	{
		HANDLE heap = GetHeap();
		if (heap == NULL) {
			return NULL;
		}
		return HeapReAlloc(heap, dwFlags, lpMem, dwBytes);
	}

	void* myalloc_mem_functions::alloc(size_t dwSize)
	{
		// даже если dwSize=0, HeapAlloc всё-равно выделяет кусок памяти. Соптимизируем расход памяти немного
		if (dwSize == 0) {
			return (void*)1;
		}
		return _HeapAlloc(0, dwSize);
	}

	void* myalloc_mem_functions::alloc_clear(size_t dwSize)
	{
		// даже если dwSize=0, HeapAlloc всё-равно выделяет кусок памяти. Соптимизируем расход памяти немного
		if (dwSize == 0) {
			return (void*)1;
		}
		return _HeapAlloc(HEAP_ZERO_MEMORY, dwSize);
	}

	bool myalloc_mem_functions::realloc(void*& mem, size_t newSize, bool clear)
	{
		// даже если dwSize=0, HeapAlloc всё-равно выделяет кусок памяти. Соптимизируем расход памяти немного.
		// If HeapReAlloc fails and you have not specified HEAP_GENERATE_EXCEPTIONS, the return value is NULL. 
		// HeapReAlloc does not call SetLastError.
		if (newSize == 0)
		{
			free(mem);
			mem = (void*)1;
			return true;
		}
		else if (mem <= (void*)1)
		{
			return (mem = (clear ? alloc_clear(newSize) : alloc(newSize))) != NULL;
		}
		else
		{
			void* newmem = _HeapReAlloc(clear ? HEAP_ZERO_MEMORY : 0, mem, newSize);
			if (newmem) { 
				mem = newmem; 
				return true; 
			} else {
				return false;
			}
		}
	}
	
	void myalloc_mem_functions::free(void* mem)
	{
		// HeapFree работает c mem=NULL (причём возвращает true), но не работает с mem=1 (падает с exception'ом)
		if (mem <= (void*)1) {
			return;
		}
		HeapFree(GetHeap(), 0, mem);
	}

#endif


#ifdef MYALLOC_USE_CRT

	/* implementation via malloc */
	// Помним, что функция malloc возвращает 0 если не может выделить память.

	#include <malloc.h>

	void* myalloc_mem_functions::alloc(size_t dwSize)
	{
		// даже если dwSize=0, malloc всё-равно выделяет кусок памяти. Соптимизируем расход памяти немного
		if (dwSize == 0) {
			return (void*)1;
		}
		return ::malloc(dwSize);
	}
	
	void* myalloc_mem_functions::alloc_clear(size_t dwSize)
	{
		if (dwSize == 0) {
			return (void*)1;
		}
		return ::calloc(dwSize, 1);
	}

	bool myalloc_mem_functions::realloc(void*& mem, size_t newSize, bool clear)
	{
		// realloc: If there is not enough available memory to expand the block to the given size, the original block is left unchanged, and NULL is returned.
		// realloc: If size is zero, then the block pointed to by memblock is freed; the return value is NULL.
		if (newSize == 0)
		{
			free(mem);
			mem = (void*)1;
			return true;
		}
		else if (mem <= (void*)1)
		{
			return (mem = clear ? alloc_clear(newSize) : alloc(newSize)) != NULL;
		}
		else
		{
			size_t oldSize = ::_msize(mem);
			void* newmem = ::realloc(mem, newSize);
			if (!newmem) {
				return false;
			}
			if (clear && newSize > oldSize) {
				memset_((char*)newmem + oldSize, 0, newSize - oldSize);
			}
			mem = newmem;
			return true;
		}
	}
	
	void myalloc_mem_functions::free(void* mem)
	{
		if (mem <= (void*)1) {
			return;
		}
		::free(mem);
	}

#endif

