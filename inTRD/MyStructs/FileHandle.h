﻿
/*
 * Helper for creating, opening and reading/writing files
*/

#pragma once

#include "..\common.h"
#include "Result.h"
#include <tchar.h>

/****** Settings **********/

//#define FILEHANDLE_USE_CONSTRUCTOR
//#undef FILEHANDLE_USE_CONSTRUCTOR

#ifdef MYSTRUCTS_ALLOW_CTORS
	#define FILEHANDLE_USE_CONSTRUCTOR
#endif

// Следующую настройку следует ставить в common.h, а не здесь:

//рекомендуется, если делаем wcx-плагин для Total Commander
//#define FILEHANDLE_WCX_SUPPORT

/***************************/


/****** Implementation *********/

struct FileHandle
{

private:
	
	// bitwise-inverted HANDLE value; zero means invalid HANDLE value
	size_t inv_handle;

	void UpdateErrorCode(DWORD newErrorCode)
	{
		if (ErrorCode == 0) {
			ErrorCode = newErrorCode;
		}
	}

	void GetOSErrorCode()
	{
		UpdateErrorCode(GetLastError());
	}

public:

	HANDLE Handle()
	{
		return HANDLE(~inv_handle);
	}

	bool Created;			// true if file didn't exist before opening.
	uint64 Size;
	HANDLE Mapping;			// non-NULL if memory mapping used
	byte* Adr;				// addr for memory-mapped file
	uint64 MappingSize;		/* size of memory-mapped file.
							   Почему недостаточно переменной Size? Потому что она может измениться, а нам важно знать, не имел ли файл нулевой размер при открытии маппинга.
							   Также замечу, что нет гарантий, что здесь записан правильный размер маппинга (см. ф-цию OpenMapping). */

	DWORD ErrorCode;		// GetLastError value after the first failed operation, or zero

	TCHAR* Path;			// (allocated with new[]) A copy of "path" arg passed to Open method.

private:
	bool deleteOnClose;

public:

#ifdef FILEHANDLE_USE_CONSTRUCTOR
	FileHandle();
	~FileHandle();

	// Whether to call Close() in destructor. Default is true.
	bool CloseInDestructor;
#endif


private:

	Result Open_Core(
		const TCHAR* path,			// file path
		bool writeable,				// false = read only; true = read & write
		DWORD createDisposition, 
		bool seq_scan,				// add FILE_FLAG_SEQUENTIAL_SCAN hint
		bool startMapping,			// use memory mapping
		uint64 maxFileSize,			// abort if file size exceeds this value
		Result onFilesizeExceed,	// value returned if file size exceeds maxFileSize
		bool deleteOnClose			// delete file on close; can be overriden with deleteOnClose_keep parameter
	);

	// Closes (if opened) File Handle and Mapping.
	// Deletes file it deleteOnClose=true and deleteOnClose_keep=false.
	Result Close_Core(bool deleteOnClose_keep);

public:

	// Sets File Handle, Size and Created fields.
	// If already opened, returns error.
	Result Open(
		const TCHAR* path, bool writeable, DWORD createDisposition, bool seq_scan, bool startMapping, 
		uint64 maxFileSize, Result onFilesizeExceed);

	// Sets File Handle, Size and Created fields.
	// If already opened, returns error.
	Result Open(
		const TCHAR* path, bool writeable, DWORD createDisposition, bool seq_scan, bool startMapping);


	// Open file and configure FileHandle to delete file on closing.
	// Sets File Handle, Size and Created fields.
	// If already opened, returns error.
	// The file will be deleted using path from 'path' arg.
	// Make sure path represents absolute, not relative path;
	// also remember, that file may be deleted even if it was not actually created
	// (i.e. an existing one was opened ('Created' field is false)).
	Result Open_DeleteOnClose(
		const TCHAR* path, bool writeable, DWORD createDisposition, bool seq_scan);

	// Closes (if opened) File Handle and Mapping.
	Result Close();

	// Closes (if opened) File Handle and Mapping.
	// Deletes file if deleteOnClose mode is on and deleteOnClose_keep=false.
	Result Close(bool deleteOnClose_keep);
	
	bool OpenMapping(bool writeable);
	bool IsMapped();
	// Closes (if opened) memory mapping, but doesn't close file handle.
	bool CloseMapping();

	// Queries file size from system, but doesn't update Size variable!
	// Returns zero and false on error.
	bool GetSize(uint64& out_size);

	bool Read(LPVOID lpBuffer, uint64 nNumberOfBytesToRead);
	bool Write(LPVOID lpBuffer, uint64 nNumberOfBytesToWrite);

	// Reads nNumberOfBytesToRead bytes, filling specified buffer in a cyclically
	bool TestRead(LPVOID lpBuffer, size_t bufSize, uint64 nNumberOfBytesToRead);

	bool GetPosition(uint64& out_pos);
	bool SetPosition(uint64 pos);
	bool SetPosition(int64 pos, DWORD dwMoveMethod);
	bool SetEndOfFile();

};
