﻿
/** Miscellaneous general purpose helpers and types **/

#include "serv.h"

const char HexChars_Up[17] = "0123456789ABCDEF";
const char HexChars_Low[17] = "0123456789abcdef";

// zero appended automatically, and it is counted by sizeof !
const char InvalidFileNameChars[41] = "\"<>|\\/?*:\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F";
const char InvalidPathChars[39]     = "\"<>|?*:\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F";

bool is_clear(char* buf, size_t count)
{
	while(count--) {
		if (*buf++ != 0) return false;
	}
	return true;
};

// Display error message, abort program, kill process or at least kill thread
__declspec(noreturn) void _throw(char* msg)
{
	static const char msg1[] = "Unrecoverable error occured.\nPlease report to the author!\n";
	char buf[500];
	clear_array(buf);
	strcpy_(buf, msg1);
	
	if (msg != NULL)
	{
		// Cut off 'Internal error' text
		size_t len = strlen_(msg);
		if (len >= 14 && memeq(&msg[1], "nternal error", 13))
		{
			len -= 13; msg += 14;
			if (len > 0 && msg[0] == ':') len--, msg++;
			if (len > 0 && msg[0] == ' ') len--, msg++;
		}

		strcat_(buf, "\nInternal error:\n");

		len = min(len, ARRAY_LEN(buf) - 100);  // 100 >= длина всего того, что мы уже напихали в buf
		COPY_ARR(End(buf), msg, len);
	}

	MessageBoxA(NULL, buf, ERROR_MSGBOX_CAPTION, MB_OK | MB_APPLMODAL | MB_ICONERROR);

	// Странно, но это не всегда приводит к завершению работы приложения:
	*(int*)NULL = 0;
	
	// поэтому пытаемся ещё.
	// Вот неплохой вариант, показывает messagebox об unhandled exception
	// и убивает процесс.
	throw;

	// Если уж предыдущие команды не сработали, тогда явно прикончим поток.
	// Тем более, что мы пометили функцию параметром declspec(noreturn)
	// и не можем себе позволить возврат из функции.
	ExitThread(10);
};

