﻿/*
 * Various helpers for making Total Commander wcx plugins
 */

#pragma once

#include "..\common.h"
#include "..\MyStructs\Result.h"
#include "..\MyStructs\String.h"
#include "..\MyStructs\FileHandle.h"
#include "..\wcxhead.h"

// path must not end with slash!
extern bool IsDirectory(const TCHAR* path);

// path must not end with slash!
extern bool DirectoryExists(const TCHAR* path);


// Delete files after packing to archive with PK_PACK_MOVE_FILES option specified
template<bool deleteDirs>
Result DeletePackedFiles(const TCHAR* AddList, const TCHAR* SrcPath)
{
	// Gotta move from end to beginning so directories are cleared before they are deleted

	#ifndef STRING_USE_CONSTRUCTOR
		#error DeletePackedFiles implementation requires STRING_USE_CONSTRUCTOR
	#endif

	Result res;
	String<TCHAR> path; 
	bool ok = true;

	const TCHAR* listItem = AddList;
	while(*listItem) while(*listItem++);  // move to end of list

	while(listItem > AddList)
	{
		listItem--;
		bool isDir = IsSlash(listItem[-1]);
		do { listItem--; } while(listItem > AddList && listItem[-1] != 0); // find the start of file/dir path

		res = path.Set(SrcPath);
		res &= path.AppendPath(listItem);
		if (!res) {
			goto k9; // most likely Out of mem
		}

		if (isDir)
			ok &= (!deleteDirs || RemoveDirectory(path.GetPtr()) != 0);
		else
			ok &= (DeleteFile(path.GetPtr()) != 0);
	}

    res = SUCCESS;	
    if (!ok) {
		res = Result("Error deleting source files/directories", true);
	}

k9:

	return res;
}

// Compare two strings up to the first slash char
template<class TCHR>
bool ComparePathItemName(const TCHR* item1, const TCHR* item2)
{
	const TCHR *p1 = item1, *p2 = item2;
	TCHR c1, c2;
	do {
		c1 = *p1++; if (IsSlash(c1)) c1 = 0;
		c2 = *p2++; if (IsSlash(c2)) c2 = 0;
		if (c1 != c2) {
			return false;
		}
	} while (c1);
	return true;
};


extern FILETIME NowUtc();
extern bool NowLocal(FILETIME* out_time);

// Returns 0 on error
extern DWORD NowDosLocal();

extern bool SetFileModificationDate(HANDLE hFile, FILETIME time, bool timeIsUtc);

extern bool GetFileModificationDateUtc(HANDLE hFile, FILETIME* out_time);

// Returns 0 on error
extern DWORD Utc2DosLocal(FILETIME utc);

// Returns 0 on error
extern DWORD GetFileModificationDateDosLocal(HANDLE hFile);

extern bool SetFileModificationDateDosLocal(HANDLE hFile, DWORD filetimeLocal);

extern FILETIME MakeDifferentFileTime(FILETIME time);
extern bool UpdateFileWriteTime(HANDLE hFile);
extern bool UpdateFileWriteTime2(HANDLE hFile, FILETIME originalFileTime);


// Helper for implementing progress reporting and cancellation
class ProgressReporter
{
private:
	tProcessDataProcW ProcessDataProc;

public:
	ProgressReporter(tProcessDataProcW* processDataProc_varPtr);

	// Returns true if cancellation pending
	bool Report(int64 done, WCHAR* filename);
	
	// Returns true if cancellation pending
	bool SetProgress(int firstIndicatorPercents, int secondIndicatorPercents, WCHAR* filename);
};


// Create temporary file within given full path with random number appended.
// Returns path and FileHandle.
template<class TCHR>
Result CreateTempFile(const TCHR* origPath, String<TCHR>& tempPath, FileHandle& tempFile)
{
	Result res;
	for(uint retry = 5; retry-- != 0; )  // retry a few times
	{
		res = tempPath.Set(origPath);
		res &= tempPath.Append(_T("."));
		res &= tempPath.AppendUint(Random_Next());
		res &= tempPath.Append(_T(".temp"));
		if (!res) {
			// most likely Out of mem or resulting path is too long
			break;
		}

		res = tempFile.Open(tempPath.GetPtr(), true, CREATE_NEW, true, false);
		if (res) {
			return SUCCESS;
		}
		
		//if (tempFile.ErrorCode != ERROR_FILE_EXISTS) {	// UPD: decided to retry regardles of error kind
		//	break;
		//}

		Sleep(5);
	}

	// failed to create file
	res.WcxCode() = E_ECREATE;
	tempPath.Clear();
	return res;
};

