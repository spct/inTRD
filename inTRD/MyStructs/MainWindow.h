﻿
/* Helper to get Total Commander window handle */

#pragma once

#include "..\common.h"
#include <Windows.h>

class MainProcessWindow
{
public:

	HWND Get();

private:

	DWORD processId;
	HWND bestHandle;

	static BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
	{
		MainProcessWindow* mpw = (MainProcessWindow*)lParam;
		DWORD pid;
		GetWindowThreadProcessId(hwnd, &pid);
		if (pid == mpw->processId) { 
			if (IsMainWindow(hwnd)) { 
				mpw->bestHandle = hwnd;
				return false;
			}
		}
		return true;
	}

	static bool IsMainWindow(HWND hwnd) 
	{ 
		if (GetWindow(hwnd, GW_OWNER) != (HWND)0	// has owner window?
			|| !IsWindowVisible(hwnd))				// is not visible?
			return false;

		return true; 
	}
};
