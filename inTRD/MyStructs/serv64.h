﻿
/** Miscellaneous general purpose helpers and types **/
// part of serv.h for x64 platform

#include "..\common.h"

template<typename TCHR>
static void utoa(unsigned int a, TCHR* buf)
{
	byte stack[10];
	size_t sp = 0;
	do {
		stack[sp++] = a % 10u;
		a /= 10u;
	} while (a != 0);
	do {
		*buf++ = stack[--sp] + '0';
	} while (sp != 0);
	buf[0] = 0;
}

template<typename TCHR>
static void utoa0(unsigned int a, TCHR* buf, size_t digitCount)
{
	buf[digitCount] = 0;
	while(digitCount-- != 0)
	{
		buf[digitCount] = a % 10u + '0';
		a /= 10u;
	}
}

template<typename TCHR>
static void utoa0_hex(unsigned int a, TCHR* buf, size_t digitCount, const char* chars)
{
	buf[digitCount] = 0;
	while(digitCount-- != 0)
	{
		buf[digitCount] = chars[a & 15];
		a >>= 4;
	}
}

template<typename TCHR>
static void itoa(int a, TCHR* buf)
{
	if (a < 0) {
		*buf++ = '-';
	}
	utoa(abs(a), buf);
}

template<typename TCHR>
static void utoa_t(size_t a, TCHR* buf)
{
	utoa64(a, buf);
}
