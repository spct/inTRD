﻿
/*
	Враппер для работы с памятью (alloc, calloc, free, realloc).

	Помогает находить утечки, buffer overflow, double free.

	Переопределяет new и delete.

	В качестве базы может быть Windows Heap или malloc.

	Делается оптимизация для случая, когда выделяется 0 байт: ничего реально не выделяется и возвращается (void*)1.
	Надеюсь, что такого адреса ни одна из использованных функций выделения вернуть не может :)
*/

/*
	TODO:
		HeapRealloc падает с expection'ом (вместо того, чтобы вернуть NULL) если dwSize >= (1<<31) в 32-битном режиме
		или >= (1<<42) в 64-битном режиме
*/


#pragma once

#include "..\common.h"

/* settings */

// Эти настройки следует ставить в common.h, а не здесь.
//#define MYALLOC_USE_WINAPI_HEAP
//#define MYALLOC_USE_CRT


/* myalloc_implementation class declaration */

class myalloc_mem_functions
{

#ifdef MYALLOC_USE_WINAPI_HEAP
private:
	HANDLE GetHeap();
	LPVOID _HeapAlloc(DWORD dwFlags, SIZE_T dwSize);
	LPVOID _HeapReAlloc(DWORD dwFlags, LPVOID lpMem, SIZE_T dwBytes);
#endif

public:
	void* alloc(size_t dwSize);
	void* alloc_clear(size_t dwSize);
	bool realloc(void*& mem, size_t newSize, bool clear);
	void free(void* mem);
};


/**/

extern ssize_t _allocated_;
extern myalloc_mem_functions _myalloc_mf;

	/**/
	#pragma pack(push,1)
	struct myalloc_chunk
	{
		private: static const uint GUARD1, GUARD2;

		#ifndef _WIN64
			// Layout: size 00_33_66_99 data 99 66 33 FF
			private: size_t size; 
			private: uint guard1;
			private: uint guard2;

			public: inline size_t Size() { return size; }
			private: inline uint& Guard1() { return guard1; }
			public: inline byte* Data() { return (byte*)&guard2; }
			private: inline uint& Guard2() { return *(uint*)(Data() + Size()); }
		#else
			// size_00_00 66_99 data 99 66 33 FF
			// в этом режиме size занимает 6 байт, guard1 занимает 2 байта (0x9966)
			private: uint64 size;  // он же и guard1
			private: uint guard2;

			public: inline size_t Size() { return size & 0x0000FFFFFFFFFFFF; }
			private: inline ushort& Guard1() { return ((ushort*)&size)[3]; }
			public: inline byte* Data() { return (byte*)&guard2; }
			private: inline uint& Guard2() { return *(uint*)(Data() + Size()); }
		#endif

		public: myalloc_chunk(int dummy);

		public: void Init(size_t size);
		public: void CheckGuards();
		public: void CheckAllocated();
		public: void SetGuards();
		public: void ClearGuards();
		public: void SetSize(size_t size);
	};
	#pragma pack(pop)

	extern myalloc_chunk* allocate_myalloc_chunk(size_t size);
	extern void deallocate_myalloc_chunk(myalloc_chunk* ch);
	extern myalloc_chunk* reallocate_myalloc_chunk(myalloc_chunk* ch, size_t newSize);
	extern myalloc_chunk* myalloc_chunk_from_dataPtr(void* data);
	/**/

/* functions for use */

template<typename TItem> TItem* myalloc(size_t count)
{
	if (count > SIZE_MAX / sizeof(TItem)) {
		return NULL;
	}
	size_t size = count * sizeof(TItem);
	myalloc_chunk* c = allocate_myalloc_chunk(size);
	if (c == NULL) {
		return NULL;
	}
	return (TItem*)c->Data();
};

template<typename TItem> TItem* myalloc_clear(size_t count)
{
	return myalloc<TItem>(count);
};

// No-op for mem==NULL
extern void myfree(void* mem);

template<typename TItem> bool myrealloc(TItem*& mem, size_t count, bool clear)
{
	if (count > SIZE_MAX / sizeof(TItem)) {
		return false;
	}
	size_t newSize = count * sizeof(TItem);
	myalloc_chunk* oldC = myalloc_chunk_from_dataPtr(mem);
	myalloc_chunk* newC = reallocate_myalloc_chunk(oldC, newSize);
	if (!newC) {
		return false;
	}
	mem = (TItem*)newC->Data();
	return true;
};

// Округляет вверх до ближайшего возможного allocation size.
// Подробности описаны в документе mem allocation granularity.docx
// Гарантируется, что возвращённое значение будет не меньше count.
template<typename TItem>
size_t RecommendedAllocCount(size_t count)
{
	if (count == 0) {
		return 0;
	}
	size_t sz = count * sizeof(TItem);
	if (sz < count) {
		return SIZE_MAX; // overflow. Let it fail on allocate.
	}

	#if defined(_WIN64) && defined(MYALLOC_USE_WINAPI_HEAP)
		size_t allocSz = ((sz + 4 + 15) & size_t(0xFFFFFFFFFFFFFFF0ui64)) + 0x10;
	#elif defined(_WIN64) && defined(MYALLOC_USE_CRT)
		size_t allocSz = ((sz + 4 + 15) & size_t(0xFFFFFFFFFFFFFFF0ui64)) + 0x10;
	#elif !defined(_WIN64) && defined(MYALLOC_USE_WINAPI_HEAP)
		size_t allocSz = ((sz + 4 + 7) & size_t(0xFFFFFFF8u)) + 0x10;
	#elif !defined(_WIN64) && defined(MYALLOC_USE_CRT)
		size_t allocSz = ((sz + 4 + 7) & size_t(0xFFFFFFF8u)) + 0x10;
	#endif
	
	size_t allocCount = allocSz / sizeof(TItem);
	if (allocCount < count) {
		return SIZE_MAX; // overflow. Let it fail on allocate.
	}
	return allocCount;
};
