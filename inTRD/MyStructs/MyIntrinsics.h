﻿
#pragma once

#include "..\common.h"
#include <Windows.h>		// required for built-in intrinsics
#include <math.h>
#pragma intrinsic(abs, fabs, labs, _rotl, _rotr, _lrotr, _lrotl)

// helpers
#define clear_array(a)	memset_(a, 0, sizeof(a))
#define fill_array(a, val)	{ for(size_t _i = 0; _i < ARRAY_LEN(a); _i++) (a)[_i] = (val); }
#define clear_struct(a)	memset_(&a, 0, sizeof(a))

// Поосторожнее с этим при использовании вне конструктора.
// Перед тем как делать это, надо убедиться, что вызваны деструкторы на всём, чём можно.
#define clear_this()	memset_(this, 0, sizeof(*this))

template<class T> __forceinline T rotl(T a, int q) { return _rotl(a, q); }
template<class T> __forceinline T rotr(T a, int q) { return _rotr(a, q); }


// выбираем, какие intrinsics использовать
// UPD: это делается не здесь, а в common.h
//#define CUSTOM_INTRINSICS
//#undef CUSTOM_INTRINSICS


#ifndef CUSTOM_INTRINSICS

	// форсируем применение системных intrinsic функций
	#pragma intrinsic(strcmp, strcpy, memcmp, strlen, memcpy, memset, strcat, _strset)

	#define _impl_strlen strlen
	#define _impl_wcslen wcslen
	#define _impl_strcpy strcpy
	#define _impl_wcscpy wcscpy
	#define _impl_strcmp strcmp
	#define _impl_wcscmp wcscmp
	#define _impl_strcat strcat
	#define _impl_wcscat wcscat
	#define _impl_strchr strchr
	#define _impl__strset _strset

	#define memset_ memset
	#define memcpy_ memcpy
	#define memcmp_ memcmp
	#define memchr_ memchr

	#define memmove_ memmove

	template<typename T>
	static void tmemcpy(T *dest, const T *src, size_t count)
	{
		memcpy_((void*)dest, (const void*)src, count * sizeof(T));
	}


#else // Custom instrinsics implementation

	#error Custom intrinsics not supported

#endif

	// Унификация строковых функций (для char и wchar)

	inline char   * strcpy_(char    *strDestination, const char    *strSource) { return _impl_strcpy(strDestination, strSource); }
	inline wchar_t* strcpy_(wchar_t *strDestination, const wchar_t *strSource) { return _impl_wcscpy(strDestination, strSource); }
	
	inline size_t strlen_(const char    *str) { return _impl_strlen(str); }
	inline size_t strlen_(const wchar_t *str) { return _impl_wcslen(str); }
	inline size_t strlen_(const byte    *str) { return _impl_strlen((char*)str); }

	inline int strcmp_(const char    *string1, const char    *string2) { return _impl_strcmp(string1, string2); }
	inline int strcmp_(const wchar_t *string1, const wchar_t *string2) { return _impl_wcscmp(string1, string2); }

	inline char   * strcat_(char    *strDestination, const char    *strSource) { return _impl_strcat(strDestination, strSource); }
	inline wchar_t* strcat_(wchar_t *strDestination, const wchar_t *strSource) { return _impl_wcscat(strDestination, strSource); }

