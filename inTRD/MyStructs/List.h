﻿
#pragma once

#include "..\common.h"
#include <tchar.h>
#include "Result.h"
#include "myalloc.h"

//#define LIST_USE_CONSTRUCTOR
//#undef LIST_USE_CONSTRUCTOR

#ifdef MYSTRUCTS_ALLOW_CTORS
	#define LIST_USE_CONSTRUCTOR
#endif

// Эту настройку следует ставить в common.h, а не здесь:
//#define LIST_COUNT_TYPE_DEFAULT uint

const Result RESULT_LIST_CAPACITY_TOO_LARGE = Result("Capacity of list is too large", true);
const Result RESULT_LIST_OVERFLOW = Result("Overflow of list", true);

// List выделяет память с помощью myalloc и myfree,
// то есть конструкторы и деструкторы для T не вызываются.
// count_t: тип данных для представления кол-ва элементов; 
// любое беззнаковое целое, но не больше size_t; обычно uint.
template <typename T, typename count_t = LIST_COUNT_TYPE_DEFAULT>
class List
{
	private: static const count_t count_t_max = count_t(count_t(0) - 1);
	public: static const count_t MAX_CAPACITY = count_t_max / 2u / sizeof(T);

	public:	T* data;
	public:	count_t Count;
	private: count_t Capacity;		// текущий размер массива data

	// лучше иметь функции, через которые класс будет работать с памятью.
	// заодно можно явно задать оптимизацию для случая count=0.
	private: bool mem_alloc(count_t count, T** mem)
	{
		if (count == 0) {
			*mem = NULL; return true;
		} else {
			return (*mem = myalloc<T>(count)) != NULL;
		}
	}
	private: void mem_free(T* mem)
	{
		if (mem) myfree(mem);
	}
	private: bool mem_realloc(count_t newCount, T*& mem)
	{
		if (newCount == 0) {
			mem_free(mem); mem = NULL; return true;
		}
		else if (mem == NULL) {
			return mem_alloc(newCount, &mem);
		}
		else {
			return myrealloc<T>(mem, newCount, false);
		}
	}

#ifdef LIST_USE_CONSTRUCTOR

public:

	List()
	{
		clear_this();
	}
	~List()
	{
		Destroy();
	}

	// Вызывать не обязательно, всё будет проинициализировано при первом вызове метода Add
	// Вернуть может:
	//      RESULT_LIST_CAPACITY_TOO_LARGE
	//		RESULT_NO_MEM
	Result Init(count_t initialCapacity)
	{
		if (data) {
			_throw("already initialized");
		}
		if (initialCapacity > MAX_CAPACITY) {
			return RESULT_LIST_CAPACITY_TOO_LARGE;
		}
		if (!mem_alloc(initialCapacity, &data)) {
			return RESULT_NO_MEM;
		}
		Capacity = initialCapacity;
		Count = 0;
		return SUCCESS;
	}

	// Вызывать не обязательно, всё будет проинициализировано при первом вызове метода Add
	Result Init()
	{
		return Init(0);
	}

	// Вызывается деструктором, явно вызывать не обязательно
	void Destroy()
	{
		mem_free(data);
		clear_this();
	}

#else

public:

	// Обязательно к вызову после создания объекта (объявления переменной).
	// Если Init вернул ошибку, то деструктор вызывать не обязательно.
	// Вернуть может:
	//      RESULT_LIST_CAPACITY_TOO_LARGE
	//		RESULT_NO_MEM
	Result Init(count_t initialCapacity)
	{
		//if (data) _throw("already initialized");	// без конструктора нельзя такую проверку делать, т.к. память может содержать мусор
		clear_this();
		if (initialCapacity > MAX_CAPACITY) {
			return RESULT_LIST_CAPACITY_TOO_LARGE;
		}
		if (!mem_alloc(initialCapacity, &data)) {
			return RESULT_NO_MEM;
		}
		Capacity = initialCapacity;
		return SUCCESS;
	}

	Result Init()
	{
		return Init(0);
	}

	void Destroy()
	{
		mem_free(data);
		clear_this();
	}

#endif

	// Вернуть может:
	//      RESULT_LIST_OVERFLOW
	//		RESULT_NO_MEM
	public: Result Insert(count_t index, T value)
	{
		if (index > Count) {
			_throw("Index out of list");
		}
		
		if (Count >= Capacity)
		{
			if (Capacity >= MAX_CAPACITY) {
				return RESULT_LIST_OVERFLOW;
			}
			// переполнения умножения здесь не будет, т.к. Capacity <= MAX_CAPACITY <= count_t_max/2
			count_t newCapacity = (Capacity == 0) ? 1 : min(Capacity * 2, MAX_CAPACITY);
			if (!mem_realloc(newCapacity, data)) {
				return RESULT_NO_MEM;
			}
			Capacity = newCapacity;
		}

		count_t q = Count - index;
		if (q != 0) {
			memmove_(&data[index + 1], &data[index], q * sizeof(T));
		}

		data[index] = value;
		Count++;
		return SUCCESS;
	}

	// Вернуть может:
	//      RESULT_LIST_OVERFLOW
	//		RESULT_NO_MEM
	public: Result Add(T value)
	{
		return Insert(Count, value);
	}

	public: T& Get(count_t index) const
	{
		if (index >= Count) {
			_throw("Index out of list");
		}
		return data[index];
	}

	public: T& operator[](count_t index) const {
		return Get(index);
	}
	
	public: void Clear() {
		Count = 0; 
	}

	public: void RemoveAt(count_t index)
	{
		if (index >= Count) {
			_throw("Index out of list");
		}
		memmove_(&data[index], &data[index + 1], (Count - (index + 1)) * sizeof(T));
		Count--;
	}

};

