﻿
/* Helper to get Total Commander window handle */

#include "..\common.h"
#include "MainWindow.h"

HWND MainProcessWindow::Get()
{
	bestHandle = 0;
	processId = GetCurrentProcessId();
	EnumWindows(&EnumWindowsProc, (LPARAM)this);
	return bestHandle;
}
