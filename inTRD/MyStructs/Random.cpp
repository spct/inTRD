﻿
/*
 * Pseudo Random Number Generator based on CRC hash
 */

#include "Random.h"

// __umulh: умножение 64-битных целых (64*64 => 64)
#ifdef _WIN64
	#include <winnt.h>		// __umulh
#else
	uint64 __umulh(uint64 a, uint64 b)
	{
		__asm
		{
			mov eax, dword ptr a+0
			mul dword ptr b+0
			mov ecx, edx
	
			mov eax, dword ptr a+4
			mul dword ptr b+0
			add ecx, eax
			adc edx, 0
			push edx


			mov eax, dword ptr a+0
			mul dword ptr b+4
			add eax, ecx
			pushf ; store CF
			mov ecx, edx
	
			mov eax, dword ptr a+4
			mul dword ptr b+4
			add eax, ecx
			adc edx, 0

			popf
			pop ecx
			adc eax, ecx
			adc edx, 0
		}
	}
#endif



static uint val = 0xffffffff;
static const uint kCrcPoly = 0xEDB88320;
static volatile uint counter;

static void crc_update(void* data, size_t count)
{
	byte* p = (byte*)data;
    while(count--)
	{
		val ^= *p++;
		for (uint i = 0; i < 8; i++)
			val = (val >> 1) ^ (kCrcPoly & ~((val & 1) - 1));
	}
};

//#pragma warning(suppress : 4700)
void Random_Feed()
{
	// наскребаем энтропию с нескольких источников:
	// системные часы, системный таймер, статический счётчик, и немного мусора со стека :)

	#pragma pack(push,1)	// чтобы размер структуры был не кратен 4
	struct
	{
		FILETIME a;
		LARGE_INTEGER b;
		uint c;
		byte d;
	} s;
	#pragma pack(pop)
	
	GetSystemTimeAsFileTime(&s.a);
	// GetSystemTimeAsFileTime работает быстрее и даёт большую точность, чем GetSystemTime. 

	QueryPerformanceCounter(&s.b);
	s.c = ++counter;
	
	byte& p = s.d; p -= 17;
	// промежуточный указатель нужен, иначе под дебагом останавливается с ошибкой "Uninitialized variable"

	crc_update(&s, sizeof(s));
};
//#pragma warning(default : 4700)

void Random_Feed(void* data, size_t size)
{
	Random_Feed();
	crc_update(data, size);
};

//#pragma warning(suppress : 4700)
void Random_FeedFast()
{
	#pragma pack(push,1)	// чтобы размер структуры был не кратен 4
	struct
	{
		uint c;
		byte d;
	} s;
	#pragma pack(pop)

	s.c = ++counter;
	byte& p = s.d; p -= 17;
	// промежуточный указатель нужен, иначе под дебагом останавливается с ошибкой "Uninitialized variable"
	
	crc_update(&s, sizeof(s));
};
//#pragma warning(default : 4700)

// Returns random uint32
uint Random_Next()
{
	Random_Feed();
	return val;
};

// Returns random uint32 from [0; N)
uint Random_Next(uint N)
{
	Random_Feed();
	return uint((uint64(val) * N) >> 32);
};

// Returns random uint64
uint64 Random_Next64()
{
	uint64 t;
	Random_Next(&t, 8);
	return t;
};

// Returns random uint64 from [0; N)
uint64 Random_Next64(uint64 N)
{
	return __umulh(Random_Next64(), N);
};

// Returns random size_t from [0; N)
size_t Random_NextT(size_t N)
{
	#ifdef _WIN64
		return Random_Next64(N);
	#else
		return Random_Next(N);
	#endif
};

// Returns random bytes
void Random_Next(void* output, size_t count)
{
	// Используем часть буфера результата (от 1/10 до 1/17) для feed'а

	if (count < 16)
	{
		Random_Feed(output, count);
	}
	else
	{
		size_t d = (val & 7) + 10;
		byte* p = (byte*)output;
		byte* end = p + count;
		for(; p < end; p += d) {
			crc_update(p, 1);
		}
		Random_Feed();
	}
	
	byte* p = (byte*)output;
	while(count >= sizeof(val))
	{
		*(uint*)p = val;
		p += sizeof(val);
		count -= sizeof(val);
		Random_FeedFast();
	}
	
	memcpy_(p, &val, count);
};

