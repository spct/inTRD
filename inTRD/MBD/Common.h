﻿
#pragma once

#include "..\common.h"
#include "..\MyStructs\Result.h"
#include "..\MyStructs\String.h"
#include "Disk.h"
#include "Options.h"

/*
	Имя файла выглядит так:

	<name>.<type><gap><params>

	Здесь:
		<name>   - имя. От 0 до бесконечности символов. Конец - последняя точка.
		           Парсер берёт только первые N символов, остальные теряет.
		           Допустимы только символы с кодами 32...126; 
				   остальные, а также InvalidFilenameChars и '#' эскейпятся.
		<type>   - тип. Десятичное (не hex!) число без лидирующих нулей. Парсер, впрочем,
		           принимает и с лидирующими нулями.
		<gap>    - пробелы. Не менее одного!!
		<params> - произвольная строка, содержащая в качестве подстрок 
		           значения параметров (все в hex по 2/4 цифры или все в dec по 3/5 цифр). 

        Нигде в строке не должно быть символов с кодами <32 или >126.

	Примечания: 

		Заметим, что ParamPattern может содержать hexadecimal символы, поэтому в
		некоторых случаях может быть неоднозначный парсинг или ложное срабатывание
		(найтись параметры в имени, просто содержащем кучу цифр, но не являющийся 
		никаким params pattern'ом). 

		Последняя точка гарантированно отделяет имя от всего остального, т.к.
		в ParamsPattern мы запретили точку.

*/

namespace MBD
{

	struct ParamOffset
	{ 
		byte offs, size;

		uint GetParamValue(const FileItem* fileitem) const
		{
			byte* ptr = (byte*)fileitem + offs;
			return (size == 1) ? *ptr : *(ushort*)ptr;
		}

		void Set(FileItem* fileitem, uint value) const
		{
			byte* ptr = (byte*)fileitem + offs;
			if (size == 1) {
				*ptr = (byte)value;
			} else {
				*(ushort*)ptr = (ushort)value;
			}
		}

		bool CheckRange(uint x) const
		{
			uint max = 1 << (size * 8);
			return InRange(x, 0, max-1);
		}
	};


	// Appends params to file extension.
	// Doesn't add a point. Skips gap spaces!
	extern Result NameMBD2PC_AppendParams(String<char>& result, const FileItem* fileitem, FILE_NAMING_MODE& fmode);

	// Build Windows file name of bs-dos file
	extern Result NameMBD2PC_File(String<char>& result, const FileItem* fileitem, FILE_NAMING_MODE& fmode);

	// Convert BS-DOS file name to windows. Result is appended to "result".
	extern Result NameMBD2PC_Simple(String<char>& result, const byte* name, size_t nameLen, bool isDir);

	// Parse name (using NamePC2MBD_Simple) and params (including type).
	// If params and type are ok, sets FileItem fields, otherwise doesn't change them.
	extern void NamePC2MBD_FilenameParse(
		const char* name, size_t nameLen, 
		FileItem* fileItem, bool* out_params_found);

};
