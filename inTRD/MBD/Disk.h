﻿
#pragma once

#include "..\common.h"
#include "..\MyStructs\Result.h"
#include "..\TAP\Structs.h"

namespace MBD
{
	#pragma pack(push, 1)
	struct FileItem
	{
		byte ID;
		int FileDateTime;
		TAP::Header Header;
		WORD BodyAdditionalAddr;
		uint BodyLength;
		byte BodyFlags;
		byte FileAttributes;
		WORD fileStartSector;	// bits 0-13 only; if BodyLength==0 then this field is not valid

		// Caller must check returned value for correctness!
		// For zero-length file returns UINT_MAX!
		uint FileStartSector() { return (BodyLength == 0) ? UINT_MAX : (fileStartSector & 0x3FFF); }

		//bool IsHeaderless() { return (ID & 0x10) == 0; }	// not used
		
		// Returns true if item represents valid existing file (not directory)
		bool IsValidFile() const { return (ID & 0xCF) == 0x80; }
	};
	#pragma pack(pop)

};
