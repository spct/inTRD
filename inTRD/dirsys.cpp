﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

#include "dirsys.h"
#include "common.h"

bool DirSys_Exists(byte* image, uint size)
{
	// valid dirsys takes at least 2 sectors (267+1 bytes)
	// max size is 267 + 11*127 + 1 = 1665 = 7 sectors
	if (size < 0xB00) {
		return false;
	}
	return memeq(image + 0x902, "DirSys1", 7);
}

// Scan dirsys structure to find its actual size.
// Returns the position of terminal zero byte.
// Does not check dirsys header (call DirSys_Exists before).
// If dirsys structure looks corrupted, returns 0.
uint DirSys_End(byte* image, uint size)
{
	// Scan directory names to find actual dirsys size
	uint pos = 0x900 + 2 + 9 + 128 + 127 + 1;
	uint dirCnt = 0;
	while(true) {
		if (pos >= size) {
			// disk image truncated
			return 0;
		}
		if (image[pos] == 0) {
			break;
		}
		pos += 11;
		dirCnt++;
		if (dirCnt > 127) {
			// terminal zero not found
			return 0;
		}
	}
	return pos;
}

ushort DirSys_CRC(byte* data, uint count)
{
    ushort crc = 0;
    for (uint i = 0; i < count; i++)
    {
        crc ^= data[i];
        for (int j = 0; j < 8; j++)
        {
			crc = (crc >> 1) | (crc << 15);
            if ((short)crc < 0)
				crc ^= 0xA001;
        }
        crc ^= (data[i] << 8);
    }
	return crc;
}
