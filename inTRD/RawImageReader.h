﻿
#include "common.h"
#include "MyStructs\Result.h"

class RawImageReader
{
public:

	// Open SCL/TRD image and test it. 
	// If image is corrupt, show error message and close reader.
	virtual Result OpenAndTest(const WCHAR* path) =0;
	
	// Move to next file if any.
	// out_headerPtr: return ptr to 14-byte TR-DOS file header.
	// out_bodyPtr: return ptr to file body.
	// out_bodySize: return file body size.
	virtual bool GetNextFile(const byte** out_headerPtr, const byte** out_bodyPtr, uint* out_bodySize) =0;
	
	virtual Result Close() =0;
	
	virtual ~RawImageReader() {};

protected:

	// Show modal message box with two lines of text
	void ShowErrorMsg(const WCHAR* part1, const WCHAR* part2);
};

// Create RawSclReader or RawTrdReader instance
extern RawImageReader* CreateRawImageReader(char type);
