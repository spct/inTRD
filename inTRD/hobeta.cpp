﻿
#include "hobeta.h"

void WriteHobetaHeaderSum(HOBETA_HEADER* header)
{
	uint sum = 0;
	for(uint i = 0; i < 15; i++) sum += header->Name[i];
	sum += (sum << 8) + 105;
	header->Checksum = (ushort)sum;
};

bool CheckHobetaChecksum(HOBETA_HEADER* header)
{	
	uint sum = 0;
	for(uint i = 0; i < 15; i++) sum += header->Name[i];
	sum += (sum << 8) + 105;
	return (ushort)sum == header->Checksum;
};

Result CheckHobeta(byte* data, uint size, bool* out_isHobeta)
{
	*out_isHobeta = false;
	HOBETA_HEADER* h = (HOBETA_HEADER*)data;
	if ((byte)size == 17 && CheckHobetaChecksum(h))
	{
		if (h->Size != size - 17)
		{
			Result res = Result(E_BAD_DATA);
			res.SetMessage("Bad hobeta file: invalid size in hobeta header", true);
			return res;
		}

		*out_isHobeta = true;
	}
	return SUCCESS;
};
