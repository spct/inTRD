﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

#include "Format_TRD.h"
#include "ArcHandle.h"
#include "RawImageReader.h"
#include "MyStructs\EncodingHelpers.h"
#include "dirsys.h"

// Compute actual image size from file size and disk type
uint GetDiskSize(uint imageSize, byte diskType)
{
	uint limit = 
		diskType == 0x17 ? 40 * 2 * 4096 :
		diskType == 0x18 ? 80 * 1 * 4096 :
		diskType == 0x19 ? 40 * 1 * 4096 :
		256 * 4096;	// для нормальных дисков количество секторов определяем по размеру образа

	return min(imageSize, limit);
}

// Create Entry from n-th entry in TRDOS catalogue and Add to arc->Entries
Result addEntry(ArcHandle& arc, uint n, NAME_HEADER& nameheader)
{
	Result res;
	byte* disk = arc.Adr();
	uint diskSize = GetDiskSize(arc.Size(), disk[DISK_TYPE]);
	const TRD_HEADER* h = &((TRD_HEADER*)disk)[n];

	Entry* e;
	if (!(res = arc.CreateEntry(&e))) {
		return res;
	}
	e->Header = h;
	e->NameHeader = nameheader;
	e->CreateDate = FileCreateDateData(h->Start, h->Length);

	e->IsRealFile = true;
	e->FileN = n;

	e->FileDataPos = h->GetStartSector() * 256; // В случае ошибки будет число заведомо за пределами архива
	e->SizeInSectors = h->Sectors;

	bool IsBasic = (h->Extension == 'B');
	uint autostart = 0;		// for Basic; 0 = no autostart
	uint smartFileSize;		// smart-длина файла; для basic - без учёта 4 байт автостарта
	uint extraDataSize = 0;	// 0, либо 4 - для basic-файла с байтами автостарта
		
	if (!IsBasic)
	{
		// If file size in Length param matches sector size, use Length, otherwise use sectors size
		smartFileSize = (h->Length + 255) / 256u == h->Sectors
			? h->Length
			: h->Sectors * 256;
	}
	else
	{
		// Smart-compute Basic file size.
		// Мы можем взять длину basic-программы из заголовка, если можем доверять данным из него, 
		// а именно:
		// - длина с переменными не меньше длины_без_переменных;
		// - длина_с_переменными+4 совпадает с секторной длиной; UPD: а вот этот пункт лишний! Ведь файл может содержать оверлеи.
		// - по смещению Start есть 4 байта автостарта
		//   (в TRDOS любой basic файл - даже без автостарта - имеет эти байты; значение 0 означает отсутствие автостарта)

		smartFileSize = h->Sectors * 256;

		if (h->Start >= h->Length &&
			//(h->Start + 4 + 255) / 256u == h->Sectors &&
			h->Start + 4u <= h->Sectors * 256u && 
			e->FileDataPos + h->Start + 4u <= diskSize &&
			*(ushort*)(disk + e->FileDataPos + h->Start) == 0xAA80)
		{
			autostart = *(ushort*)(disk + e->FileDataPos + h->Start + 2);
			if ((h->Start + 4 + 255) / 256u == h->Sectors)
			{
				smartFileSize = h->Start;
				extraDataSize = 4;
			}
		}
	}

	if (arc.Cfg.SmartLenDetect())
		e->FileDataSize = smartFileSize + extraDataSize;
	else
		e->FileDataSize = h->Sectors * 256;

	// Следующие два if'а взаимоисключающи

	if (arc.Cfg.ExtractHobeta())
	{
		e->FileDataSize = h->Sectors * 256;

		e->HobetaHeaderProvided = true;
		memcpy_(&e->HobetaHeader, h, 8+1+2+2);
		e->HobetaHeader.Size = h->Sectors * 256;
		WriteHobetaHeaderSum(&e->HobetaHeader);
	}

	if (arc.Cfg.InMBDCompatible)
	{
		e->MBD_Mode = true;

		// Сформируем MBD::FileItem для файла типа CODE

		e->MBDFileInfo.ID = 0xB0;
		e->MBDFileInfo.FileDateTime = arc.ArchiveDateTC;
			
		fill_array(e->MBDFileInfo.Header.Name, ' ');
		memcpy_(e->MBDFileInfo.Header.Name, h->Name, 8);
			
		e->MBDFileInfo.Header.Type = (h->Extension == 'C') ? 3 : h->Extension;
		e->MBDFileInfo.Header.Length = h->Length;
		e->MBDFileInfo.Header.Param1 = h->Start;
		e->MBDFileInfo.Header.Param2 = 32768;

		e->MBDFileInfo.BodyAdditionalAddr = 0; // In general, body address value should be the same as ADRESS parameter in header. But it does not matter what value is here, you can put here #0000 :)  (c) Busy
		e->MBDFileInfo.BodyLength = e->FileDataSize;
		e->MBDFileInfo.BodyFlags = 255;
		e->MBDFileInfo.FileAttributes = 0;
		//e->MBDFileInfo.fileStartSector = 0xFFFF;

		// Подкорректируем, если это Basic
		if (IsBasic)
		{
			e->MBDFileInfo.Header.Type = 0;
			e->MBDFileInfo.Header.Length = h->Start;
			e->MBDFileInfo.Header.Param1 = (autostart == 0) ? 0x8000 : autostart;
			e->MBDFileInfo.Header.Param2 = h->Length;

			if (arc.Cfg.SmartLenDetect()) {
				e->FileDataSize = e->MBDFileInfo.BodyLength = smartFileSize;  // cut off 4 bytes of autostart
			}
		}
	}

	return SUCCESS;
}

bool FilesIntersect(const TRD_HEADER* h1, const TRD_HEADER* h2, uint diskSize)
{
	uint start1 = h1->GetStartSector();
	uint end1 = start1 + h1->Sectors;
	uint start2 = h2->GetStartSector();
	uint end2 = start2 + h2->Sectors;
	return 
		start1 < diskSize && start2 < diskSize &&
		!(start1 >= end2 || end1 <= start2);
}

// Build list of files, populating arc->Entries
Result TRD_FillEntries(ArcHandle& arc)
{
	// Метод ArcHandle::Open уже проверил, что размер архива не менее 0x900 байт.

	Result res;
	byte* disk = arc.Adr();
	uint diskSize = GetDiskSize(arc.Size(), disk[DISK_TYPE]);
	TRD_HEADER* headers = (TRD_HEADER*)disk;
	
	// TODO: trdos не показывает (list, cat) файлы после нулевого байта, 
	// команды LOAD, RUN также их не видят.
	// Мне в режиме не-showDeletedFiles надо сделать так же.
	// Только надо продумать, что делать с такими дисками при модификации.
	// Ведь диск либо испорчен, либо это защита какая. Безопасно ли изменять дакой диск?
	// Если решаем изменять, то надо продумать, в какое место писать добавляемые файлы.
	// В конец, на свободное место в каталоге? Тогда он станет невидимым. (так делает trdos).
	// На место этого нулевого байта? Тогда файлы после него станут видимыми, но фактически 
	// они испорчены (затёрты новым файлом).

	uint fileCount = 128;
	while(fileCount > 0 && headers[fileCount - 1].Name[0] <= 1) {
		fileCount--;
	}

	for(uint i=0; i < fileCount; i++)
	{
		//if (i * 16 + 16 > diskSize) return Result(E_BAD_ARCHIVE); // это не может сработать
		TRD_HEADER* h = &headers[i];
		
		bool isDeleted = (h->Name[0] <= 1);
		if (isDeleted && !arc.Cfg.ShowDeletedFiles) {
			continue;
		}
		
		if (!(res = addEntry(arc, i, *(NAME_HEADER*)h))) {
			return res;
		}
	}

	if (arc.Cfg.ShowDeletedFiles)
	{
		// Since v6.40 display more deleted files. Scan all 128 entries and display those
		// not intersecting with non-deleted ones. See INFERN42.TRD image.
		for(uint i = fileCount; i < 128; i++)
		{
			TRD_HEADER* h = &headers[i];
			int* b = (int*)h;
			if ((b[0] | b[1] | b[2] | b[3]) != 0) // entry with non-zero bytes - potentially deleted file
			{
				bool intersectsNondeletedFile = false;
				for(uint j = 0; j < fileCount && !intersectsNondeletedFile; j++)
					intersectsNondeletedFile |= FilesIntersect(h, &headers[j], diskSize);

				if (!intersectsNondeletedFile)
				{
					NAME_HEADER fixedHeader;
					memcpy_(&fixedHeader, h, sizeof(fixedHeader));
					fixedHeader.Name[0] = 0; // считаю недопустимым, чтобы удалённые файлы выглядели как неудалённые
					if (!(res = addEntry(arc, i, fixedHeader))) {
						return res;
					}
				}
			}
		}
		// TODO: пофиксить операцию тестирования архива так, чтобы файлы, найденные здесь,
		// не тестировались.
	}

	return SUCCESS;
};

// Check that file is within image boundaries
bool TRD_CheckFileReadable(ArcHandle* arc, Entry* entry)
{
	// здесь я намеренно проверяю размер образа, а не форматированный 
	// размер диска (который может быть меньше у одностороннего или 40-дорожечного диска), 
	// т.к. не вижу смысла при чтении ограничиваться заявленным размером диска, а не реальным размером образа.
	uint64 fileEnd = (uint64)entry->FileDataPos + entry->FileDataSize;
	uint arcEnd = arc->Size();
	return (fileEnd <= arcEnd);
};


// Create/update image.
// Adding: false = create new image; true = update existing one;
// newArc: save image to this file (if operation succeeds);
// SrcPath: source dir of packed files;
// AddList: files to pack (paths relative to SrcPath);
Result TRD_PackFiles(bool Adding, ArcHandle& arc, FileHandle& newArc, WCHAR* SrcPath, WCHAR* AddList, CONFIG& cfg)
{
	Result RESULT_FILETOOBIG(E_NOT_SUPPORTED);
	RESULT_FILETOOBIG.SetMessage("Cannot pack files larger than 65280 bytes.", true);
	
	String<WCHAR> path;
	Result res = SUCCESS; 
	byte* image = NULL;	// allocated with myalloc()
	
	uint imageSize = Adding ? arc.Size() : 655360;
	if (!InRange(imageSize, 0x900, 256 * 4096))
	{ 
		// Вообще-то такая проверка есть в OpenArchive, но на всякий случай...
		res = Result(E_BAD_ARCHIVE);
		goto k9; 
	}

	image = myalloc_clear<byte>(imageSize);
	if (!image) {
		res = RESULT_NO_MEM;
		goto k9;
	}
	
	byte* disk = image;
	uint diskSize;
	TRD_HEADER* headers = (TRD_HEADER*)disk;
	TRD_SystemSector* syssec = (TRD_SystemSector*)(disk + 0x800);

	if (Adding)
	{
		// Read existing image to buffer
		memcpy_(image, arc.Adr(), arc.Size());
		diskSize = GetDiskSize(imageSize, syssec->DiskType);
	}
	else
	{
		// Format new disk
		syssec->FirstFreeSector_T = 1;
		syssec->DiskType = 0x16;
		diskSize = GetDiskSize(imageSize, syssec->DiskType);
		syssec->FreeSectorCount = (WORD)max(0, (int)(diskSize / 256u - 16));	// проверено, что используется знаковое сравнение
		syssec->Mark_10 = 0x10;
		fill_array(syssec->unused3, 32);
		fill_array(syssec->DiskName, ' ');
	}


	uint fileCount = 0;
	uint curSector = 16;	// sector number for next added file
	if (Adding)
	{
		// Find last file in existing image
		fileCount = 128;
		while (fileCount > 0 && headers[fileCount - 1].Name[0] <= 1) {
			fileCount--;
		}
	}

	// calc next free sector
	for(uint i=0; i < fileCount; i++) {
		uint fileEnd = headers[i].GetStartSector() + headers[i].Sectors; // В случае ошибки будет число заведомо за пределами архива
		curSector = max(curSector, fileEnd);
	}

	//if (curSector < 16)  // Impossible
	//{
	//	res = Result(E_BAD_ARCHIVE);
	//	res.SetMessage("TRD image is corrupted or protected and cannot be modified", true);
	//	goto k9;
	//}

	WCHAR* nextItem = NULL;
	for(WCHAR* listItem = AddList; *listItem; listItem = nextItem)
	{
		WCHAR* wname = listItem;
		for(nextItem = listItem; *nextItem; nextItem++) {
			if (IsSlash(*nextItem))
				wname = nextItem + 1;
		}
		nextItem++;

		if (*wname == 0) {
			continue; // skip directories
		}

		// path = full path to the file being packed
		res = path.Set(SrcPath);
		res &= path.AppendPath(listItem);
		if (!res) {
			goto k9;
		}

		bool isScl = ends_with_ci(wname, L".scl");
		bool isTrd = !isScl && ends_with_ci(wname, L".trd");
		if (isScl || isTrd)
		{
			// Raw-copy files from SCL or TRD image
			res = RESULT_NO_MEM;
			RawImageReader* sclTrd = CreateRawImageReader(isScl ? 's' : 't');
			if (sclTrd)
			{
				res = sclTrd->OpenAndTest(path.GetPtr());
				if (res)
				{
					while(true) // iterate through files inside image
					{
						const byte* pHeader; // ptr to 14-byte TR-DOS file header
						const byte* pBody;
						uint bodySize;

						// since we are copying from SCL or TRD image, file body already contains autostart bytes
						const uint add_basic_autostart_bytes = 0;

						if (!sclTrd->GetNextFile(&pHeader, &pBody, &bodySize)) {
							// No more files
							res = Result(E_END_ARCHIVE);
							break;
						}

						{
							fileCount++;
							if (fileCount > 128)
							{
								res = Result(E_TOO_MANY_FILES);
								res.SetMessage("TRD image cannot contain more than 128 files.", true);
								break;
							}

							TRD_HEADER& header = headers[fileCount - 1];
							clear_struct(header);

							memcpy_(&header, pHeader, 14);

							// пункт 3) заполнить поля в header: Sectors, StartTrack, StartSector

							if (bodySize + add_basic_autostart_bytes > 0xFF00) {
								res = RESULT_FILETOOBIG;
								break;
							}
							header.Sectors = (bodySize + add_basic_autostart_bytes + 255) / 256u;

							if ((curSector + header.Sectors) * 256 > diskSize) {
								res = Result("Not enough space in TRD image.", true);
								break;
							}

							header.StartSector = curSector % 16u;
							header.StartTrack = curSector / 16u;

							// пункт 4) копируем содержимое файла,
							// добавляем байты автостарта, если надо.

							size_t p = curSector * 256;
							memcpy_(disk + p, pBody, bodySize);
							p += bodySize;
							if (add_basic_autostart_bytes) // const false
							{
								//disk[p++] = 0x80;
								//disk[p++] = 0xAA;
								//disk[p++] = autostart;
								//disk[p++] = autostart >> 8;
							}
							while(p % 256u != 0) disk[p++] = 0;

							// готово, переходим к следующему файлу

							curSector += header.Sectors;
						}
					} // while

					if (!res && res.WcxCode() == E_END_ARCHIVE) {
						res = SUCCESS;
					}
					sclTrd->Close();
				}
				delete sclTrd;
			}
			if (!res) {
				goto k9;
			}
		}
		else
		{
			// Pack one file whose full path is in path var

			fileCount++;
			if (fileCount > 128)
			{
				res = Result(E_TOO_MANY_FILES);
				res.SetMessage("TRD image cannot contain more than 128 files.", true);
				goto k9;
			}


			FileHandle f;
			res = f.Open(path.GetPtr(), false, OPEN_EXISTING, true, true, 0xFF00+sizeof(HOBETA_HEADER), RESULT_FILETOOBIG);
			if (!res) {
				goto k9;
			}

			byte* data = f.Adr;
			uint size = (uint)f.Size;

			TRD_HEADER& header = headers[fileCount - 1];
			clear_struct(header);

			uint add_basic_autostart_bytes = 0; // either 0 or 4
			uint autostart = 0; // 0 = no autostart

			// Convert WCHAR to char
			char name[260];
			if (!Convert_W_to_A(wname, name, sizeof(name)))	{
				res = Result("Invalid file path to pack", true);
				goto k9;
			}

			/*
			 * План действий:
			 * 1) заполнить поля в header: Name, Extension, Start, Length;
			 * 2) если это basic, то определить, надо ли добавлять байты autostart;
			 * 3) заполнить поля в header: Sectors, StartTrack, StartSector;
			 * 4) скопировать содержимое файла; для basic добавить 4 байта автостарта, если их там ещё нет;
			 */

			// Если имя похоже на сформированное inMBD, надо взять всё из него:
			// имя, Start, Length, autostart.
			// При этом на hobeta не проверяем.

			MBD::FileItem mbd;
			bool is_mbd;
			MBD::NamePC2MBD_FilenameParse(name, strlen_(name), &mbd, &is_mbd);
			if (is_mbd)
			{
				memcpy_(header.Name, mbd.Header.Name, 8); // и TRD_HEADER и TAP HEADER используют паддинг пробелами
		
				// Сформируем для файла типа CODE
				header.Extension = (mbd.Header.Type == 3) ? 'C' : mbd.Header.Type;
				header.Start = mbd.Header.Param1;
				header.Length = mbd.Header.Length;
				if (mbd.Header.Type == 0)
				{
					// Сформируем для файла типа BASIC
					header.Extension = 'B';
					header.Start = mbd.Header.Length;
					header.Length = mbd.Header.Param2;
				
					// Valid values for START parameter of BASIC are values from range 0..#3FFF. #4000 and more means no autostart. When it is needed to creat basic wihtout autostart, the value #8000 can be used (zx rom uses this #8000). (c) Busy
					if (mbd.Header.Param1 < 0x4000) {
						autostart = mbd.Header.Param1;
					}
				
					// Надо ли добавлять байты автостарта?
					// Не забываем, что они должны быть по смещению Start, иначе в них нет смысла.
					// 1) если по смещению Start уже есть байты автостарта, то очевидно, что не надо добавлять.
					// 2) если длина файла не равна Start то не надо.
					//    (иначе придётся писать байты в середину файла, а портить содержимое файла не стоит;
					//     кроме того, в этом случае они там, скоре всего, и так есть)
					// 3) иначе надо.
					if (header.Start == size) {
						add_basic_autostart_bytes = 4;
					}
				}
			}
			else
			{
				bool hasHobetaHeader = false, hasHobetaExtension = false;
				if (cfg.DetectHobeta) {
					if (!(res = CheckHobeta(data, size, &hasHobetaHeader))) {
						goto k9;
					}
				}

				NAME_HEADER& name_header = (NAME_HEADER&)header;
				NamePC2ZX(name, &name_header, cfg, hasHobetaHeader, &hasHobetaExtension); // Name, Ext, ?Start?
		
				if (hasHobetaHeader && hasHobetaExtension)
				{
					memcpy_(&header, data, 8+1+2+2); // Name, Ext, Start, Length
					data += 17; // skip hobeta header
					size -= 17; //
				}
				else
				{
					// С параметром Start поступаем следующим образом:
					// 1) если Start и Length закодированы в дате файла - берём их оттуда;
					// 2) иначе если первый символ расширения - "B", то ставим его равным Length (длина basic-программы);
					//      2b) если файл заканчивается на 80 AA xx xx, то уменьшить Start и Length на 4.
					// 3) иначе отдаём на откуп функции NamePC2ZX, а именно:
					//		a) если опция ExtendedExtension включена, расширение имеет длину >= 2 и символы 1,2 имеют коды от 33(32) до 127, то записываем extended extension;
					//		b) иначе записываем 0.
					//
					// TODO: надо подумать, что делать, если сработает пункт 3 и первый символ - "B" и далее ещё пара символов.
					// Ведь тогда может получиться файл типа Basic с параметром start не похожим на длину basic-программы.

					// сейчас header.Start соответствует пункту 3), а Length=0

					FILETIME dt;
					if (!GetFileTime(f.Handle(), &dt, NULL, NULL)) {
						res = Result(E_EOPEN);
						goto k9;
					}
					FileCreateDateData createDate(dt);

					if (createDate.Has_StartLength)
					{
						header.Start = createDate.Start;
						header.Length = createDate.Length;
					}
					else
					{
						header.Length = size;
						if (name_header.Extension == 'B')
						{
							header.Start = size;
							if (size >= 4 && *(ushort*)(data + size - 4) == 0xAA80) {
								header.Start = header.Length = size - 4;
							}
						}
					}
				}
			}

			// пункт 3) заполнить поля в header: Sectors, StartTrack, StartSector

			if (size + add_basic_autostart_bytes > 0xFF00) {
				res = RESULT_FILETOOBIG;
				goto k9;
			}
			header.Sectors = (size + add_basic_autostart_bytes + 255) / 256u;

			if ((curSector + header.Sectors) * 256 > diskSize) {
				res = Result("Not enough space in TRD image.", true);
				goto k9;
			}

			header.StartSector = curSector % 16u;
			header.StartTrack = curSector / 16u;

			// пункт 4) копируем содержимое файла,
			// добавляем байты автостарта, если надо.

			size_t p = curSector * 256;
			memcpy_(disk + p, data, size);
			p += size;
			if (add_basic_autostart_bytes)
			{
				disk[p++] = 0x80;
				disk[p++] = 0xAA;
				disk[p++] = autostart;
				disk[p++] = autostart >> 8;
			}
			while(p % 256u != 0) disk[p++] = 0;

			// готово, переходим к следующему файлу

			curSector += header.Sectors;

			f.Close();
		}
	}

	// Update sys sector

	uint delCount = 0;
	for(uint i = 0; i < fileCount; i++) {
		if (headers[i].Name[0] <= 1) 
			delCount++;
	}
	for(uint i = fileCount; i < 128; i++) {
		headers[i].Name[0] = 0;
	}
	
	syssec->DeletedFileCount = delCount;
	syssec->FileCount = fileCount;
	syssec->FirstFreeSector_S = curSector % 16u;
	syssec->FirstFreeSector_T = curSector / 16u;
	syssec->FreeSectorCount = (WORD)max(0, (int)(diskSize / 256u - curSector)); // signed comparison

	// Finally write save modified image.

	res = newArc.Write(image, imageSize) ? SUCCESS : Result(E_EWRITE);

k9:
	myfree(image);
	return res;
};

// Delete files from image. Does not defragment image.
Result TRD_DeleteFiles(ArcHandle& arc, const WCHAR* DeleteList, CONFIG& cfg)
{
	// Процедура DeleteFiles уже прогнала проверку архива,
	// так что можем расчитывать, что он не сильно испорчен.
	// UPD: этих проверок недостаточно, например, для диска INSTY_10.TRD,
	// поэтому мы немного усложняем логику вычисления First free sector
	// и Free sector count.
	//
	// Учитываем, что в архиве могут быть файлы с одинаковым именем,
	// поэтому смотреть по имени не надо, придётся положиться на то,
	// что файлы в списке DeleteList идут в том порядке, в котором
	// они возвращалсь функцией ReadHeader.
	// UPD: добавлен dup-counter, теперь можно полагаться на имя.
	// Но решено не переделывать: даже если порядок в DeleteList не гарантирован,
	// сильно страшного не произойдёт, просто могут не все выбранные файлы удалиться.

	// Поскольку мы будем изменять только каталог и dirsys,
	// сделаем его копию и потом запишем его весь сразу,
	// не будем открывать временный архив.

	// catalog + sys sector + dirsys <= 0x1000
	uint arcData_size = min(arc.Size(), 0x1000);
	byte arcData[0x1000];
	clear_array(arcData);
	memcpy_(arcData, arc.Adr(), arcData_size);

	// Determine which files to delete

	const uint MAX_FILES = 128;
	bool delete_[MAX_FILES];
	Entry* realFileEntries[MAX_FILES];
	clear_array(delete_);
	clear_array(realFileEntries);

	const WCHAR* listItem = DeleteList;
	for(uint i = 0; i < arc.Entries.Count; i++)
	{
		Entry* e = arc.Entries[i];
		if (e->IsRealFile) {
			realFileEntries[e->FileN] = e;
		}

		if (*listItem != 0)
		{
			// Get entry name and convert it from char to WCHAR
			char name[260];
			WCHAR nameW[260];
			Result res = GetName(*e, name, cfg, true);
			if (res) {
				res = Convert_A_to_W(name, nameW, ARRAY_LEN(nameW)) 
					? SUCCESS : Result("Error reading archive headers", true);
			}
			if (!res) {
				return res;
			}

			if (streq(nameW, listItem))
			{
				// Удалять разрешаем как real, так и autojoined файлы
				uint n = e->IsJoinedFile ? e->JoinedFileStart : e->FileN;
				uint q = e->IsJoinedFile ? e->JoinedFileCount : 1;
				if (n + q > MAX_FILES) {
					return Result("Internal error DF09", true); // should never happen
				}
				for(uint j = 0; j < q; j++) {
					delete_[n + j] = true;
				}
				while(*listItem++) {} // move to next item in DeleteList
			}
		}
	}

	byte* fileDirTable = arcData + 0x900 + 2 + 9;
	uint dirsysEnd = DirSys_Exists(arcData, arc.Size()) ? DirSys_End(arcData, arc.Size()) : 0;
	bool dirsys = (dirsysEnd != 0);
#ifdef _DEBUG
	if (dirsys) {
		uint crc1 = DirSys_CRC(arcData + 0x902, dirsysEnd - 0x902);
		if (arcData[0x900] != crc1>>8 || arcData[0x901] != (byte)crc1) _throw("bad dirsys crc");
	}
#endif

	TRD_HEADER* headers = (TRD_HEADER*)arcData;

	// Actually mark files deleted
	for(uint i = 0; i < MAX_FILES; i++) {
		if (realFileEntries[i] != NULL)	{
			if (delete_[i])	{
				headers[i].Name[0] = 0;
			}
		}
	}

	// Find new end of catalog
	uint fileCount = 128;
	while(fileCount > 0 && headers[fileCount - 1].Name[0] <= 1) {
		headers[fileCount - 1].Name[0] = 0;
		if (dirsys) {
			fileDirTable[fileCount - 1] = 0;
		}
		fileCount--;
	}

	// count deleted files
	uint delCount = 0;
	for(uint i = 0; i < fileCount; i++) {
		if (headers[i].Name[0] <= 1) {
			headers[i].Name[0] = 1;
			delCount++;
		}
	}

	// Compute firstFreeSector.
	// Can't simply take last file header, for corrupted/protected disk result may be
	// nonsensical, leading to further corruption. Example of such disk is INSTY_10.TRD. 
	// So, take max of all file headers.
	uint firstFreeSector = 16;
	for(uint i=0; i < fileCount; i++) {
		uint fileEnd = headers[i].GetStartSector() + headers[i].Sectors;
		firstFreeSector = max(firstFreeSector, fileEnd);
	}

	// Update sys sector

	TRD_SystemSector* syssec = (TRD_SystemSector*)(arcData + 0x800);

	uint diskSize = GetDiskSize(arc.Size(), syssec->DiskType);
	WORD freeSectors = (WORD)max(0, (int)(diskSize / 256u - firstFreeSector));

	firstFreeSector = min(firstFreeSector, 255 * 16 + 15);  // prevent FirstFreeSector_T overflow

	syssec->DeletedFileCount = delCount;
	syssec->FileCount = fileCount;
	syssec->FirstFreeSector_S = firstFreeSector % 16u;
	syssec->FirstFreeSector_T = firstFreeSector / 16u;
	syssec->FreeSectorCount = freeSectors;

	// Update dirsys checksum
	if (dirsys) {
		//dirsysEnd = DirSys_End(arcData, arc.Size()); // couldn't change
		uint crc = DirSys_CRC(arcData + 0x902, dirsysEnd - 0x902);
		arcData[0x900] = byte(crc >> 8);
		arcData[0x901] = byte(crc >> 0);
	}

	// Save catalog and dirsys
	memcpy_(arc.Adr(), arcData, arcData_size);
	
	return SUCCESS;
};

// Delete files from image, then defragment image
Result TRD_DeleteFiles_WithDefragment(ArcHandle& arc, const WCHAR* DeleteList, FileHandle& newArc, CONFIG& cfg)
{
	// Процедура DeleteFiles уже прогнала проверку архива,
	// так что можем расчитывать, что он не сильно испорчен.

	byte* arcData = myalloc<byte>(arc.Size());
	if (!arcData) {
		return RESULT_NO_MEM;
	}

	memcpy_(arcData, arc.Adr(), arc.Size()); // read image to buffer

	// Determine which files to delete

	const uint MAX_FILES = 128;
	bool delete_[MAX_FILES];
	Entry* realFileEntries[MAX_FILES];
	clear_array(delete_);
	clear_array(realFileEntries);

	const WCHAR* listItem = DeleteList;
	for(uint i = 0; i < arc.Entries.Count; i++)
	{
		Entry* e = arc.Entries[i];
		if (e->IsRealFile) {
			realFileEntries[e->FileN] = e;
		}

		if (*listItem != 0)
		{
			// Get entry name and convert it from char to WCHAR
			char name[260];
			WCHAR nameW[260];
			Result res = GetName(*e, name, cfg, true);
			if (res) {
				res = Convert_A_to_W(name, nameW, ARRAY_LEN(nameW)) 
					? SUCCESS : Result("Error reading archive headers", true);
			}
			if (!res) {
				myfree(arcData);
				return res;
			}

			if (streq(nameW, listItem))
			{
				// Удалять разрешаем как real, так и autojoined файлы
				uint n = e->IsJoinedFile ? e->JoinedFileStart : e->FileN;
				uint q = e->IsJoinedFile ? e->JoinedFileCount : 1;
				if (n + q > MAX_FILES) { // should never happen
					myfree(arcData);
					return Result("Internal error DF09", true);
				}
				for(uint j = 0; j < q; j++) {
					delete_[n + j] = true;
				}
				while(*listItem++) {} // move to next item in DeleteList
			}
		}
	}

	byte* fileDirTable = arcData + 0x900 + 2 + 9;
	uint dirsysEnd = DirSys_Exists(arcData, arc.Size()) ? DirSys_End(arcData, arc.Size()) : 0;
	bool dirsys = (dirsysEnd != 0);
#ifdef _DEBUG
	if (dirsys) {
		uint crc1 = DirSys_CRC(arcData + 0x902, dirsysEnd - 0x902);
		if (arcData[0x900] != crc1>>8 || arcData[0x901] != (byte)crc1) _throw("bad dirsys crc");
	}
#endif

	const TRD_HEADER* oldHeaders = (TRD_HEADER*)arc.Adr();
	TRD_HEADER* newHeaders = (TRD_HEADER*)arcData;

	// Actually mark files deleted
	for(uint i = 0; i < MAX_FILES; i++) {
		if (realFileEntries[i] != NULL) {
			if (delete_[i]) {
				newHeaders[i].Name[0] = 0;
			}
		}
	}

	// Find new end of catalog
	uint fileCount = 128;
	while(fileCount > 0 && newHeaders[fileCount - 1].Name[0] <= 1) {
		newHeaders[fileCount - 1].Name[0] = 0;
		if (dirsys) {
			fileDirTable[fileCount - 1] = 0;
		}
		fileCount--;
	}

	// Do defragment the disk.
	// Перенесём из arc.Adr() в arcData всё файлы, которые остались не удалёнными

	uint newFileCount = 0, newFilePos = 4096;
	for(uint i = 0; i < fileCount; i++) {
		// следим за логикой, чтобы не перезаписать newHeaders, которые ещё нужны для определения isDeleted
		bool isDeleted = (newHeaders[i].Name[0] <= 1);
		if (!isDeleted) {
			TRD_HEADER h = oldHeaders[i];
			if (h.GetStartSector() * 256 + h.Sectors * 256 > arc.Size()) {
				// файл за пределами диска
				myfree(arcData);
				Result res = Result(E_BAD_ARCHIVE);
				res.SetMessage("TRD image is corrupted or protected and cannot be modified", true);
				return res;
			}
			if (newFilePos + h.Sectors * 256 > arc.Size()) {
				// в результате дефрагментации файлы перестали помещаться на диск;
				// это возможно, если диск был косячный.
				myfree(arcData);
				return Result("Not enough space in TRD image.", true);
			}

			// copy file data
			memcpy_(arcData + newFilePos, arc.Adr() + h.GetStartSector() * 256, h.Sectors * 256);

			// write new file header
			h.StartSector = newFilePos / 256u % 16u;
			h.StartTrack = newFilePos / 256u / 16u;
			newHeaders[newFileCount] = h;

			// update file location in dirsys
			byte t = fileDirTable[i];
			fileDirTable[i] = 0;
			fileDirTable[newFileCount] = t;

			newFilePos += h.Sectors * 256;
			newFileCount++;
		}
	}

	uint firstFreeSector = newFilePos / 256u;
	for(uint i = newFileCount; i < 128; i++) {
		newHeaders[i].Name[0] = 0;
	}

	// Update sys sector

	TRD_SystemSector* syssec = (TRD_SystemSector*)(arcData + 0x800);

	uint diskSize = GetDiskSize(arc.Size(), syssec->DiskType);
	WORD freeSectors = (WORD)max(0, (int)(diskSize / 256u - firstFreeSector)); // signed comparison

	firstFreeSector = min(firstFreeSector, 255 * 16 + 15);  // prevent FirstFreeSector_T overflow

	syssec->DeletedFileCount = 0;
	syssec->FileCount = newFileCount;
	syssec->FirstFreeSector_S = firstFreeSector % 16u;
	syssec->FirstFreeSector_T = firstFreeSector / 16u;
	syssec->FreeSectorCount = freeSectors;

	// Update dirsys checksum
	if (dirsys) {
		//dirsysEnd = DirSys_End(arcData, arc.Size()); // couldn't change
		uint crc = DirSys_CRC(arcData + 0x902, dirsysEnd - 0x902);
		arcData[0x900] = byte(crc >> 8);
		arcData[0x901] = byte(crc >> 0);
	}

	// Write modified image
	Result res = newArc.Write(arcData, arc.Size()) ? SUCCESS : Result(E_EWRITE);
	
	myfree(arcData);
	return res;
};
