﻿
#include "..\common.h"
#include "wdxhead.h"
#include "..\MyStructs\EncodingHelpers.h"  // Convert_A_to_W

// import from main.cpp
extern int __stdcall ContentGetValueW(WCHAR* FileName,int FieldIndex,int UnitIndex, void* FieldValue,int maxlen,int flags);

// ANSI version. Although we don't support TC 7.49-, the plugin can't be
// installed without this function.
int __stdcall ContentGetValue(char* FileName,int FieldIndex,int UnitIndex, void* FieldValue,int maxlen,int flags)
{
	// Convert filename to WCHAR
	WCHAR wname[300];
	if (!Convert_A_to_W(FileName, wname, ARRAY_LEN(wname))) {
		return ft_fileerror;
	}

	// Note: do not convert FieldValue from WCHAR as long as returning ft_string
	return ContentGetValueW(wname, FieldIndex, UnitIndex, FieldValue, maxlen, flags);
};
