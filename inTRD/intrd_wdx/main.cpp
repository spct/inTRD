﻿
#include "..\common.h"
#include "wdxhead.h"
#include "..\ArcHandle.h"
#include "..\Format_TRD.h"

// secure strcpy;
// maxlen: dest buffer size in bytes
void sstrcpy(char* dest, char* src, int maxlen)
{
	dest[0] = 0;
	if (maxlen <= 0) {
		return;
	}
	size_t sz = strlen_(src) + 1;
	sz = min(sz, (uint)maxlen);
	memcpy_(dest, src, sz);
	dest[maxlen - 1] = 0; // extra precaution
}

// ContentGetSupportedField is called to enumerate all supported fields. FieldIndex is 
// increased by 1 starting from 0 until the plugin returns ft_nomorefields.
// maxlen: The maximum number of characters, including the trailing 0, which may be returned in each of the fields
int __stdcall ContentGetSupportedField(uint FieldIndex, char* FieldName, char* Units, int maxlen)
{
	Units[0] = 0;
	switch(FieldIndex)
	{
		case 0: sstrcpy(FieldName, "Disk name", maxlen); return ft_string;
		case 1: sstrcpy(FieldName, "Disk type", maxlen); return ft_string;
		case 2: sstrcpy(FieldName, "Disk sectors", maxlen); return ft_numeric_32;
		case 3: sstrcpy(FieldName, "Used sectors", maxlen); return ft_numeric_32;
		case 4: sstrcpy(FieldName, "Free sectors", maxlen); return ft_numeric_32;
		case 5: sstrcpy(FieldName, "Free bytes", maxlen); return ft_numeric_32;
		case 6: sstrcpy(FieldName, "First free sector", maxlen); return ft_numeric_32;
		case 7: sstrcpy(FieldName, "First free track/sector", maxlen); return ft_string;
		case 8: sstrcpy(FieldName, "Files", maxlen); return ft_numeric_32;
		case 9: sstrcpy(FieldName, "Deleted files", maxlen); return ft_numeric_32;
		default: return ft_nomorefields;
	}
}

// maxlen: The maximum number of bytes fitting into the FieldValue variable;
//         Note: When using Unicode strings, you need to divide this value by 2 to get the maximum number of characters!
int __stdcall ContentGetValueW(WCHAR* FileName, int FieldIndex, int UnitIndex, void* FieldValue, int maxlen, int flags)
{
	ArcHandle arc;
	Result res = arc.Open(FileName, false, OPEN_EXISTING);
	if (!res) {
		return ft_fileerror;
	}

	int* res_i32 = (int*)FieldValue;
	int64* res_i64 = (int64*)FieldValue;
	char* res_str = (char*)FieldValue;

	// ArcHandle::Open ensures archive size is at least 0x900 bytes
	TRD_SystemSector* syssec = (TRD_SystemSector*)(arc.Adr() + 0x800);

	switch(FieldIndex)
	{
		case 0: /* Disk name */ {
			char buf[11 * 3 + 5];
			size_t len = 8 + 3;
			while(len > 0 && syssec->DiskName[len - 1] <= 32) { // Find name length
				len--;
			}
			byte* p = (byte*)buf;
			for(size_t i = 0; i < len; i++) {
				byte c = syssec->DiskName[i];
				if (InRange(c, 32, 126))
					*p++ = c;
				else
					*p++ = '#', *p++ = HexChars_Up[c / 16u], *p++ = HexChars_Up[c % 16u];  // # escape char
			}
			*p = 0;
			sstrcpy(res_str, buf, maxlen);
			return ft_string;
		}

		case 1: /* Disk type */ {
			char buf[20];
			switch(syssec->DiskType)
			{
				case 0x16: strcpy_(buf, "DS/80"); break;
				case 0x17: strcpy_(buf, "DS/40"); break;
				case 0x18: strcpy_(buf, "SS/80"); break;
				case 0x19: strcpy_(buf, "SS/40"); break;
				default: {
					strcpy_(buf, "Unknown (#00)"); 
					buf[10] = HexChars_Up[syssec->DiskType / 16u];
					buf[11] = HexChars_Up[syssec->DiskType % 16u];
					break;
				}
			}
			sstrcpy(res_str, buf, maxlen);
			return ft_string;
		}
		
		case 2: /* Disk sectors */ {
			switch(syssec->DiskType)
			{
				case 0x16: *res_i32 = 2 * 80 * 16; break;
				case 0x17: *res_i32 = 2 * 40 * 16; break;
				case 0x18: *res_i32 = 1 * 80 * 16; break;
				case 0x19: *res_i32 = 1 * 40 * 16; break;
				default: *res_i32 = -1; break;
			}
			return ft_numeric_32;
		}
		
		case 3: /* Used sectors */
			*res_i32 = syssec->FirstFreeSector_T * 16 + syssec->FirstFreeSector_S;
			return ft_numeric_32;
		
		case 4: /* Free sectors */
			*res_i32 = syssec->FreeSectorCount;
			return ft_numeric_32;
		
		case 5: /* Free bytes */
			*res_i32 = syssec->FreeSectorCount * 256;
			return ft_numeric_32;
		
		case 6: /* First free sector */
			*res_i32 = syssec->FirstFreeSector_T * 16 + syssec->FirstFreeSector_S;
			return ft_numeric_32;
		
		case 7: /* First free track/sector */ {
			char buf[20];
			utoa(syssec->FirstFreeSector_T, buf);
			strcat_(buf, " / ");
			utoa(syssec->FirstFreeSector_S, End(buf));
			sstrcpy(res_str, buf, maxlen);
			return ft_string;
		}
		
		case 8: /* Files */
			*res_i32 = syssec->FileCount;
			return ft_numeric_32;

		case 9: /* Deleted files */
			*res_i32 = syssec->DeletedFileCount;
			return ft_numeric_32;
		
		default:
			return ft_nosuchfield;
	}

	// NOTE:
	// ArcHandle destructor closes archive, no need to call Close()
}

// maxlen: Maximum length, in bytes, of the detection string (currently 2k)
int __stdcall ContentGetDetectString(char* DetectString,int maxlen)
{
	strcpy_(DetectString, "");
	static char s[] = "EXT=\"TRD\"";
	if ((ssize_t)strlen_(s) + 1 <= maxlen) {
		strcpy_(DetectString, s);
	}
	return 0; // The return value is unused and should be set to 0.
}

