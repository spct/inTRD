﻿
#pragma once

#include "common.h"
#include "MyStructs\List.h"
#include "MyStructs\FileHandle.h"
#include "MyStructs\WcxHelpers.h"
#include "settings.h"
#include "Format.h"
#include "Format_TRD.h"  // TRD_HEADER
#include "hobeta.h"
#include "MBD\Common.h"

struct Entry
{
	bool IsRealFile;				// false for some virtual entries
	bool IsJoinedFile;				// virtual entry for big (compound) file
	uint JoinedFileStart;			// для составного файла - номер первого файла, из которых состоит файл. Файлы идут подряд, начиная с указанного здесь.
	uint JoinedFileCount;			// для составного файла - кол-во файлов, из которых состоит файл.
	uint FileN;						// номер файла в архиве. Для составного файла - номер первого файла.
	uint FileDataSize;				// Unpacked size Без учёта hobeta header'а!
	uint FileDataPos;				// позиция файла в образе. Может указывать за пределы образа, если образ повреждён.
	uint SizeInSectors;				// for ShowSizeInSectors, also for displaying packed size
	struct NAME_HEADER NameHeader;	// data needed to build file name, that is trdos name + extension + start
	const struct TRD_HEADER* Header;// ptr to TRDOS file header
	uint DuplicateN;				// число entr'ей с таким же именем (по GetName) среди предыдущих по списку
	FileCreateDateData CreateDate;	// start/length values
	//bool Delete;					// Marked for deletion

	bool HobetaHeaderProvided;			// extract with hobeta header
	struct HOBETA_HEADER HobetaHeader;
	
	bool MBD_Mode;						// extract using inMBD-compatible mode
	struct MBD::FileItem MBDFileInfo;

	uint Size()						// final unpacked file size
	{
		return FileDataSize + (HobetaHeaderProvided ? 17 : 0);
	}
};

struct ArcHandle
{
	FileHandle File;
	CONFIG Cfg;

	List<Entry*> Entries;		// вызов конструктора List() происходит при создании ArcHandle.
	uint CurrentEntry;

	bool ProtectionDetected;	// если обнаружены косяки в структуре архива, то не разрешает изменять архив, чтобы окончательно его не угробить.
	int OpenMode;

	FILETIME ArchiveDateUtc;	// file write date
	DWORD ArchiveDateTC;		// file write date (for ReadHeader)
	FILETIME ArchiveCreateDateUtc;	// file creation date
	TCHAR ArcName[ARRAY_LEN_FIELD(tHeaderDataExW, ArcName)];	// original archive path

	//char Temp[100];				// для хранения и передачи временных данных

	byte* Adr() const { return File.Adr; }			// addr of mem-mapped file
	uint Size() const { return (uint)File.Size; }

	ArcHandle()
	{
		clear_this();
	};

	// Note: destructor calls Close()!
	~ArcHandle()
	{
		Close();
		for(uint i=0; i < Entries.Count; i++) delete Entries[i];
		Entries.Destroy();
		clear_this();
	};

	Result Open(const TCHAR* path, bool writeable, DWORD createDisposition);

	// Closes (if opened) archive file.
	Result Close()
	{
		return File.Close();
	};

	// Create new Entry and add it to Entries list
	Result CreateEntry(Entry** out_pEntry)
	{
		*out_pEntry = NULL;
		Entry* e = new Entry();
		if (!e) {
			return RESULT_NO_MEM;
		}
		clear_struct(*e);
		
		Result res = Entries.Add(e);
		if (!res) 
			delete e;
		else
			*out_pEntry = e;
		
		return res;
	};
};
