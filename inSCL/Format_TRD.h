﻿
#pragma once

#include "common.h"
#include "MyStructs\Result.h"
#include "ArcHandle.h"

#pragma pack(push, 1)

struct TRD_SystemSector
{
	byte Mark_0;			// "...байт со смещением #00. Его значение всегда должно быть нулевым" стр.183
	byte unused1[224];
	byte FirstFreeSector_S; // номер первого...
	byte FirstFreeSector_T; //            ...незанятого сектора
	byte DiskType;			// disk type (#16 for double side 80 tracks)
	byte FileCount;
	WORD FreeSectorCount;
	byte Mark_10;			// TR-DOS ID (#10)
	byte unused2[2];		// not used, filled with 0
	byte unused3[9];		// not used, filled with 32
	byte unused4[1];		// not used, filled with 0
	byte DeletedFileCount;
	byte DiskName[8];
	byte unused5[3];		// not used, filled with 0. Sometimes used for extended (11 chars) disk name.
};

struct TRD_HEADER
{
	byte Name[8];
	byte Extension;
	WORD Start;				// For CODE: load address. For BASIC: program+vars length.
	WORD Length;			// For CODE: code length. For BASIC: program length without vars.
	byte Sectors;
	byte StartSector;
	byte StartTrack;

	uint GetStartSector() const
	{
		return StartSector < 16
			? StartSector + StartTrack * 16 
			: 256 * 16 + 1; // invalid header. Число заведомо за пределами архива (в ArcHandle стоит проверка: размер файла не может быть больше 256*4096)
	};
};

#define DISK_TYPE 0x8e3

#pragma pack(pop)

//extern Result TRD_FillEntries(ArcHandle& arc);
//extern bool TRD_CheckFileReadable(ArcHandle* arc, Entry* entry);
//extern Result TRD_PackFiles(bool Adding, ArcHandle& arc, FileHandle& newArc, WCHAR* SrcPath, WCHAR* AddList, CONFIG& cfg);
//extern Result TRD_DeleteFiles(ArcHandle& arc, const WCHAR* DeleteList, CONFIG& cfg);
//extern Result TRD_DeleteFiles_WithDefragment(ArcHandle& arc, const WCHAR* DeleteList, FileHandle& newArc, CONFIG& cfg);
