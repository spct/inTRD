﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

#include "Format_SCL.h"
#include "ArcHandle.h"
#include "RawImageReader.h"
#include "MyStructs\EncodingHelpers.h"

// Build list of files, populating arc->Entries
Result SCL_FillEntries(ArcHandle& arc)
{
	// Метод ArcHandle::Open уже проверил, что размер архива 
	// не менее 8+1+4 и не более 16.649.983 байт.
	
	Result res;
	byte* scl = arc.Adr();

	for(uint j=0; j < 9; j++)
		arc.CalculatedSum += scl[j];

	uint fileCount = scl[8];
	uint fileDataOffset = 9 + fileCount * 14;
	uint catOffset = 9;

	for(uint i=0; i < fileCount; i++)
	{
		if (catOffset + 14 > arc.Size()) {
			return Result(E_BAD_ARCHIVE);
		}

		for(uint j=0; j < 14; j++)
			arc.CalculatedSum += scl[catOffset + j];
		
		SCL_HEADER* h = (SCL_HEADER*)(scl + catOffset);
		catOffset += 14;

		Entry* e;
		if (!(res = arc.CreateEntry(&e))) {
			return res;
		}
		e->Header = h;
		memcpy(&e->NameHeader, h, 8+1+2);
		e->CreateDate = FileCreateDateData(h->Start, h->Length);

		e->IsRealFile = true;
		e->FileN = i;

		e->FileDataPos = fileDataOffset;
		fileDataOffset += h->Sectors * 256;

		e->SizeInSectors = h->Sectors;

		bool IsBasic = (h->Extension == 'B');
		uint autostart = 0;		// for Basic; 0 = no autostart
		uint smartFileSize;		// smart-длина файла; для basic - без учёта 4 байт автостарта
		uint extraDataSize = 0;	// 0, либо 4 - для basic-файла с байтами автостарта
		
		if (!IsBasic)
		{
			// If file size in Length param matches sector size, use Length, otherwise use sectors size
			smartFileSize = (h->Length + 255) / 256u == h->Sectors
				? h->Length
				: h->Sectors * 256;
		}
		else
		{
			// Smart-compute Basic file size.
			// Мы можем взять длину basic-программы из заголовка, если можем доверять данным из него, 
			// а именно:
			// - длина с переменными не меньше длины_без_переменных;
			// - длина_с_переменными+4 совпадает с секторной длиной; UPD: а вот этот пункт лишний! Ведь файл может содержать оверлеи.
			// - по смещению Start есть 4 байта автостарта
			//   (в TRDOS любой basic файл - даже без автостарта - имеет эти байты; значение 0 означает отсутствие автостарта)

			smartFileSize = h->Sectors * 256;

			if (h->Start >= h->Length &&
				//(h->Start + 4 + 255) / 256u == h->Sectors &&
				h->Start + 4u <= h->Sectors * 256u && 
				e->FileDataPos + h->Start + 4u <= arc.Size() &&
				*(ushort*)(scl + e->FileDataPos + h->Start) == 0xAA80)
			{
				autostart = *(ushort*)(scl + e->FileDataPos + h->Start + 2);
				if ((h->Start + 4 + 255) / 256u == h->Sectors)
                {
					smartFileSize = h->Start;
					extraDataSize = 4;
                }
			}
		}

		if (arc.Cfg.SmartLenDetect())
			e->FileDataSize = smartFileSize + extraDataSize;
		else
			e->FileDataSize = h->Sectors * 256;

		// Следующие два if'а взаимоисключающи

		if (arc.Cfg.ExtractHobeta())
		{
			e->FileDataSize = h->Sectors * 256;

			e->HobetaHeaderProvided = true;
			memcpy_(&e->HobetaHeader, h, 8+1+2+2);
			e->HobetaHeader.Size = h->Sectors * 256;
			WriteHobetaHeaderSum(&e->HobetaHeader);
		}

		if (arc.Cfg.InMBDCompatible)
		{
			e->MBD_Mode = true;

			// Сформируем MBD::FileItem для файла типа CODE

			e->MBDFileInfo.ID = 0xB0;
			e->MBDFileInfo.FileDateTime = arc.ArchiveDateTC;
			
			fill_array(e->MBDFileInfo.Header.Name, ' ');
			memcpy_(e->MBDFileInfo.Header.Name, h->Name, 8);
			
			e->MBDFileInfo.Header.Type = (h->Extension == 'C') ? 3 : h->Extension;
			e->MBDFileInfo.Header.Length = h->Length;
			e->MBDFileInfo.Header.Param1 = h->Start;
			e->MBDFileInfo.Header.Param2 = 32768;

			e->MBDFileInfo.BodyAdditionalAddr = 0; // In general, body address value should be the same as ADRESS parameter in header. But it does not matter what value is here, you can put here #0000 :)  (c) Busy
			e->MBDFileInfo.BodyLength = e->FileDataSize;
			e->MBDFileInfo.BodyFlags = 255;
			e->MBDFileInfo.FileAttributes = 0;
			//e->MBDFileInfo.fileStartSector = 0xFFFF;

			// Подкорректируем, если это Basic
			if (IsBasic)
			{
				e->MBDFileInfo.Header.Type = 0;
				e->MBDFileInfo.Header.Length = h->Start;
				e->MBDFileInfo.Header.Param1 = (autostart == 0) ? 0x8000 : autostart;
				e->MBDFileInfo.Header.Param2 = h->Length;

				if (arc.Cfg.SmartLenDetect()) {
					e->FileDataSize = e->MBDFileInfo.BodyLength = smartFileSize;  // cut off 4 bytes of autostart
				}
			}
		}
	}

	return SUCCESS;
};

// Check that file is within image boundaries
bool SCL_CheckFileReadable(ArcHandle* arc, Entry* entry)
{
	// Метод ArcHandle::Open уже проверил, что размер архива не менее 8+1+4 байт...
	uint64 fileEnd = (uint64)entry->FileDataPos + entry->FileDataSize;
	uint arcEnd = arc->Size() - 4; // ...так что переполнения здесь не будет
	return (fileEnd <= arcEnd);
};

Result SCL_UpdateSum(ArcHandle* arc, Entry* entry)
{
	if (entry->IsRealFile)
	{
		uint size = entry->Header->Sectors * 256;
		if ((uint64)entry->FileDataPos + size > arc->Size() - 4) {
			return Result(E_BAD_DATA);
		}

		arc->CurrentPos += 14 + size;

		byte* data = arc->Adr() + entry->FileDataPos;
		while(size-- != 0) {
			arc->CalculatedSum += *data++;
		}
	}

	return SUCCESS;
};

bool SCL_CheckSum(ArcHandle* arc)
{
	// К моменту вызова этого метода были протестированы
	// и просуммированы все Entry.

	if (arc->CurrentPos != arc->Size() - 4)
	{
		// Что-то не так с размером архива (он либо урезан, либо лишние данные в конце).
		return false;
	}

	uint sum = *(uint*)(arc->Adr() + arc->Size() - 4);
	return (sum == arc->CalculatedSum);
};

struct SCL_PackEntry
{
	byte* Data;	// [allocated with myalloc] file content ready for saving. Size is Header.Sectors * 256
	SCL_HEADER Header;

	SCL_PackEntry() {}
};

// Create/update image.
// Adding: false = create new image; true = update existing one;
// newArc: save image to this file (if operation succeeds);
// SrcPath: source dir of packed files;
// AddList: files to pack (paths relative to SrcPath);
Result SCL_PackFiles(bool Adding, ArcHandle& arc, FileHandle& newArc, WCHAR* SrcPath, WCHAR* AddList, CONFIG& cfg)
{
	Result RESULT_FILETOOBIG(E_NOT_SUPPORTED);
	RESULT_FILETOOBIG.SetMessage("Cannot pack files larger than 65280 bytes.", true);

	String<WCHAR> path;
	Result res = SUCCESS;
	List<SCL_PackEntry> newFiles;
	if (!(res = newFiles.Init(2))) {
		goto k9;
	}

	uint oldFileCount = 0;
	uint fileCount = 0;

	if (Adding)
	{
		 fileCount = oldFileCount = arc.Adr()[8];
	}

	WCHAR* nextItem = NULL;
	for(WCHAR* listItem = AddList; *listItem; listItem = nextItem)
	{
		WCHAR* wname = listItem;
		for(nextItem = listItem; *nextItem; nextItem++) {
			if (IsSlash(*nextItem))
				wname = nextItem + 1;
		}
		nextItem++;

		if (*wname == 0) {
			continue; // skip directories
		}

		// path = full path to the file being packed
		res = path.Set(SrcPath);
		res &= path.AppendPath(listItem);
		if (!res) {
			goto k9;
		}

		bool isScl = ends_with_ci(wname, L".scl");
		bool isTrd = !isScl && ends_with_ci(wname, L".trd");
		if (isScl || isTrd)
		{
			// Raw-copy files from SCL or TRD image
			res = RESULT_NO_MEM;
			RawImageReader* sclTrd = CreateRawImageReader(isScl ? 's' : 't');
			if (sclTrd)
			{
				res = sclTrd->OpenAndTest(path.GetPtr());
				if (res)
				{
					while(true) // iterate through files inside image
					{
						const byte* pHeader; // ptr to 14-byte TR-DOS file header
						const byte* pBody;
						uint bodySize;

						// since we are copying from SCL or TRD image, file body already contains autostart bytes
						const uint add_basic_autostart_bytes = 0;

						if (!sclTrd->GetNextFile(&pHeader, &pBody, &bodySize)) {
							// No more files
							res = Result(E_END_ARCHIVE);
							break;
						}

						{
							fileCount++;
							if (fileCount > 255)
							{
								res = Result(E_TOO_MANY_FILES);
								res.SetMessage("SCL archive cannot contain more than 255 files.", true);
								break;
							}

							SCL_PackEntry te;
							if (!(res = newFiles.Add(te))) {
								break;
							}
							SCL_PackEntry& e = newFiles[newFiles.Count - 1];
							clear_struct(e);
							SCL_HEADER& header = e.Header;

							memcpy_(&header, pHeader, 14);

							// пункт 3) заполнить поля в header: Sectors

							if (bodySize + add_basic_autostart_bytes > 0xFF00) {
								res = RESULT_FILETOOBIG;
								break;
							}
							header.Sectors = (bodySize + add_basic_autostart_bytes + 255) / 256u;

							// пункт 4) копируем содержимое файла,
							// добавляем байты автостарта, если надо.

							if (!(e.Data = myalloc<byte>(header.Sectors * 256))) {
								res = RESULT_NO_MEM;
								break;
							}
							memcpy_(e.Data, pBody, bodySize);
							size_t p = bodySize;
							if (add_basic_autostart_bytes) // const false
							{
								//e.Data[p++] = 0x80;
								//e.Data[p++] = 0xAA;
								//e.Data[p++] = autostart;
								//e.Data[p++] = autostart >> 8;
							}
							while(p % 256u != 0) e.Data[p++] = 0;
						}
					} // while

					if (!res && res.WcxCode() == E_END_ARCHIVE) {
						res = SUCCESS;
					}
					sclTrd->Close();
				}
				delete sclTrd;
			}
			if (!res) {
				goto k9;
			}
		}
		else
		{
			// Pack one file whose full path is in path var

			fileCount++;
			if (fileCount > 255)
			{
				res = Result(E_TOO_MANY_FILES);
				res.SetMessage("SCL archive cannot contain more than 255 files.", true);
				goto k9;
			}

			SCL_PackEntry te;
			if (!(res = newFiles.Add(te))) {
				goto k9;
			}
			SCL_PackEntry& e = newFiles[newFiles.Count - 1];
			clear_struct(e);
			SCL_HEADER& header = e.Header;

			FileHandle f;
			res = f.Open(path.GetPtr(), false, OPEN_EXISTING, true, true, 0xFF00+sizeof(HOBETA_HEADER), RESULT_FILETOOBIG);
			if (!res) {
				goto k9;
			}

			byte* data = f.Adr;
			uint size = (uint)f.Size;

			uint add_basic_autostart_bytes = 0; // 0 либо 4
			uint autostart = 0; // 0 = no autostart

			// Convert WCHAR to char
			char name[260];
			if (!Convert_W_to_A(wname, name, sizeof(name)))	{
				res = Result("Invalid file path to pack", true);
				goto k9;
			}

			/*
			 * План действий:
			 * 1) заполнить поля в header: Name, Extension, Start, Length;
			 * 2) если это basic, то определить, надо ли добавлять байты autostart;
			 * 3) заполнить поля в header: Sectors;
			 * 4) скопировать содержимое файла; для basic добавить 4 байта автостарта, если их там ещё нет;
			 */

			// Если имя похоже на сформированное inMBD, надо взять всё из него:
			// имя, Start, Length, autostart.
			// При этом на hobeta не проверяем.

			MBD::FileItem mbd;
			bool is_mbd;
			MBD::NamePC2MBD_FilenameParse(name, strlen_(name), &mbd, &is_mbd);
			if (is_mbd)
			{
				memcpy_(header.Name, mbd.Header.Name, 8); // и TRD_HEADER и TAP HEADER используют паддинг пробелами
		
				// Сформируем для файла типа CODE
				header.Extension = (mbd.Header.Type == 3) ? 'C' : mbd.Header.Type;
				header.Start = mbd.Header.Param1;
				header.Length = mbd.Header.Length;
				if (mbd.Header.Type == 0)
				{
					// Сформируем для файла типа BASIC
					header.Extension = 'B';
					header.Start = mbd.Header.Length;
					header.Length = mbd.Header.Param2;
				
					// Valid values for START parameter of BASIC are values from range 0..#3FFF. #4000 and more means no autostart. When it is needed to creat basic wihtout autostart, the value #8000 can be used (zx rom uses this #8000). (c) Busy
					if (mbd.Header.Param1 < 0x4000) {
						autostart = mbd.Header.Param1;
					}
				
					// Надо ли добавлять байты автостарта?
					// Не забываем, что они должны быть по смещению Start, иначе в них нет смысла.
					// 1) если по смещению Start уже есть байты автостарта, то очевидно, что не надо добавлять.
					// 2) если длина файла не равна Start то не надо.
					//    (иначе придётся писать байты в середину файла, а портить содержимое файла не стоит;
					//     кроме того, в этом случае они там, скоре всего, и так есть)
					// 3) иначе надо.
					if (header.Start == size) {
						add_basic_autostart_bytes = 4;
					}
				}
			}
			else
			{
				bool hasHobetaHeader = false, hasHobetaExtension = false;
				if (cfg.DetectHobeta) {
					if (!(res = CheckHobeta(data, size, &hasHobetaHeader))) {
						goto k9;
					}
				}

				NAME_HEADER& name_header = (NAME_HEADER&)header;
				NamePC2ZX(name, &name_header, cfg, hasHobetaHeader, &hasHobetaExtension); // Name, Ext, ?Start?
		
				if (hasHobetaHeader && hasHobetaExtension)
				{
					memcpy_(&header, data, 8+1+2+2); // Name, Ext, Start, Length
					data += 17; // skip hobeta header
					size -= 17; //
				}
				else
				{
					// С параметром Start поступаем следующим образом:
					// 1) если Start и Length закодированы в дате файла - берём их оттуда;
					// 2) иначе если первый символ расширения - "B", то ставим его равным Length (длина basic-программы);
					//      2b) если файл заканчивается на 80 AA xx xx, то уменьшить Start и Length на 4.
					// 3) иначе отдаём на откуп функции NamePC2ZX, а именно:
					//		a) если опция ExtendedExtension включена, расширение имеет длину >= 2 и символы 1,2 имеют коды от 33(32) до 127, то записываем extended extension;
					//		b) иначе записываем 0.
					//
					// TODO: надо подумать, что делать, если сработает пункт 3 и первый символ - "B" и далее ещё пара символов.
					// Ведь тогда может получиться файл типа Basic с параметром start не похожим на длину basic-программы.

					// сейчас header.Start соответствует пункту 3), а Length=0

					FILETIME dt;
					if (!GetFileTime(f.Handle(), &dt, NULL, NULL)) {
						res = Result(E_EOPEN);
						goto k9;
					}
					FileCreateDateData createDate(dt);

					if (createDate.Has_StartLength)
					{
						header.Start = createDate.Start;
						header.Length = createDate.Length;
					}
					else
					{
						header.Length = size;
						if (name_header.Extension == 'B')
						{
							header.Start = size;
							if (size >= 4 && *(ushort*)(data + size - 4) == 0xAA80) {
								header.Start = header.Length = size - 4;
							}
						}
					}
				}
			}

			// пункт 3) заполнить поля в header: Sectors

			if (size + add_basic_autostart_bytes > 0xFF00) {
				res = RESULT_FILETOOBIG;
				goto k9;
			}
			header.Sectors = (size + add_basic_autostart_bytes + 255) / 256u;

			// пункт 4) копируем содержимое файла,
			// добавляем байты автостарта, если надо.

			if (!(e.Data = myalloc<byte>(header.Sectors * 256))) {
				res = RESULT_NO_MEM;
				goto k9;
			}
			memcpy_(e.Data, data, size);
			size_t p = size;
			if (add_basic_autostart_bytes)
			{
				e.Data[p++] = 0x80;
				e.Data[p++] = 0xAA;
				e.Data[p++] = autostart;
				e.Data[p++] = autostart >> 8;
			}
			while(p % 256u != 0) e.Data[p++] = 0;

			// готово, переходим к следующему файлу

			f.Close();
		}
	}

	// Build new archive

	res = Result(E_EWRITE);
	uint oldDataSize = 0; // will be sum of sizes of old files
	uint oldSum = 0;
	uint newSum = 0;

	const uint SINCLAIR_SUM = 597;
	byte head[9] = "SINCLAIR";
	head[8] = fileCount;

	for(uint i = 0; i < 9; i++) newSum += (byte)head[i];
	if (!newArc.Write(head, 9)) goto k9;

	// Copy old headers

	if (Adding)
	{
		byte* p = arc.Adr();
		for(uint i = 0; i < 9; i++) oldSum += *p++;
		for(uint i = 0; i < oldFileCount; i++)
		{
			SCL_HEADER* h = (SCL_HEADER*)p;
			oldDataSize += h->Sectors * 256;
			for(uint i = 0; i < 14; i++) oldSum += *p++;
		}

		if (!newArc.Write(arc.Adr() + 9, oldFileCount * 14)) goto k9;
	}

	// Add new headers

	for(uint i = 0; i < newFiles.Count; i++)
	{
		SCL_PackEntry e = newFiles[i];
		for(uint i = 0; i < 14; i++) newSum += (byte)e.Header.Name[i];
		if (!newArc.Write(&e.Header, 14)) goto k9;
	}

	// Copy old files

	if (Adding)
	{
		byte* p = arc.Adr() + 9 + oldFileCount * 14;
		for(uint i = 0; i < oldDataSize; i++)
			oldSum += p[i];
		
		if (cfg.CheckCrcBeforeModify && oldSum != *(uint*)&p[oldDataSize])
		{
			// Вообще-то PackFiles уже проверила архив и вывела сообщение,
			// тут лишняя проверка на всякий случай.
			res = Result(E_BAD_DATA); goto k9; 
		}
		
		// нам нужно именно текущее значение CRC, даже если оно неправильное,
		// см. коммент в конце этой процедуры.
		oldSum = *(uint*)&p[oldDataSize];

		if (!newArc.Write(p, oldDataSize)) {
			goto k9;
		}
	}

	// Add new files

	for(uint i = 0; i < newFiles.Count; i++)
	{
		SCL_PackEntry e = newFiles[i];
		uint size = e.Header.Sectors * 256;
		for(uint i = 0; i < size; i++) newSum += e.Data[i];
		if (!newArc.Write(e.Data, size)) goto k9;
	}

	if (Adding) 
	{
		newSum += oldSum - SINCLAIR_SUM - oldFileCount;
		// Если у архива инвалидная CRC (это возможно, если cfg.CheckCrcBeforeModify = false),
		// то она будет изменена, но останется инвалидной - так и задумано.
	}
	
	if (!newArc.Write(&newSum, 4)) {
		goto k9;
	}

	res = SUCCESS;

k9: 

	for(uint i = 0; i != newFiles.Count; i++) {
		myfree(newFiles[i].Data);
	}
	newFiles.Destroy();

	return res;
};

Result SCL_DeleteFiles(ArcHandle& arc, FileHandle& newArc, const WCHAR* DeleteList, CONFIG& cfg)
{
	// Процедура DeleteFiles уже прогнала проверку архива,
	// так что можем расчитывать, что он не сильно испорчен.
	//
	// Учитываем, что в архиве могут быть файлы с одинаковым именем,
	// поэтому смотреть по имени не надо, придётся положиться на то,
	// что файлы в списке DeleteList идут в том порядке, в котором
	// они возвращалсь функцией ReadHeader.
	// UPD: добавлен dup-counter, теперь можно полагаться на имя.
	// Но решено не переделывать: даже если порядок в DeleteList не гарантирован,
	// сильно страшного не произойдёт, просто могут не все выбранные файлы удалиться.

	byte* arcData = arc.Adr();
	uint fileCount = arcData[8];

	const uint MAX_FILES = 255;
	bool delete_[MAX_FILES];
	Entry* realFileEntries[MAX_FILES];
	clear_array(delete_);
	clear_array(realFileEntries);

	const WCHAR* listItem = DeleteList;
	for(uint i = 0; i < arc.Entries.Count; i++)
	{
		Entry* e = arc.Entries[i];
		if (e->IsRealFile) {
			realFileEntries[e->FileN] = e;
		}

		if (*listItem != 0)
		{
			// Get entry name and convert it from char to WCHAR
			char name[260];
			WCHAR nameW[260];
			Result res = GetName(*e, name, cfg, true);
			if (res) {
				res = Convert_A_to_W(name, nameW, ARRAY_LEN(nameW)) 
					? SUCCESS : Result("Error reading archive headers", true);
			}
			if (!res) {
				return res;
			}

			if (streq(nameW, listItem))
			{
				// Удалять разрешаем как real, так и autojoined файлы
				uint n = e->IsJoinedFile ? e->JoinedFileStart : e->FileN;
				uint q = e->IsJoinedFile ? e->JoinedFileCount : 1;
				if (n + q > fileCount) {
					return Result("Internal error DF09", true); // should never happen
				}
				for(uint j = 0; j < q; j++) {
					delete_[n + j] = true;
				}
				while(*listItem++) {} // move to next item in DeleteList
			}
		}
	}

	uint deletedSum = 0;
	uint deletedCount = 0;
	for(uint i = 0; i < ARRAY_LEN(delete_); i++) {
		deletedCount += delete_[i];
	}

	// Write SCL header
	SCL_HEADER* headers = (SCL_HEADER*)(arcData + 9);
	byte head[9] = "SINCLAIR";
	head[8] = fileCount - deletedCount;
	deletedSum += deletedCount;
	if (!(newArc.Write(head, 9))) {
		return Result(E_EWRITE);
	}

	// Write file headers
	for(uint i = 0; i < MAX_FILES; i++)
	{
		Entry* e = realFileEntries[i];
		if (e == NULL) {
			continue;
		}
		if (i > fileCount) {
			return Result("Internal error DF10", true);
		}

		SCL_HEADER& h = headers[i];
		if (delete_[i])
		{
			byte* p = (byte*)&h;
			for(uint j = 0; j < 14; j++) deletedSum += *p++;
		}
		else
		{
			if (!(newArc.Write(&h, 14))) {
				return Result(E_EWRITE);
			}
		}
	}

	// Write files content
	for(uint i = 0; i < arc.Entries.Count; i++)
	{
		Entry* e = realFileEntries[i];
		if (e == NULL) {
			continue;
		}

		SCL_HEADER& h = headers[i];
		uint size = h.Sectors * 256;
		byte* p = arc.Adr() + e->FileDataPos;
		if (delete_[i])
		{
			for(uint j = 0; j < size; j++) deletedSum += *p++;
		}
		else
		{
			if (!(newArc.Write(p, size))) {
				return Result(E_EWRITE);
			}
		}
	}

	// Write SCL checksum
	{
		uint sum = *(uint*)(arcData + arc.Size() - 4);
		sum -= deletedSum;
		if (!newArc.Write(&sum, 4)) {
			return Result(E_EWRITE);
		}
		// Если у архива инвалидная crc (это может быть, если cfg.CheckCrcBeforeModify = false),
		// то она будет изменена, но останется инвалидной - так и задумано.
	}

	return SUCCESS;
};
