﻿
/*
	API for testing, debugging.

	Method 0:
		Get API id. Returns magic value 0x70F7 indicating API is supported and enabled.

	Method 1:
		Get plugin name. param2 must point to char* buffer, param1 must be buffer size. 
		Returns number of bytes written; 0 on failure.

	Method 2:
		Get plugin version. E.g. 0x6430 means 6.43.

	Method 3:
		Returns 1 if method 4 is supported.

	Method 4:
		Returns number of bytes of memory allocated by the plugin.

*/

#define expose_allocated
//#undef expose_allocated

#include "common.h"

#ifdef expose_allocated
	#include "MyStructs\myalloc.h"
#endif

// в версии 0x70F5 функция GetPluginInfo принимала только параметр int method
// в версии 0x70F7 добавлены параметры int param1, void* param2
//void* const ApiID = (void*)0x70F5;
void* const ApiID = (void*)0x70F7;

EXPORTED void* STDCALL GetPluginInfo (int method, int param1, void* param2)
{
	if (method == 0) {
		return ApiID;
	}
	if (method == 1) {
		if (param1 >= 6 && param2 != NULL) {
			strcpy_((char*)param2, "inSCL");
			return (void*)6;
		} else {
			return 0;
		}
	}
	if (method == 2) {
		return (void*)0x6520; // 6.52
	}

#ifdef expose_allocated
	if (method == 3) {
		return (void*)1;
	}
	if (method == 4) {
		return (void*)_allocated_; // можно использовать для контроля утечек памяти
	}
#else
	if (method == 3) {
		return 0;
	}
#endif

	return 0;
};

