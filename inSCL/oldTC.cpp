﻿
/*
// We don't support TC versions before 7.50.
// Older versions call ANSI functions instead of Unicode ones, and here we catch it.
*/

#include "common.h"
#include "wcxhead.h"
#include "MyStructs\Result.h"

void ReportInvalidTCVersion()
{
	Result::ShowErrorMessage(NULL, "Total Commander version 7.50 or later is REQUIRED");
	//MessageBoxA(NULL, "Total Commander version 7.50 or later is REQUIRED", ERROR_MSGBOX_CAPTION, MB_OK | MB_ICONERROR | MB_APPLMODAL);
}

EXPORTED HANDLE STDCALL OpenArchive (tOpenArchiveData *ArchiveData)
{
	ReportInvalidTCVersion();
	ArchiveData->OpenResult = E_NOT_SUPPORTED;
	return NULL;
}

EXPORTED int STDCALL ReadHeader (HANDLE hArcData, tHeaderData *HeaderData)
{
	return E_NOT_SUPPORTED;
}

EXPORTED int STDCALL ProcessFile (HANDLE hArcData, int Operation, char *DestPath, char *DestName)
{
	return E_NOT_SUPPORTED;
}

EXPORTED int STDCALL PackFiles (char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags)
{
	ReportInvalidTCVersion();
	return E_NOT_SUPPORTED;
}

EXPORTED int STDCALL DeleteFiles (char *PackedFile, char *DeleteList)
{
	ReportInvalidTCVersion();
	return E_NOT_SUPPORTED;
}

EXPORTED void STDCALL SetChangeVolProc (HANDLE hArcData, ::tChangeVolProc pChangeVolProc1)
{}

EXPORTED void STDCALL SetProcessDataProc (HANDLE hArcData, ::tProcessDataProc pProcessDataProc)
{}
