﻿
#pragma once

#include "..\common.h"
#include "..\MyStructs\serv.h"
#include "..\MyStructs\MyIntrinsics.h"

namespace MBD
{
	enum DIR_STRUCT_VIEW_MODE
	{
		DSVM_LOW = 1, 
		SIMPLE = 1,
		TREE = 2, 
		DSVM_HIGH = 2
	};

	enum FILENAME_GAP
	{
		GAP_LOW = 1,
		GAP_FIXED = 1,
		GAP_VARWIDTH_FONT = 2,
		GAP_FIXEDWIDTH_FONT = 3,
		GAP_HIGH = 3
	};

	// зачем так много? потому что кто-то может захотеть навставлять пробелов в паттерн с целью паддинга
	#define PARAMS_PATTERN_SIZE 200

	struct FILE_NAMING_MODE
	{
		static const byte STRUCT_VER = 6;	// относительно 5-й версии введено поле ParamsPattern, поле bool VariableGap заменено на FILENAME_GAP Gap

		byte StructVer;

		bool Decimal;
		FILENAME_GAP Gap;
		char ParamsPattern[PARAMS_PATTERN_SIZE]; // filled in settings.cpp/ParseParamsPattern

		bool ParamsPatternValid()
		{
			return ParamsPattern[0] != 0;
		}

		bool RestartNeeded(FILE_NAMING_MODE& other)
		{
			bool eq = (Decimal == other.Decimal && 
				       Gap == other.Gap && 
					   streq(ParamsPattern, other.ParamsPattern));
			return !eq;
		}
	};

};