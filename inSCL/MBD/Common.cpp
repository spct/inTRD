﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

/*
Helpers for inMBD compatibility functions
*/

#include "Common.h"
#include "../helpers.h" // ParseHexByte

namespace MBD
{
	// offsets of interesting fields in MBD::FileItem struct
	const ParamOffset ParamOffsets[6] = {{16,2},{18,2},{20,2},{22,2},{28,1},{29,1}};

	// Convert BS-DOS file name to windows. Result is appended to "result".
	Result NameMBD2PC_Simple(String<char>& result, const byte* name, size_t nameLen/*bytes*/, bool isDir)
	{
		Result res = SUCCESS;
		uint start = result.Length;

		// remove spaces padding
		if (isDir) {
			while(nameLen > 0 && name[nameLen - 1] <= 32) nameLen--;
		} else {
			while(nameLen > 0 && name[nameLen - 1] == ' ') nameLen--;
		}

		for(size_t i = 0; i < nameLen; i++) {
			if (memchr_(InvalidFileNameChars, name[i], sizeof(InvalidFileNameChars)) 
				|| name[i] >= 127 
				|| name[i] == '#'
			   )
				res &= result.Append('#', HexChars_Up[name[i] / 16u], HexChars_Up[name[i] & 15]);
			else
				res &= result.Append(name[i]);
		}
		
		if (!res) {
			return res;
		}

		// In Windows, dir name may not end with points, so let's escape them
		if (isDir)
		{
			uint k = 0; // number of points in the end of name
			for (uint i = result.Length; i > start && result[i-1] == '.'; i--) {
				result.RemoveLastChar();	// тут ошибок случаться не должно, поэтому не проверяем
				k++;
			}

			while(k-- != 0)	{
				res &= result.Append('#', '2', 'E');
			}
		}

		if (!res) {
			return res;
		}
		
		// File names like "con" or "PRN" are forbidden in Windows
		const char* s = &result.GetPtr()[start];
		if (streq_ci(s, "prn") || streq_ci(s, "con"))
		{
			for(uint i = 0; i < 3; i++) {
				byte c = (byte)result[start + i];
				res &= result.Append('#', HexChars_Up[c / 16u], HexChars_Up[c & 15]);
			}
			if (res) {
				res = result.Remove(start, 3);
			}
		}

		return res;
	};

	// Build Windows file name of bs-dos file
	Result NameMBD2PC_File(String<char>& result, const FileItem* fileitem, FILE_NAMING_MODE& fmode)
	{
		uint nameLen = result.Length;
		Result res = NameMBD2PC_Simple(result, fileitem->Header.Name, ARRAY_LEN(fileitem->Header.Name), false);
		if (!res) {
			return res;
		}

		if (!fileitem->IsValidFile())
		{
			if (!(res = result.Append(" [deleted]")))
				return res;
		}

		res &= result.Append('.');
		res &= result.AppendUint(fileitem->Header.Type);
		if (!res) {
			return res;
		}
		nameLen = result.Length - nameLen;

		ssize_t gap = 0;
		while(fmode.ParamsPattern[gap] == ' ') {
			gap++;
		}

		//switch(fmode.Gap)
		//{
		//	...
		//}

		// один пробел обязателен и он не учитывается ни в поле fmode.ParamsPattern,
		// ни в расчётах выше.
		gap += 1;
		gap = max(gap, 1);	// страховка. Должен быть как минимум 1 пробел
		for(ssize_t i=0; i < gap; i++) {
			res &= result.Append(' ');
		}

		if (res) {
			res = NameMBD2PC_AppendParams(result, fileitem, fmode);
		}

		return res;
	}

	// Appends params to file extension.
	// Doesn't add point. Skips gap spaces!
	Result NameMBD2PC_AppendParams(String<char>& result, const FileItem* fileitem, FILE_NAMING_MODE& fmode)
	{
		// skip gap
		byte* pat = (byte*)fmode.ParamsPattern;
		while(*pat == ' ') pat++;

		Result res = SUCCESS;
		for(; *pat; pat++)
		{
			if (*pat > ARRAY_LEN(ParamOffsets))
			{
				res &= result.Append(*pat);
			}
			else
			{
				const ParamOffset& po = ParamOffsets[*pat - 1];
				uint value = po.GetParamValue(fileitem);
				if (fmode.Decimal)
					res &= result.AppendUint0(value, po.size * 2 + 1);
				else
					res &= result.AppendUint0_Hex(value, po.size * 2, HexChars_Up);
			}
		}

		return res;
	};

	// Convert Windows file name to Bs-dos file name
	void NamePC2MBD_Simple(
		const char* name, size_t nameLen, 
		byte* dest, size_t destSize/*in chars*/)
	{
		size_t x = 0, d = 0;
		while (x < nameLen && d < destSize)
		{
			char c = name[x++];
			if (c == 0) {
				_throw("Error 932");	// Zero in pc file path
			}
			if (c == '#' && x+1 < nameLen && ParseHexByte<char, char>(name[x+0], name[x+1], &c)) {
				x += 2;
			}
			else if (!InRange(c, 32, 126)) {
				c = '?';
			}
			dest[d++] = c;
		}
		while (d < destSize) {  // spaces padding
			dest[d++] = ' ';
		}
	};


	// Parse name (using NamePC2MBD_Simple) and params (including type).
	// If params and type are ok, sets FileItem fields, otherwise doesn't change them.
	void NamePC2MBD_FilenameParse(
		const char* name, size_t nameLen, 
		FileItem* fileItem, bool* out_params_found)
	{
		/*
		   Если расширение файла представляет собой корректный паттерн, то тогда
		   проставляем Type и прочие параметры, даже если имя файла (до расширения) содержит 
		   инвалидные символы (т.е. имя файла было сформировано не плагином).
		*/

		if (out_params_found) {
			*out_params_found = false;
		}
		FileItem paramValues = *fileItem;

		// Remove duplicates number [N]

		size_t i = nameLen;
		if (nameLen != 0 && name[--i] == ']')
		{
			size_t len = 0;
			while(i > 0 && (byte)(name[--i] - '0') < 10) {
				len++;
			}
			if (len > 0 && name[i] == '[') {
				nameLen = i;
			}
		}

		// Find last point

		size_t point = nameLen;
		while(point != 0 && name[point - 1] != '.') point--;
		// point = point position + 1

		if (point == 0) { // no point?
			goto invalid_ext_pattern;
		}
		
		// Parse <type>

		uint type = 0;
		size_t cnt = 0;
		for(i = point; i < nameLen; i++)
		{
			uint t = name[i] - '0';
			if (t >= 10) {
				break;
			}
			cnt++;
			type = type * 10 + t;
			if (type > 255) {
				break;
			}
		}

		if (cnt == 0 /*no type*/ || type > 255 /*invalid type value*/) {
			goto invalid_ext_pattern;
		}

		paramValues.Header.Type = type;

		// Parse <gap>

		cnt = 0;
		for(; i < nameLen; i++) {
			if (name[i] != ' ') {
				break;
			}
			cnt++;
		}

		if (cnt == 0) { // gap must be at least one space
			goto invalid_ext_pattern;
		}

		// Parse <params>.  Start pos = i; end pos = nameLen

		for(uint hex = 0; ; hex++) // first try decimal, then try hex
		{
			if (hex == 2) {
				goto invalid_ext_pattern;
			}

			size_t lastParamPos = nameLen; // prevent params overlapping (e.g., treating 1234 as 123 and 234)
			size_t p = ARRAY_LEN(ParamOffsets);
			for(size_t pos = nameLen - 1; pos >= i; pos--)
			{
				bool isParam = false;
				if (p != 0)
				{
					const ParamOffset& po = ParamOffsets[p-1];
					uint len = po.size * 2 + (1 - hex);
					uint x = 0;
					for(uint j = 0; j < len; j++)
					{
						if (pos + j >= lastParamPos) {
							goto not_param;
						}
						byte c = (byte)(name[pos + j] - '0');
						if (c >= 10) {
							if (!hex || (c += '0' - 'A' + 10) >= 16) {
								goto not_param;
							}
						}
						x = x * (hex ? 16 : 10) + c;
					}

					if (!po.CheckRange(x)) {
						goto not_param;
					}
					
					po.Set(&paramValues, x);
					lastParamPos = pos;
					p--;
					isParam = true;
				not_param: ;
				}

				if (!isParam)
				{
					if (!InRange(name[pos], 32, 126))
						goto invalid_ext_pattern;
				}
			}

			if (p == 0) {
				break;	// all parameters parsed
			}
		}

		// ok, assign type and params

		if (out_params_found) {
			*out_params_found = true;
		}
		*fileItem = paramValues;
		nameLen = point - 1;

	invalid_ext_pattern:
		
		// finally convert name

		NamePC2MBD_Simple(name, nameLen, fileItem->Header.Name, ARRAY_LEN(fileItem->Header.Name));
	};

};
