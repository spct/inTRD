﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

#include "common.h"
#include "RawImageReader.h"
#include "Format_SCL.h"
#include "Format_TRD.h"
#include "MyStructs\String.h"
#include "MyStructs\MainWindow.h"

void RawImageReader::ShowErrorMsg(const WCHAR* part1, const WCHAR* part2)
{
	size_t l1 = strlen_(part1);
	size_t l2 = strlen_(part2);
	String<WCHAR> s;
	Result res = s.Append(part1);
	res &= s.Append(part2);
	if (res) {
		MainProcessWindow hwndTC;
		Result::ShowErrorMessage(hwndTC.Get(), s.GetPtr());
	}
	s.Destroy();
}

class RawSclReader : public RawImageReader
{
private:
	uint fileCount;
	uint currentFile;
	size_t headerPos;
	size_t dataPos;
	FileHandle fImage;

public:
	RawSclReader()
	{
		clear_struct(fImage);
	}

	~RawSclReader()
	{
		Close();
	}

	Result OpenAndTest(const WCHAR* path)
	{
		if (!fImage.Open(path, false, OPEN_EXISTING, false, true)) {
			ShowErrorMsg(L"Error reading file:\r\n", path);
			return Result(E_EOPEN);
		}
		
		// Main header checks

		if (!InRange(fImage.Size, 8+1+4, 8+1+4 + (14 + 255 * 256) * 255)
			|| memcmp_(fImage.Adr, "SINCLAIR", 8) != 0)
		{
			fImage.Close();
			ShowErrorMsg(L"This SCL image is corrupt:\r\n", path);
			return Result(E_UNKNOWN_FORMAT);
		}
		
		const byte* scl = fImage.Adr;
		const uint sclSize = (uint)fImage.Size;
		const uint ChecksumSize = 4; // last 4 bytes are SCL image checksum

		uint checksum = 0;
		headerPos = 0;
		while(headerPos < 9) {
			checksum += scl[headerPos++];
		}

		fileCount = scl[8];
		dataPos = 9 + fileCount * sizeof(SCL_HEADER);

		// Test image

		for(uint i = 0; i < fileCount; i++)
		{
			if (headerPos + sizeof(SCL_HEADER) > sclSize - ChecksumSize) {
				fImage.Close();
				ShowErrorMsg(L"This SCL image is corrupt:\r\n", path);
				return Result(E_BAD_ARCHIVE);
			}
			SCL_HEADER* h = (SCL_HEADER*)(scl + headerPos);
			uint fileSize = h->Sectors * 256;
			if (dataPos + fileSize > sclSize - ChecksumSize) {
				fImage.Close();
				ShowErrorMsg(L"This SCL image is corrupt:\r\n", path);
				return Result(E_BAD_ARCHIVE);
			}

			// update checksum and move to next file inside image

			for(uint j=0; j < sizeof(SCL_HEADER); j++) {
				checksum += scl[headerPos++];
			}
			for(uint j=0; j < fileSize; j++) {
				checksum += scl[dataPos++];
			}
		}

		if (dataPos != sclSize - ChecksumSize) {
			// extra data in the end of image?
			fImage.Close();
			ShowErrorMsg(L"This SCL image is corrupt:\r\n", path);
			return Result(E_BAD_ARCHIVE);
		}

		if (checksum != *(uint*)(scl + dataPos)) {
			fImage.Close();
			ShowErrorMsg(L"This SCL image is corrupt (wrong checksum):\r\n", path);
			return Result(E_BAD_DATA); // invalid image checksum
		}

		currentFile = 0;
		headerPos = 9;
		dataPos = 9 + fileCount * sizeof(SCL_HEADER);

		return SUCCESS;
	}

	bool GetNextFile(const byte** out_headerPtr, const byte** out_bodyPtr, uint* out_bodySize)
	{
		if (currentFile >= fileCount) {
			return false;
		}

		*out_headerPtr = fImage.Adr + headerPos;
		SCL_HEADER* h = (SCL_HEADER*)(fImage.Adr + headerPos);
		*out_bodySize = h->Sectors * 256;
		*out_bodyPtr = fImage.Adr + dataPos;

		headerPos += sizeof(SCL_HEADER);
		dataPos += h->Sectors * 256;
		currentFile++;
		return true;
	}
	
	Result Close()
	{
		return fImage.Close();
	}
};

class RawTrdReader : public RawImageReader
{
private:
	uint fileCount;
	uint currentFile;
	FileHandle fImage;

public:
	RawTrdReader()
	{
		clear_struct(fImage);
	}

	~RawTrdReader()
	{
		Close();
	}

	Result OpenAndTest(const WCHAR* path)
	{
		if (!fImage.Open(path, false, OPEN_EXISTING, false, true)) {
			ShowErrorMsg(L"Error reading file:\r\n", path);
			return Result(E_EOPEN);
		}
		
		// Image must have sys sector; image can't be larger than 256 tracks
		TRD_SystemSector* syssec = (TRD_SystemSector*)(fImage.Adr + 0x800);
		if (!InRange(fImage.Size, 0x900, 256 * 4096)
			|| syssec->Mark_10 != 0x10)
		{
			fImage.Close();
			ShowErrorMsg(L"This TRD image is corrupt:\r\n", path);
			return Result(E_UNKNOWN_FORMAT);
		}

		const byte* trd = fImage.Adr;
		const uint trdSize = (uint)fImage.Size;

		const TRD_HEADER* headers = (TRD_HEADER*)trd;
		fileCount = 128;
		while(fileCount > 0 && headers[fileCount - 1].Name[0] <= 1) {
			fileCount--;
		}

		// Check all non-deleted files are well within image boundaries
		for(uint i = 0; i < fileCount; i++)
		{
			bool isDeleted = (headers[i].Name[0] <= 1);
			if (!isDeleted)
			{
				uint fileSize = headers[i].Sectors * 256;
				uint filePos = headers[i].GetStartSector() * 256;
				if (filePos + fileSize > trdSize) {
					fImage.Close();
					ShowErrorMsg(L"This TRD image is corrupt:\r\n", path);
					return Result(E_BAD_ARCHIVE);
				}
			}
		}

		currentFile = 0;

		return SUCCESS;
	}

	bool GetNextFile(const byte** out_headerPtr, const byte** out_bodyPtr, uint* out_bodySize)
	{
		// Find next non-deleted file
		while (currentFile < fileCount)
		{
			const TRD_HEADER* h = (TRD_HEADER*)(fImage.Adr + currentFile * sizeof(TRD_HEADER));
			bool isDeleted = (h->Name[0] <= 1);
			if (isDeleted)
			{
				currentFile++;
			}
			else
			{
				*out_headerPtr = (byte*)h;
				*out_bodySize = h->Sectors * 256;
				*out_bodyPtr = fImage.Adr + h->GetStartSector() * 256;
				currentFile++;
				return true;
			}
		}
		return false; // no more files
	}
	
	Result Close()
	{
		return fImage.Close();
	}
};

RawImageReader* CreateRawImageReader(char type)
{
	switch(type)
	{
	case 's': return new RawSclReader();
	case 't': return new RawTrdReader();
	default: _throw("RR01");
	}
}
