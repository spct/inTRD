﻿
#pragma once

#include "common.h"
#include "MyStructs\Result.h"
#include "ArcHandle.h"

#pragma pack(push, 1)
struct SCL_HEADER
{
	byte Name[8];
	byte Extension;
	WORD Start;				// For CODE: load address. For BASIC: program+vars length.
	WORD Length;			// For CODE: code length. For BASIC: program length without vars.
	byte Sectors;
};
#pragma pack(pop)

extern Result SCL_FillEntries(ArcHandle& arc);
extern bool SCL_CheckFileReadable(ArcHandle* arc, Entry* entry);
extern Result SCL_PackFiles(bool Adding, ArcHandle& arc, FileHandle& newArc, WCHAR* SrcPath, WCHAR* AddList, CONFIG& cfg);
extern Result SCL_DeleteFiles(ArcHandle& arc, FileHandle& newArc, const WCHAR* DeleteList, CONFIG& cfg);

extern Result SCL_UpdateSum(ArcHandle* arc, Entry* entry);
extern bool SCL_CheckSum(ArcHandle* arc);

