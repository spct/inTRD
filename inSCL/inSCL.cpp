﻿/*
 * Copyright © 2003-2025 Eugene Larchenko <spct@mail.ru>. All rights reserved.
 * See the attached LICENSE file.
 */

#include "common.h"
#include "wcxhead.h"
#include "ArcHandle.h"
#include "MyStructs\Result.h"
#include "MyStructs\Random.h"
#include "MyStructs\EncodingHelpers.h"
#include "settings.h"
#include "Format.h"
#include "Format_SCL.h"

HINSTANCE dllInstance;


// Поддержка дупликатов имён обязательна, т.к. они встречаются нередко,
// особенно в режиме inMBD-compatibility. Например на диске IG#7_2.TRD
// будет два файла "200D.3   FF00 787A 8000 0000 FF 00"
// и еще куча других.

// Подсчитывает кол-во нодов (среди нодов от 0 до nodeN-1) для которых
// GetName возвращает то же, что и для nodes[nodeN].
Result CalcDuplicates(List<Entry*>& nodes, uint nodeN, uint* result, CONFIG& cfg)
{
	uint count = 0;
	Entry* n1 = nodes[nodeN];
	char name1[260];
	Result res = GetName(*n1, name1, cfg, false);
	if (!res) {
		return res;
	}
	
	char name2[260];
	for(uint i = 0; i < nodeN; i++)
	{
		Entry* n2 = nodes[i];
		res = GetName(*n2, name2, cfg, false);
		if (!res) {
			return res;
		}

		if (streq_ci(name1, name2)) {
			count++;
		}
	}
	*result = count;
	return SUCCESS;
}

Result CalcDuplicates(List<Entry*>& nodes, CONFIG& cfg)
{
	for(uint i=0; i < nodes.Count; i++) {
		Result res = CalcDuplicates(nodes, i, &nodes[i]->DuplicateN, cfg);
		if (!res) {
			return res;
		}
	}
	return SUCCESS;
}


EXPORTED HANDLE STDCALL OpenArchiveW (tOpenArchiveDataW *ArchiveData)
{
	Random_Feed(ArchiveData, sizeof(*ArchiveData));

	Result res = RESULT_NO_MEM;
	ArcHandle* arc = new ArcHandle();
	if (!arc) {
		goto oa_error;
	}
	arc->Cfg = GetPrimaryConfig();
	arc->OpenMode = ArchiveData->OpenMode;

	if (strlen_(ArchiveData->ArcName) + 1 > ARRAY_LEN(arc->ArcName))
	{
		res = Result("Archive path too long", true);
		goto oa_error;
	}
	strcpy_(arc->ArcName, ArchiveData->ArcName);

	res = arc->Open(ArchiveData->ArcName, false, OPEN_EXISTING);
	if (!res) {
		goto oa_error;
	}

	res = SCL_FillEntries(*arc);
	if (!res) {
		goto oa_error;
	}

	if (arc->Cfg.AutoJoinLargeFiles())
	{
		// Add fictitious entries for big (compound) files
		if (!(res = AutojoinFiles(*arc)))
			goto oa_error;
	}

	// Precalc [n] suffixes for files with same names
	res = CalcDuplicates(arc->Entries, arc->Cfg);
	if (!res) {
		goto oa_error;
	}

	arc->CurrentEntry = 0;
	arc->CurrentPos = 8 + 1;

	ArchiveData->OpenResult = 0;
	return arc;
	
oa_error:

	delete arc;	// Close вызовется деструктором

	res.ShowMessageIfNeed(NULL);
	ArchiveData->OpenResult = res.WcxErrorCode();
	return NULL;
};

EXPORTED int STDCALL CloseArchive (HANDLE hArcData)
{
	ArcHandle* arc = (ArcHandle*)hArcData;
	Result res = arc->Close();
	delete arc;
	//res.ShowMessageIfNeed(NULL);	// no sense in displaying error message here
	return res.WcxErrorCode();
};

EXPORTED int STDCALL ReadHeaderExW (HANDLE hArcData, tHeaderDataExW *HeaderData)
{
	ArcHandle* arc = (ArcHandle*)hArcData;
	if (arc->CurrentEntry >= arc->Entries.Count) {
		return E_END_ARCHIVE;
	}
	Entry* e = arc->Entries[arc->CurrentEntry];

	clear_struct(*HeaderData);
	strcpy_(HeaderData->ArcName, arc->ArcName);
	HeaderData->FileTime = arc->ArchiveDateTC;
	HeaderData->PackSize = arc->Cfg.ShowSizeInSectors ? e->SizeInSectors : e->SizeInSectors * 256;
	HeaderData->UnpSize = arc->Cfg.ShowSizeInSectors ? e->SizeInSectors : e->Size();

	char name[260];
	Result res = GetName(*e, name, arc->Cfg, true);
	if (res)
	{
		res = Convert_A_to_W(name, HeaderData->FileName, ARRAY_LEN(HeaderData->FileName))
			? SUCCESS
			: Result("Error reading archive headers", true);
	}
	
	if (!res)
	{
		res.ShowMessageIfNeed(NULL);
		return res.WcxErrorCode();
	}

	return 0;
};

Result processFile(HANDLE hArcData, int Operation, WCHAR *DestPath, WCHAR *DestName, bool errorOnBadCrcInTestMode)
{
	Result res = SUCCESS;

	ArcHandle& arc = *(ArcHandle*)hArcData;
	Entry& e = *arc.Entries[arc.CurrentEntry];

	if (Operation == PK_TEST)
		arc.IsTestMode = true;

	if (arc.OpenMode == PK_OM_EXTRACT)
	{
		// Нет смысла проверять сумму в режиме распаковки, т.к. TC может
		// прервать цикл сразу как только распакован нужный файл, и тогда
		// всё-равно проверка не будет выполнена до конца.
		if (arc.IsTestMode)
		{
			// Подсчёт и проверку КС надо делать для всех файлов подряд, 
			// не только для тестируемых/распаковываемых.

			if (!(res = SCL_UpdateSum(&arc, &e))) {
				goto k9;
			}

			if (arc.CurrentEntry + 1 == arc.Entries.Count)
			{
				if (errorOnBadCrcInTestMode && !SCL_CheckSum(&arc))
				{
					// TC выдаёт невразумительное "Error reading files."
					res = Result(E_BAD_DATA);
					res.SetMessage("Archive checksum is wrong.\nProbably some files in SCL archive are corrupt.", true);
					goto k9; 
				}
			}
		}
	}
	else // PK_OM_LIST
	{
		if (Operation != PK_SKIP) {
			res = Result("Invalid operation (code PF6)", true);
			goto k9;
		}
	}

	if (Operation != PK_SKIP)
	{
		if (DestPath != NULL) // TC never uses this param
		{
			res = Result(E_NOT_SUPPORTED);
			res.SetMessage("DestPath is not null.\nPlease report this error to plugin author.", true);
			goto k9;
		}
		
		// Test file
		bool readable = SCL_CheckFileReadable(&arc, &e);
		if (!readable) {
			res = Result(E_BAD_DATA); goto k9;
		}

		if (Operation == PK_EXTRACT)
		{
			FILETIME createDate = {0,0};
			if (e.CreateDate.IsValid()) {
				createDate = e.CreateDate.MakeFiletime();
			}

			FileHandle out;
			res = out.Open_DeleteOnClose(DestName, true, CREATE_NEW, true);
			if (e.HobetaHeaderProvided)
			{
				if (res) res = out.Write(&e.HobetaHeader, 17) ? SUCCESS : Result(E_EWRITE);
			}
			if (res) res = out.Write(arc.Adr() + e.FileDataPos, e.FileDataSize) ? SUCCESS : Result(E_EWRITE);
			if (res) res = SetFileModificationAndCreationDate(out.Handle(), arc.ArchiveDateUtc, createDate) ? SUCCESS : Result(E_EWRITE);
			if (!out.Close(res.IsSuccess())) res &= Result(E_EWRITE);
			if (!res) {
				goto k9;
			}
		}
	}

	res = SUCCESS;

k9:
	arc.CurrentEntry++;
	return res;
}

EXPORTED int STDCALL ProcessFileW (HANDLE hArcData, int Operation, WCHAR *DestPath, WCHAR *DestName)
{
	Result res = processFile(hArcData, Operation, DestPath, DestName, true);
	res.ShowMessageIfNeed(NULL);
	return res.WcxErrorCode();
}

EXPORTED int STDCALL PackFilesW (const WCHAR *PackedFile, const WCHAR *SubPath_notused, WCHAR *SrcPath, WCHAR *AddList, int Flags)
{
	Result res = SUCCESS;
	ArcHandle arc;
	arc.Cfg = GetPrimaryConfig();
	String<WCHAR> tempPath;
	FileHandle tempArc;
	bool deleteTempArc = false;

	if (!(res = arc.Open(PackedFile, true, OPEN_ALWAYS))) {
		goto pf_error;
	}
	bool Adding = !arc.File.Created; // true = modify existing archive; false = create new archive

	if (Adding)
	{
		// Adding to existing archive.
		// Have to test it first, because if it's not ok, we corrupt it even more.
		// Simulating "Test archive" TC command here.

		res = SCL_FillEntries(arc);
		arc.CurrentPos = 8+1;

		// Autojoin здесь не делаем, тестировать фиктивные файлы ни к чему.
		
		if (res)
		{
			res = CalcDuplicates(arc.Entries, arc.Cfg);
		}
		
		if (res)
		{
			arc.OpenMode = PK_OM_EXTRACT;
			tHeaderDataExW headerData;
			int t;
			while (0 == (t = ReadHeaderExW(&arc, &headerData))) {
				if (0 != (t = processFile(&arc, PK_TEST, NULL, NULL, arc.Cfg.CheckCrcBeforeModify).WcxErrorCode()))
					break;
			}
			if (t != E_END_ARCHIVE) {
				res = FAIL;
			}
		}
		
		if (arc.ProtectionDetected) {
			res = FAIL;
		}

		if (!res)
		{
			res = Result(E_BAD_ARCHIVE);
			res.SetMessage("SCL archive is corrupted or protected and cannot be modified.", true);
			goto pf_error;
		}
	}

	if (Adding)
	{
		// To be on a safe side, we write modified version of image to a temp file.
		// After operation succeeds, replace original image with temp file.
		res = CreateTempFile<WCHAR>(PackedFile, tempPath, tempArc);
		if (!res) {
			goto pf_error;
		}
		deleteTempArc = true;
	
		// Set creation date. We want image file creation date unchanged.
		SetFileTime(tempArc.Handle(), &arc.ArchiveCreateDateUtc, NULL, NULL);
	}

	FileHandle& newArc = Adding ? tempArc : arc.File;
	res = SCL_PackFiles(Adding, arc, newArc, SrcPath, AddList, arc.Cfg);
	if (!res) {
		goto pf_error;
	}

	if (Adding) {
		if (!(res = tempArc.Close()))
			goto pf_error;
	}

	if (!(res = arc.Close())) {
		goto pf_error;
	}

	if (Adding)
	{
		// Replace original image with temp file
		if (!MoveFileEx(tempPath.GetPtr(), PackedFile, MOVEFILE_REPLACE_EXISTING)) {
			res = Result(E_EWRITE);
			goto pf_error;
		}
		deleteTempArc = false;
	}

	res = SUCCESS;


pf_error:

	tempArc.Close(); // it's ok to Close() twice
	if (deleteTempArc) {
		DeleteFile(tempPath.GetPtr());
	}

	bool fileCreated = arc.File.Created;
	bool close_ok = arc.Close();
	if (!res && fileCreated) {
		// Creating new image has failed. Delete incomplete image.
		DeleteFile(PackedFile);
	}
	if (res && !close_ok) {
		res = Result(E_ECLOSE);
	}

	if (res && (Flags & PK_PACK_MOVE_FILES))
	{
		// User has picked "Move to archive" option
		DeletePackedFiles<true>(AddList, SrcPath);
	}

	// Обновлять дату архива с помощью UpdateFileWriteTime2 здесь не нужно, т.к. мы 
	// по сути создали новый архив и TC и так обновит панель. Да и размер его изменился.

	res.ShowMessageIfNeed(NULL);
	return res.WcxErrorCode();
};

EXPORTED int STDCALL DeleteFilesW (WCHAR *PackedFile, WCHAR *DeleteList)
{
	Result res = SUCCESS;
	ArcHandle arc;
	arc.Cfg = GetPrimaryConfig();
	String<WCHAR> tempPath;
	FileHandle tempArc;
	bool deleteTempArc = false;

	if (!(res = arc.Open(PackedFile, true, OPEN_EXISTING))) {
		goto df_error;
	}

	// Populate arc->Entries

	arc.CurrentPos = 8+1;
	res = SCL_FillEntries(arc);
	if (!res) {
		goto df_error;
	}

	if (arc.Cfg.AutoJoinLargeFiles()) {
		if (!(res = AutojoinFiles(arc)))
			goto df_error;
	}

	res = CalcDuplicates(arc.Entries, arc.Cfg);
	if (!res) {
		goto df_error;
	}

	// Have to test it first, because if it's not ok, we corrupt it even more.
	// Simulating "Test archive" TC command here.
	{
		arc.OpenMode = PK_OM_EXTRACT;
		tHeaderDataExW headerData;
		int t;
		while (0 == (t = ReadHeaderExW(&arc, &headerData))) {
			if (0 != (t = processFile(&arc, PK_TEST, NULL, NULL, arc.Cfg.CheckCrcBeforeModify).WcxErrorCode()))
				break;
		}
		if (t != E_END_ARCHIVE) {
			res = FAIL;
		}
		
		if (arc.ProtectionDetected) {
			res = FAIL;
		}

		if (!res)
		{
			res = Result(E_BAD_ARCHIVE);
			res.SetMessage("SCL archive is corrupted or protected and cannot be modified.", true);
			goto df_error;
		}
	}

	// To be on a safe side, we write modified version of image to a temp file.
	// After operation succeeds, replace original image with temp file.
	{
		res = CreateTempFile<WCHAR>(PackedFile, tempPath, tempArc);
		if (!res) {
			goto df_error;
		}
		deleteTempArc = true;
		SetFileTime(tempArc.Handle(), &arc.ArchiveCreateDateUtc, NULL, NULL);
		// Это нужно чтобы при изменении архива не изменилась его дата создания.
		// А возможные ошибки я тут игнорю намеренно.
	}

	res = SCL_DeleteFiles(arc, tempArc, DeleteList, arc.Cfg);
	if (!res) {
		goto df_error;
	}

	if (!(res = tempArc.Close())) goto df_error;
	if (!(res = arc.Close())) goto df_error;

	// Replace original image with temp file
	if (!MoveFileEx(tempPath.GetPtr(), PackedFile, MOVEFILE_REPLACE_EXISTING)) {
		res = Result(E_EWRITE);
		goto df_error;
	}
	deleteTempArc = false;

	res = SUCCESS;

df_error:

	tempArc.Close();
	if (deleteTempArc) {
		DeleteFile(tempPath.GetPtr());
	}
	arc.Close();

	res.ShowMessageIfNeed(NULL);
	return res.WcxErrorCode();
}

EXPORTED int STDCALL GetPackerCaps()
{
	return 0
		| PK_CAPS_NEW
		| PK_CAPS_MODIFY
		| PK_CAPS_MULTIPLE
		| PK_CAPS_DELETE
		| PK_CAPS_OPTIONS
		//| PK_CAPS_MEMPACK
		//| PK_CAPS_BY_CONTENT
		//| PK_CAPS_SEARCHTEXT
		//| PK_CAPS_HIDE
		//| PK_CAPS_ENCRYPT
		;
}

EXPORTED void STDCALL SetChangeVolProcW (HANDLE hArcData, ::tChangeVolProcW pChangeVolProc)
{}

EXPORTED void STDCALL SetProcessDataProcW (HANDLE hArcData, ::tProcessDataProcW pProcessDataProc)
{}

BOOL WINAPI DllMain (HANDLE hModule, DWORD reason, LPVOID reserv) 
{
	if (sizeof(SCL_HEADER) != 14) { // compiler settings check
		_throw(0);
	}

	if (reason == DLL_PROCESS_ATTACH) 
	{
		dllInstance = (HINSTANCE)hModule;
		InitConfig(hModule);
	}
	else if (reason == DLL_PROCESS_DETACH) 
	{
	}

	return TRUE;
}

