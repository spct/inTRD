﻿
#pragma once

#include "..\common.h"

namespace TAP
{
	// Checks checksum of a tape block.
	// data: pointer to first byte (flags byte) of block.
	// size: size of block, including flags byte and checksum byte.
	static bool CheckSum(byte* data, size_t size)
	{
		byte t = 0;
		while(size-- != 0) t ^= *data++;
		return (t == 0);
	};

	// Computes and writes checksum of a tape block.
	// data: pointer to first byte (flags byte) of block.
	// size: size of block, starting from flags byte, NOT including checksum byte.
	static void WriteChecksum(byte* data, size_t size)
	{
		byte t = 0;
		while (size-- != 0) t ^= *data++;
		*data = t;
	};
};
