﻿
/* Helper for passing error codes and error messages around */
/* Вспомогательная структура для передачи кодов и сообщений об ошибках */


#pragma once


/****** Настройки **********/

// Следующие настройки следует ставить в common.h, а не здесь.

//default caption for message boxes
//#define ERROR_MSGBOX_CAPTION "Error"

//use MainWindow class to get application main window handle and use it as parent for messageboxes
//#define RESULT_USE_MAINWINDOW

//рекомендуется, если делаем wcx-плагин для Total Commander
//#define RESULT_WCX_SUPPORT



/****** Реализация *********/

#include "..\common.h"
#include <tchar.h>

#ifdef RESULT_WCX_SUPPORT
	#include "..\wcxhead.h"
#endif
#ifdef RESULT_USE_MAINWINDOW
	#include "MainWindow.h"
#endif

struct Result
{

	#ifdef _DEBUG
		public: int DebugInfo;
	#endif

private:
	
	// data format:
	// bit      0: 0=success, 1=error
	// bit      1: 1=win32Code specified
	// bit      2: 1=show message
	// byte  8-15: WCX error code; 0 = not an error or no wcx code specified
	// bits 16-23: not used
	int data;

	// result is not success
	#define RESULT_BIT_FAIL					1
	
	// Win32 error code known
	#define RESULT_BIT_WIN32CODESPECIFIED	2

	// display error message text
	#define RESULT_BIT_SHOWMESSAGE			4

	// message is unicode (WCHAR*)
	#define RESULT_BIT_WIDEMESSAGE			8

	// GetLastError value
	DWORD Win32Code;

	const void* customMsg;	// NULL if not specified; unicode if RESULT_BIT_WIDEMESSAGE is set

	bool success()				{ return (data & RESULT_BIT_FAIL) == 0; }
	bool win32CodeSpecified()	{ return (data & RESULT_BIT_WIN32CODESPECIFIED) != 0; }
	bool showMessage()			{ return (data & RESULT_BIT_SHOWMESSAGE) != 0; }

	#ifdef RESULT_WCX_SUPPORT
		byte& wcxCode()			{ return ((byte*)&data)[1]; }
	#endif

	bool IsNoMemError()
	{
		#ifdef RESULT_WCX_SUPPORT
			return (win32CodeSpecified() && Win32Code == ERROR_NOT_ENOUGH_MEMORY) || wcxCode() == E_NO_MEMORY;
		#else
			return (win32CodeSpecified() && Win32Code == ERROR_NOT_ENOUGH_MEMORY);
		#endif
	}

public:

	bool IsSuccess() { return success(); }


	#ifdef RESULT_WCX_SUPPORT
	
		byte& WcxCode() { return wcxCode(); }

		// Returns 0 ONLY if Success
		int WcxErrorCode()
		{
			if (IsSuccess()) return 0;
			if (WcxCode() != 0) return WcxCode();
			if (IsNoMemError()) return E_NO_MEMORY;
			return 1;	// assume general wcx error
		}

		// creates Fail result
		Result(int wcxCode)
		{
			data = RESULT_BIT_FAIL;
			this->wcxCode() = wcxCode;
			customMsg = NULL;
		}

	#endif

	// Creates Success result
	Result()
	{
		data = 0;
		customMsg = NULL;
	}

	// Creates Fail result
	Result(DWORD win32Code, bool showMessage)
	{
		data = RESULT_BIT_FAIL + RESULT_BIT_WIN32CODESPECIFIED + RESULT_BIT_SHOWMESSAGE * showMessage;
		this->Win32Code = win32Code;
		customMsg = NULL;
	}

	// creates Fail result
	Result(const char* customMsg, bool showMessage)
	{
		data = RESULT_BIT_FAIL + RESULT_BIT_SHOWMESSAGE * showMessage;
		this->customMsg = customMsg;
	}

	// creates Fail result
	Result(const WCHAR* customMsg, bool showMessage)
	{
		data = RESULT_BIT_FAIL + RESULT_BIT_SHOWMESSAGE * showMessage + RESULT_BIT_WIDEMESSAGE;
		this->customMsg = customMsg;
	}

	// creates Fail result
	Result(DWORD win32code, const char* customMsg, bool showMessage)
	{
		data = RESULT_BIT_FAIL + RESULT_BIT_SHOWMESSAGE * showMessage + RESULT_BIT_WIN32CODESPECIFIED;
		this->customMsg = customMsg;
		Win32Code = win32code;
	}

	// creates Fail result
	Result(DWORD win32code, const WCHAR* customMsg, bool showMessage)
	{
		data = RESULT_BIT_FAIL + RESULT_BIT_SHOWMESSAGE * showMessage + RESULT_BIT_WIN32CODESPECIFIED + RESULT_BIT_WIDEMESSAGE;
		this->customMsg = customMsg;
		Win32Code = win32code;
	}

	void SetMessage(const char* customMsg, bool showMessage)
	{
		this->customMsg = customMsg;
		data = (data & ~RESULT_BIT_SHOWMESSAGE) + RESULT_BIT_SHOWMESSAGE * showMessage;
	}

	void SetMessage(const WCHAR* customMsg, bool showMessage)
	{
		this->customMsg = customMsg;
		data = (data & ~RESULT_BIT_SHOWMESSAGE) + RESULT_BIT_SHOWMESSAGE * showMessage + RESULT_BIT_WIDEMESSAGE;
	}

	operator bool()
	{
		return success();
	}

	Result& operator&= (const Result& rhs)
	{
		if (success()) {
			*this = rhs;
		}
		return *this;
	}

	// helper method
	static void MessageBoxAW(HWND parent, const WCHAR* text, DWORD uType)
	{
		char* caption = ERROR_MSGBOX_CAPTION; // type check (expecting caption to be char*, not something else)
		const size_t capsize = ARRAY_LEN(ERROR_MSGBOX_CAPTION);
		WCHAR buf[capsize];
		if (MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, caption, capsize, buf, capsize) == 0) {
			buf[0] = 0;
		}
		LAST(buf) = 0;	// extra precaution
		MessageBoxW(parent, text, buf, uType);
	};

	// parent is optional (can be null)
	static void ShowErrorMessage(HWND parent, const char* text)
	{
		#ifdef RESULT_USE_MAINWINDOW
			if (!parent) { 
				parent = MainProcessWindow().Get(); 
			}
		#endif
		MessageBoxA(parent, text, ERROR_MSGBOX_CAPTION, MB_APPLMODAL | MB_OK | MB_ICONERROR);
	}

	// parent is optional (can be null)
	static void ShowErrorMessage(HWND parent, const WCHAR* text)
	{
		#ifdef RESULT_USE_MAINWINDOW
			if (!parent) { 
				parent = MainProcessWindow().Get();
			}
		#endif
		MessageBoxAW(parent, text, MB_APPLMODAL | MB_OK | MB_ICONERROR);
	}

	// Show meessage if it's error and a message text provided.
	// parent is optional (can be null)
	void ShowMessageIfNeed(HWND parent)
	{
		if (success() || !showMessage()) {
			return;
		}

		// first, display system message if available

		const TCHAR* sysmsg = NULL;
		DWORD slen = 0;
		if (win32CodeSpecified())
		{
			if (Win32Code == ERROR_NOT_ENOUGH_MEMORY)
			{
				// Обрабатываем по-особому, т.к. во-первых, может не хватить памяти для работы FormatMessage,
				// а во-вторых, системное сообщение выглядит непонятно: "Not enough storage is available to process this command."
				// Единственная проблема: мы покажем сообщение не на языке пользвателя.
				sysmsg = _T("Not enough memory");
				slen = UINT_MAX;	// do not LocalFree
			}
			else
			{
				slen = FormatMessage(
					FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					Win32Code,
					LANG_USER_DEFAULT,	// почему бы и не показать на языке пользователя
					(LPTSTR)&sysmsg,
					0,
					NULL);			// returns 0 on error
			}
		}
		
		if (slen) {
			ShowErrorMessage(parent, sysmsg);
		}
		if (slen != UINT_MAX) {
			LocalFree((HLOCAL)sysmsg); // "If the hMem parameter is NULL, LocalFree ignores the parameter and returns NULL."
		}

		// display custom message if available

		if (customMsg)
		{
			if (data & RESULT_BIT_WIDEMESSAGE) 
				ShowErrorMessage(parent, (WCHAR*)customMsg);
			else 
				ShowErrorMessage(parent, (char*)customMsg);
		}
	}

	static Result Create(int data) { 
		Result res; res.data = data; return res; 
	}

	static Result Create(int data, DWORD win32code) { 
		Result res; res.data = data; res.Win32Code = win32code; return res; 
	}
};


const Result RESULT_NO_MEM	= Result(DWORD(ERROR_NOT_ENOUGH_MEMORY), true);
const Result SUCCESS		= Result();
const Result FAIL			= Result::Create(RESULT_BIT_FAIL);			// error, just error

