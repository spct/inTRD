﻿
#include "..\common.h"
#include <Windows.h>

template<int nIDDialog>
class Dialog
{
public:
	virtual ~Dialog() {};

protected:
	HWND hDlg;
	HWND hwndParent;
	HINSTANCE dllInstance;
	bool DlgInitialized; // fields initialization complete?

	// Called on WM_INITDIALOG.
	// The dialog box procedure should return TRUE to direct the system to set the keyboard focus to the control specified by wParam. 
	// Otherwise, it should return FALSE to prevent the system from setting the default keyboard focus.
	virtual bool InitDialog(
		WPARAM hFocusItem,	// A handle to the control to receive the default keyboard focus. The system assigns the default keyboard focus only if the dialog box procedure returns TRUE.
		LPARAM dwInitParam
	) = 0;
	
	virtual bool DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;

private:

	HFONT hBoldFont;

	// This is used to be a ctor of Dialog object.
	void Init()
	{
		DlgInitialized = false;
		hBoldFont = NULL;
	};

	static INT_PTR CALLBACK DialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		// Замечу, что WM_INITDIALOG приходит далеко не первым, хотя всё же до первого WM_COMMAND.
		// Но вот действия, производимые в WM_INITDIALOG, часто приводят к генерации WM_COMMAND
		// до завершения обработки WM_INITDIALOG.
		if (uMsg == WM_INITDIALOG)
		{
			Dialog* cd = (Dialog*)lParam;
			cd->hDlg = hwndDlg;
			cd->Init();
			SetWindowLongPtr(hwndDlg, GWLP_USERDATA, (LONG_PTR)cd);
			return cd->InitDialog(wParam, lParam) ? TRUE : FALSE;
		}
		else
		{
			Dialog* cd = (Dialog*)GetWindowLongPtr(hwndDlg, GWLP_USERDATA);
			if (cd) {
				return cd->DialogProc(uMsg, wParam, lParam) ? TRUE : FALSE;
			} else {
				return FALSE;
			}
		}
	};

public:

	INT_PTR Show(HWND Parent, HINSTANCE DllInstance)
	{
		hwndParent = Parent;
		dllInstance = DllInstance;
		return DialogBoxParam(DllInstance, MAKEINTRESOURCE(nIDDialog), Parent, DialogProc, (LONG_PTR)this);
	};

protected: 

	bool IsChecked(int idButton)
	{
		return IsDlgButtonChecked(hDlg, idButton) == BST_CHECKED;
	}

	void SetCheck(int idButton, bool checked)
	{
		CheckDlgButton(hDlg, idButton, checked ? BST_CHECKED : BST_UNCHECKED);
	}

	void CheckRadioButton(int idFirstButton, int idLastButton, int idCheckButton)
	{
		if (idCheckButton >= idFirstButton && idCheckButton <= idLastButton)
			::CheckRadioButton(hDlg, idFirstButton, idLastButton, idCheckButton);
	}

	int GetRadioValue(int idFirstButton, int idLastButton)
	{
		return GetRadioValue(idFirstButton, idLastButton, idFirstButton);
	}

	int GetRadioValue(int idFirstButton, int idLastButton, int defaultValue)
	{
		for(int i = idFirstButton; i <= idLastButton; i++) {
			if (IsChecked(i)) 
				return i;
		}
		return defaultValue;
	}

	void EnableDlgItem(int nIDDlgItem, bool enable)
	{
		EnableWindow(GetDlgItem(hDlg, nIDDlgItem), enable ? TRUE : FALSE);
		// The function returns value indicating whether the window was previously visible.
	};

	void ShowDlgItem(int nIDDlgItem, bool show)
	{
		ShowWindow(GetDlgItem(hDlg, nIDDlgItem), show ? SW_SHOW : SW_HIDE);
		// The function returns value indicating whether the window was previously shown.
	};

	// limit - the maximum number of TCHARs the user can enter (not including zeroterm).
	void SetLimitText(int nIDDlgItem, WPARAM limit) 
	{
		SendDlgItemMessage(hDlg, nIDDlgItem, EM_SETLIMITTEXT, limit, 0);
	};

	// Returns old font, which must be saved and passed to UnsetBoldFont when closing form.
	HFONT SetBoldFont(int nIDDlgItem)
	{
		CreateBoldFont(nIDDlgItem);
		HFONT oldFont = (HFONT)SendDlgItemMessage(hDlg, nIDDlgItem, WM_GETFONT, 0,0);
		SendDlgItemMessage(hDlg, nIDDlgItem, WM_SETFONT, (WPARAM)hBoldFont, FALSE);
		return oldFont;
	}

	void UnsetBoldFont(int nIDDlgItem, HFONT oldFont)
	{
		SendDlgItemMessage(hDlg, nIDDlgItem, WM_SETFONT, (WPARAM)oldFont, FALSE);
	}

	void CreateBoldFont(int nIDDlgItem)
	{
		if (hBoldFont == NULL)
		{
			// Make bold version of GUI font.
			// NOTE: MSDN says: It is not recommended that you use DEFAULT_GUI_FONT or SYSTEM_FONT to obtain the font used by dialogs and windows...
			// That's why we prefer using font of some real dlg item.
			HFONT hFont = (HFONT)SendDlgItemMessage(hDlg, nIDDlgItem, WM_GETFONT, 0, 0);
			if (hFont == NULL) {
				hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
			}
			LOGFONT lf;
			GetObject(hFont, sizeof(lf), &lf);
			lf.lfWeight = FW_BOLD;
			hBoldFont = CreateFontIndirect(&lf);
		}
	}

	void DestroyBoldFont()
	{
		DeleteObject(hBoldFont);
		hBoldFont = NULL;
	}

	bool PostDlgItemMessage(int nIDDlgItem, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		return (0 != ::PostMessage(GetDlgItem(hDlg, nIDDlgItem), uMsg, wParam, lParam));
	}

	bool ListBox_AddString(int itemID, const TCHAR* text)
	{
		LRESULT res = (int)SendDlgItemMessage(hDlg, itemID, LB_ADDSTRING, 0, (LPARAM)text);
		return (res != LB_ERR && res != LB_ERRSPACE);
	}

	// Set listbox column width to match the witdh of the longest item text
	bool UpdateListboxHExtent(int nIDDlgItem)
	{
		#ifndef STRING_USE_CONSTRUCTOR
			// This implementation counts on String having ctor and dtor.
			_throw(0);
		#endif

		HWND hListbox = GetDlgItem(hDlg, nIDDlgItem);
		if (!hListbox) {
			return false;
		}

		// buffer for texts from listbox
		String<> buf;
		if (!buf.Append('@')) {  // this is to force String<> to create internal buffer
			return false;
		}

		int border = max(0, GetSystemMetrics(SM_CXBORDER));	// при ошибке возвращает 0, что нам пойдёт

		HDC hDC = GetDC(hListbox);
		if (!hDC) {
			return false;
		}

		HFONT hFont = (HFONT)SendMessage(hListbox, WM_GETFONT, 0, 0);	// returns NULL if the control is using the system font.
		HFONT hOldFont = (HFONT)SelectObject(hDC, hFont);

		LONG extent = 0;

		bool ok = true;
		{
			LONG_PTR style = GetWindowLongPtr(hListbox, GWL_STYLE);

			int cnt = (int)SendMessage(hListbox, LB_GETCOUNT, 0, 0);
			if (cnt < 0) { 
				ok = false; 
				goto exit; 
			}

			for(int i = 0; i < cnt; i++)
			{
				LRESULT l = SendMessage(hListbox, LB_GETTEXTLEN, i, 0);
				if (l < 0 || l >= 1000000000) { 
					ok = false; 
					goto exit; 
				}
				int len = (int)l;
				if (!buf.EnsureCapacity((uint)len + 5)) { 
					ok = false; 
					goto exit; 
				}
			
				if (SendMessage(hListbox, LB_GETTEXT, i, (LPARAM)buf.GetPtr()) == LB_ERR) { 
					ok = false; 
					goto exit; 
				}

				SIZE size;
				if ((style & LBS_USETABSTOPS) == 0)
				{
					ok = (0 != GetTextExtentPoint32(hDC, buf.GetPtr(), len, &size));
					size.cx += 2;
				}
				else
				{
					// Expand tabs as well
					DWORD t = GetTabbedTextExtent(hDC, buf.GetPtr(), len, 0, NULL);
					ok = (t != 0);
					size.cx = LOWORD(t);
					size.cx += 3;
				}

				if (!ok) {
					goto exit;
				}
			
				size.cx += 3 * border + 5;
				extent = max(extent, size.cx);
			}
		}

		exit:
		SelectObject(hDC, hOldFont);
		
		//exit_destroyDC:
		ReleaseDC(hListbox, hDC);

		if (ok)
		{
			SendMessage(hListbox, LB_SETHORIZONTALEXTENT, extent, 0);
		}

		return ok;
	}

};
