﻿
#pragma once

#include "..\common.h"

extern bool Convert_W_to_A(
	const WCHAR* input, // zero-terminated string to convert
	char* out_buf,		// output buffer
	size_t out_size		// size (in bytes) of output buffer including zero terminator
	);

extern bool Convert_A_to_W(
	const char* input,  // zero-terminated string to convert
	WCHAR* out_buf,		// output buffer
	size_t out_size		// size (in characters) of output buffer including zero terminator
	);

extern bool Convert_W_to_Utf8(
	const WCHAR* input, // zero-terminated string to convert
	char* out_buf,		// output buffer
	size_t out_size		// size (in bytes) of output buffer including zero terminator
	);

extern bool Convert_Utf8_to_W(
	const char* input,  // zero-terminated string to convert
	WCHAR* out_buf,		// output buffer
	size_t out_size		// size (in characters) of output buffer including zero terminator
	);

