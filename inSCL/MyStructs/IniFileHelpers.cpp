﻿
#include "IniFileHelpers.h"

Result ConfigFile_SaveStruct(TCHAR* filename, TCHAR* appName, TCHAR* keyName, void* pStruct, size_t structSize)
{
	if (structSize > UINT_MAX) _throw("CFG_SS01");
	BOOL res = WritePrivateProfileStruct(appName, keyName, pStruct, (uint)structSize, filename);
	if (res) {
		return SUCCESS;
	}
	
	// NOTE: Result.ShowErrorMessage устроена таким образом, что сперва показывает 
	// системное сообщение (соответствующее GetLastError),
	// а потом наш custom message.

	return Result(GetLastError(), "Error saving settings.", true); 
};

Result ConfigFile_SaveBool(TCHAR* filename, TCHAR* appName, TCHAR* keyName, bool val)
{
	BOOL res = WritePrivateProfileString(appName, keyName, val ? _T("1") : _T("0"), filename);
	if (res) {
		return SUCCESS;
	}
	return Result(GetLastError(), "Error saving settings.", true);
};

Result ConfigFile_SaveUint(TCHAR* filename, TCHAR* appName, TCHAR* keyName, uint val)
{
	TCHAR buf[15];
	utoa(val, buf);
	BOOL res = WritePrivateProfileString(appName, keyName, buf, filename);
	if (res) {
		return SUCCESS;
	}
	return Result(GetLastError(), "Error saving settings.", true);
};

uint ConfigFile_LoadUint(
	TCHAR* filename, TCHAR* appName, TCHAR* keyName, uint def_val)
{
	TCHAR buf[15];
	DWORD len = GetPrivateProfileString(appName, keyName, _T(""), buf, ARRAY_LEN(buf), filename); 
	// "The return value is the number of characters copied to the buffer, not including the terminating null character."
	uint result = def_val;
	if (len != 0 && len < ARRAY_LEN(buf) - 1) // if string is not empty and not truncated
	{
		// Parse integer
		uint64 t = 0;
		for(TCHAR* s = buf; *s; s++) {
			if (!InRange(*s, TCHAR('0'), TCHAR('9'))) {
				goto k0;
			}
			t = t * 10u + uint(*s - '0');
			if (t > UINT_MAX) {
				goto k0;
			}
		}
		result = (uint)t;
		k0:;
	}
	return result;
};

bool ConfigFile_LoadBool(TCHAR* filename, TCHAR* appName, TCHAR* keyName, bool def_val)
{
	INT t = (INT)GetPrivateProfileInt(appName, keyName, -1, filename);
	if (t < 0) {
		return def_val;
	}
	return (t != 0);
};

