﻿/*
 * Helper class for working with strings
 */

#pragma once

#include "..\common.h"
#include "Result.h"
#include "myalloc.h"

/*
	В режиме STRING_USE_CONSTRUCTOR инициализация не нужна,
	она происходит в конструкторе.
	Тем не менее метод Destroy остался, чтобы можно было писать
	универсальный код (рассчитаный и на конструкторы, и на их отсутствие).
*/

#ifdef MYSTRUCTS_ALLOW_CTORS
	#define STRING_USE_CONSTRUCTOR
#endif

//#define STRING_USE_CONSTRUCTOR
//#undef STRING_USE_CONSTRUCTOR

// Эту настройку следует ставить в common.h, а не здесь:
//#define STRING_COUNT_TYPE_DEFAULT uint

const Result RESULT_STRING_CAPACITY_TOO_LARGE = Result("Capacity of string is too large", true);
const Result RESULT_STRING_OVERFLOW = Result("Overflow of string", true);
const Result RESULT_STRING_INVALID_REMOVE_ARGS = Result("Invalid args of String.Remove operation", true);

// count_t: data type for string length; 
// can be any unsigned integer not larger than size_t; usually uint
template <typename T = TCHAR, typename count_t = STRING_COUNT_TYPE_DEFAULT>
class String
{
	// Эта переменная будет только одна для каждой комбинации <T, count_t>.
	// Даже если String использован в нескольких cpp.
	private: static const T empty_buf = 0;

	private: static const count_t count_t_max = count_t(~count_t(0)); // count_t must be unsigned!
	public: static const count_t MAX_CAPACITY = count_t_max / 2u / sizeof(T) - 5;

	// String content. 
	// Not necessarily ending with zero. Zero added on GetPtr() or End0Ptr(). 
	// Array size is Capacity+1.
	// Can be NULL when Capacity = 0.
	private: T* data;					

	// data array size not including zero-terminator
	private: count_t Capacity;		

	// string length, not including zero-terminator
	public: count_t Length;

	// Memory helpers
	private: bool mem_alloc(count_t count, T** mem)
	{
		if (count <= 1) { *mem = NULL; return true; }
		else { return (*mem = myalloc<T>(count)) != NULL; }
	}
	private: void mem_free(T* mem)
	{
		if (mem) myfree(mem);
	}
	private: bool mem_realloc(count_t newCount, T*& mem)
	{
		if (newCount <= 1) { mem_free(mem); mem = NULL; return true; }
		else if (mem == NULL) return mem_alloc(newCount, &mem);
		else return myrealloc<T>(mem, newCount, false);
	}


private:

	static const byte INITIALIZED = 0x8F;	// magic non-zero value
	byte Initialized;

	void CheckInitialized()
	{
		if (Initialized == INITIALIZED) {
			return;
		}
		if (this_is_clear()) { 
			Initialized = INITIALIZED; 
			return; 
		}
		_throw("String not initialized");
	}

#ifdef STRING_USE_CONSTRUCTOR

public:

	String()
	{
		clear_this();
		Initialized = INITIALIZED;
	}
	~String()
	{
		Destroy();
	}

	// Destructs this instance. Calling it is optional as it's done by destructor.
	void Destroy()
	{
		if (data)
		{
			CheckInitialized();
			mem_free(data);
		}
		clear_this();
	}


#else

public: 

	// Обязательно к вызову после создания объекта (объявления переменной).
	// Если Init вернул ошибку, то деструктор вызывать не обязательно.
	// Вернуть может:
	//      SUCCESS
	Result Init()
	{
		//if (data) _throw("already initialized");	// без конструктора нельзя такую проверку делать, т.к. память может содержать мусор
		clear_this();
		Initialized = INITIALIZED;
		return SUCCESS;
	}

	Result Init(const T* initValue)
	{
		Result res = Init();
		if (!res) {
			return res;
		}
		return Set(initValue);
	}

	void Destroy()
	{
		if (data)
		{
			CheckInitialized();
			mem_free(data);
		}
		clear_this();
	}

#endif

	public: Result EnsureCapacity(count_t reqCapacity)
	{
		CheckInitialized();
		if (reqCapacity > Capacity)
		{
			if (reqCapacity > MAX_CAPACITY) {
				return RESULT_STRING_CAPACITY_TOO_LARGE;
			}
			
			count_t newCapacity = (Capacity == 0) ? reqCapacity : Capacity;
			while (newCapacity < reqCapacity) {
				newCapacity = min(newCapacity * 2, MAX_CAPACITY);
				// no overflow possible as newCapacity <= MAX_CAPACITY <= count_t_max/2
			}
			
			if (newCapacity != 0)
			{
				// So we need buffer of size newCapacity+1 chars or more.
				// Round up to memory allocation unit size.
				size_t bufSize = RecommendedAllocCount<T>(newCapacity + 1);
				newCapacity = count_t(min(bufSize - 1, MAX_CAPACITY));
			}

			if (!mem_realloc(newCapacity + 1, data)) {
				return RESULT_NO_MEM;
			}
			Capacity = newCapacity;
		}
		return SUCCESS;
	}

	// Returns ptr to internal string buffer; 
	// Guarantees zero-terminator in the end of string.
	public: const T* GetPtr()
	{
		CheckInitialized();
		if (Length == 0) {
			return &empty_buf;	// Mandatory because of mem_alloc optimization for count=0
		}
		data[Length] = 0;
		return data;
	}

	// Same as EnsureCapacity(Length + count)
	public: Result AddSpace(count_t count)
	{
		CheckInitialized();
		// addition overflow impossible because MAX_CAPACITY = count_t_max/2
		if (count > MAX_CAPACITY || Length + count > MAX_CAPACITY) {
			return RESULT_STRING_CAPACITY_TOO_LARGE;
		}
		return EnsureCapacity(Length + count);
	}

	Result Append(const T* src)
	{
		CheckInitialized();

		// Compute new string length
		count_t newLength = Length;
		for(const T* x = src; *x; x++)
		{
			if (newLength == MAX_CAPACITY) {
				return RESULT_STRING_CAPACITY_TOO_LARGE;
			}
			newLength++;
		}

		// Increase buffer size if necessary
		Result res = EnsureCapacity(newLength);
		if (!res) {
			return res;
		}

		// Append new data
		for(const T* x = src; *x; x++) {
			data[Length++] = *x;
		}

		return SUCCESS;
	}

	Result Set(const T* src)
	{
		CheckInitialized();
		Length = 0;
		return Append(src);
	}

	void Clear()
	{
		CheckInitialized();
		Length = 0;
	}

	Result Append(T chr)
	{
		CheckInitialized();
		Result res = EnsureCapacity(Length + 1);
		if (!res) {
			return res;
		}
		data[Length++] = chr;
		return SUCCESS;
	}

	Result Append(T chr1, T chr2) 
	{	
		Result res = Append(chr1); res &= Append(chr2); 
		return res; 
	}

	Result Append(T chr1, T chr2, T chr3)
	{	
		Result res = Append(chr1); res &= Append(chr2); res &= Append(chr3); 
		return res; 
	}
	
	// Removes one last char, or throw exception if string is empty
	void RemoveLastChar()
	{
		CheckInitialized();
		if (Length == 0) {
			_throw("String is empty");
		}
		Length--;
	}

	T& operator[](count_t index)
	{
		CheckInitialized();
		if (index >= Length) {
			_throw("Index out of string");
		}
		return data[index];
	}
	
	// Make new String instance and return it in *out_string
	Result Clone(String<T>* out_result)
	{
		CheckInitialized();
		String<T> t;
		Result res = SUCCESS;
		#ifndef STRING_USE_CONSTRUCTOR
			res = t.Init();
		#endif
		if (res) {
			res = t.Append(GetPtr());
		}
		if (!res) {
			return res;
		}
		*out_result = t; // mem copy
		clear_struct(t); // make t.destructor no-op preventing it from freeing data
		return SUCCESS;
	}

	// Get pointer to the end of string (it is not necessarily zero there!)
	const T* EndPtr()
	{
		CheckInitialized();
		if (Length == 0) {
			return &empty_buf;	// Mandatory because of mem_alloc optimization for count=0
		}
		return &data[Length];
	}

	// Get pointer to the end of string (zero-term forcibly written there!)
	const T* End0Ptr()
	{
		CheckInitialized();
		if (Length == 0) {
			return &empty_buf;	// Mandatory because of mem_alloc optimization for count=0
		}
		data[Length] = 0;
		return &data[Length];
	}

	Result Remove(count_t start, count_t count)
	{
		CheckInitialized();
		count_t from = start + count;
		if (start > MAX_CAPACITY || count > MAX_CAPACITY || from > Length) {
			return RESULT_STRING_INVALID_REMOVE_ARGS;
		}
		while(from != Length) {
			data[start++] = data[from++];
		}
		Length -= count;
		return SUCCESS;
	}

	bool EndsWithSlash()
	{
		CheckInitialized();
		return Length != 0 && IsSlash(data[Length-1]);
	}

	Result AppendPath(const T* path)
	{
		CheckInitialized();
		if (Length != 0 && !EndsWithSlash()) {
			Result res = Append('\\');
			if (!res) {
				return res;
			}
		}
		if (IsSlash(path[0])) {
			path++;
		}
		return Append(path);
	}

	Result AppendInt(int x)
	{
		T buf[15];
		itoa(x, buf);				// calls one of two itoa overloads in serv.h
		return Append(buf);
	}

	Result AppendUint(uint x)
	{
		T buf[15];
		utoa(x, buf);				// calls one of two utoa overloads in serv.h
		return Append(buf);
	}

	Result AppendSizeT(size_t x)
	{
		T buf[25];
		utoa_t(x, buf);				// calls one of two utoa_t overloads in serv.h
		return Append(buf);
	}

	// Append integer with leading zeroes.
	// digitCount must be <= 14
	Result AppendUint0(uint x, size_t digitCount)
	{
		T buf[15];
		if (digitCount > ARRAY_LEN(buf) - 1) {
			return Result("Error s091", true);
		}
		utoa0(x, buf, digitCount);
		return Append(buf);
	}

	// Append hex integer with leading zeroes.
	// digitCount must be <= 14.
	// hexChars may be HexChars_Up or HexChars_Low from serv.h
	Result AppendUint0_Hex(uint x, size_t digitCount, const char* hexChars)
	{
		T buf[15];
		if (digitCount > ARRAY_LEN(buf) - 1) {
			return Result("Error s092", true);
		}
		utoa0_hex(x, buf, digitCount, hexChars);
		return Append(buf);
	}
};
