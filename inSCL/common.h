﻿
//#pragma once
// нельзя этот pragma писать здесь, иначе инклуды внизу сработают только один раз,
// в общем, не будет работать.

//#include <vld.h>
// Чтобы VLD заработал, надо раскомментить этот инклуд, и еще в DllMain вызвать VLDEnable.

/* MyStructs settings */

//#define CUSTOM_INTRINSICS
#undef CUSTOM_INTRINSICS

#define MYSTRUCTS_ALLOW_CTORS
//#undef MYSTRUCTS_ALLOW_CTORS

#define MYALLOC_USE_WINAPI_HEAP
//#define MYALLOC_USE_CRT
//#define MYALLOC_DEBUG_OUTPUT	// закомментить перед релизом

#define ERROR_MSGBOX_CAPTION "inSCL error"
#define RESULT_USE_MAINWINDOW
#define RESULT_WCX_SUPPORT

#define STRING_COUNT_TYPE_DEFAULT uint
#define LIST_COUNT_TYPE_DEFAULT uint

#define FILEHANDLE_WCX_SUPPORT

/* End of MyStructs settings */


#include <Windows.h>
#include "MyStructs\serv.h"
#include "MyStructs\MyIntrinsics.h"

#define INSCL
