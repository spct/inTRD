﻿
#include "ArcHandle.h"


Result ArcHandle::Open(const TCHAR* path, bool writeable, DWORD createDisposition)
{
	Result res = File.Open(path, writeable, createDisposition, false, true);
	if (res && !File.Created)
	{
		// Opening existing archive
		
		if (!InRange(File.Size, 8+1+4, 8+1+4 + (14 + 255 * 256) * 255))
			res = Result(E_UNKNOWN_FORMAT);
		else if (memcmp_(File.Adr, "SINCLAIR", 8) != 0)
			res = Result(E_UNKNOWN_FORMAT);
	}

	if (res)
	{
		// Save creation and mod date, we may need these
		if ((0 == GetFileTime(File.Handle(), &ArchiveCreateDateUtc, NULL, &ArchiveDateUtc))
			|| !(ArchiveDateTC = Utc2DosLocal(ArchiveDateUtc)))
		{
			res = Result(E_EOPEN);
		}
	}

	if (!res) {
		File.Close();
	}
	return res;
};
