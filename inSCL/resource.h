//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by inSCL.rc
//
#define IDD_DIALOG1                     101
#define IDD_CONFIG                      101
#define IDC_ABOUT                       1001
#define IDC_CFGPATH                     1002
#define IDC_MESSAGE                     1003
#define IDC_NUMBERING_NONE              1011
#define IDC_NUMBERING_HEX               1012
#define IDC_NUMBERING_DEC               1013
#define IDC_NUMBERING_SEPARATOR         1016
#define IDC_INMBD_COMPATIBLE            1017
#define IDC_SMART_LEN_DETECT            1021
#define IDC_EXT_EXT                     1022
#define IDC_SHOW_DELETED                1023
#define IDC_DETECT_HOBETA               1024
#define IDC_EXTRACT_HOBETA              1025
#define IDC_SHOWSIZEINSECTORS           1027
#define IDC_AUTOJOIN                    1031
#define IDC_AUTOJOINED_PREFIX           1032

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1043
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
