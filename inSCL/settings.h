﻿
#pragma once

#include "common.h"

enum NUMBERING
{
	NONE = 0,
	HEX = 1,
	DEC = 2
};

struct CONFIG
{
	static const int STRUCT_VER = 2;
	int StructVer;			// всегда полезно иметь версию структуры настроек

	bool InMBDCompatible;
	NUMBERING FileNameNumbering_;
	char FileNameNumberingSeparator[9];	// в кофиге показываем, в файле храним, но изменять через GUI не даём

	bool SmartLenDetect_;
	bool ExtendedExtension;		// TODO: не имеет смысла (да и не используется) в режиме inMBD compatibile
	bool DetectHobeta;
	bool ExtractHobeta_;
	bool ShowDeletedFiles;		// в SCL не бывает удалённых файлов, для него это поле не используется, но всё-равно сохраняется
	bool ShowSizeInSectors;		// affects displayed size only, not unpacked size
	bool CheckCrcBeforeModify;	// default is true; hidden option - not displayed in gui

	bool AutoJoinLargeFiles_;
	char AutojoinedFilePrefix[9];

	bool ExtractHobeta()
	{
		return ExtractHobeta_ && !InMBDCompatible;
	}

	bool SmartLenDetect()
	{
		// Не имеет смысла для hobeta, в хобета всегда сохраняется целое число секторов.
		// А вот с режимом InMBDCompatible совместимо, почему бы и нет.
		return SmartLenDetect_ && !ExtractHobeta();
	}

	bool AutoJoinLargeFiles()
	{
		// Не имеет смысла для hobeta, в хобета не может быть больше 255 секторов
		return AutoJoinLargeFiles_ && !ExtractHobeta();
	}

	NUMBERING FileNameNumbering()
	{
		// Не имеет смысла в режиме inMBD-compatible.
		return InMBDCompatible ? NUMBERING::NONE : FileNameNumbering_;
	}
};

extern BOOL InitConfig(HANDLE hModule);
extern CONFIG GetPrimaryConfig();
//extern void SetPrimaryConfig(CONFIG cfg);
