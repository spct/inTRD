﻿
#pragma once

#include "common.h"
#include "settings.h"
//#include "ArcHandle.h"	// this leads to cyclic include. Do forward declaration instead
#include "MBD\Common.h"
#include "MBD\Options.h"
#include "MyStructs\String.h"
#include "MyStructs\Result.h"
#include "wcxhead.h"

struct Entry;
struct ArcHandle;


#pragma pack(push, 1)
struct NAME_HEADER
{
	byte Name[8];
	byte Extension;
	WORD Extension2;
};
#pragma pack(pop)

// Helper for storing Start/Length values in file creation date
struct FileCreateDateData
{
public:

	bool Has_StartLength;
	bool IsHeaderless;		// file has no header block (for tape images)
	WORD Start;				// Адрес загрузки кодового файла. Для BASIC - длина basic-программы и переменных.
	WORD Length;			// Длина кодового файла.          Для BASIC - длина basic-программы без переменных.
	
	// Creates FileCreateDateData struct with IsValid()=false
	FileCreateDateData()
	{
		clear_this();
	}

	// Invalidate
	~FileCreateDateData()
	{
		clear_this();
	}

	// Returns true if found start/length values or headerless marker
	bool IsValid()
	{ 
		return (Has_StartLength != IsHeaderless);
	}

	FileCreateDateData(WORD start, WORD length)
	{
		Start = start;
		Length = length;
		Has_StartLength = true;
		IsHeaderless = false;
	}

	FileCreateDateData(FILETIME createDate);

	FILETIME MakeFiletime();
};


// Build file name in dest.
// Assumes that dest points to tHeaderDataEx.FileName, which is at least [260]
extern Result GetName(Entry& entry, char* dest, CONFIG& cfg, bool withDups);

// Отрезает numbering prefix и DuplicateN-суффикс.
// Заполняет Name, Extension;
// если есть длинное расширение и разрешена его поддержка, то заполняет и Start.
// name может содержать и '\', т.е. относительный путь.
// name не может быть пустым!
extern void NamePC2ZX(char* name, NAME_HEADER* output, const CONFIG& cfg, bool canBeHobeta, bool* out_hasHobetaExtension);


extern bool SetFileModificationAndCreationDate(HANDLE hFile, FILETIME modificationTimeUtc, FILETIME creationTimeUtc);

extern Result AutojoinFiles(ArcHandle& arc);

